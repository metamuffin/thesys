#![feature(iterator_try_collect)]
use std::{
    cell::UnsafeCell,
    collections::{BTreeMap, VecDeque},
    env::var,
    fmt::Write as FmtWrite,
    io::{read_to_string, stdin, stdout, BufReader, BufWriter, Bytes, Error, Read, Write},
    net::{SocketAddr, TcpListener, TcpStream, ToSocketAddrs},
    sync::{
        atomic::{AtomicI64, AtomicUsize, Ordering},
        mpsc::{channel, Receiver, Sender},
        Arc, RwLock,
    },
    thread,
    time::Duration,
};

type Value = i64;

const OP_NOA: i64 = 0;
const OP_ADD: i64 = 1;
const OP_SUB: i64 = 2;
const OP_MUL: i64 = 3;
const OP_DIV: i64 = 4;
const OP_MOD: i64 = 5;
const OP_EQUAL: i64 = 6;
const OP_LESS: i64 = 7;
const OP_GREATER: i64 = 8;
const OP_LNOT: i64 = 9;
const OP_BNOT: i64 = 10;
const OP_XOR: i64 = 11;
const OP_OR: i64 = 12;
const OP_AND: i64 = 13;
const OP_SHL: i64 = 14;
const OP_SHR: i64 = 15;
const OP_MOVE: i64 = 16;
const OP_RSP: i64 = 17;
const OP_MSP: i64 = 18;
const OP_JUMP: i64 = 19;
const OP_CJUMP: i64 = 20;
const OP_RPC: i64 = 21;
const OP_SPAWN: i64 = 22;
const OP_HALT: i64 = 23;
const OP_ACCEPT: i64 = 24;
const OP_READ: i64 = 25;
const OP_WRITE: i64 = 26;
const OP_FADD: i64 = 27;
const OP_FSUB: i64 = 28;
const OP_FMUL: i64 = 29;
const OP_FDIV: i64 = 30;
const OP_FTOI: i64 = 31;
const OP_ITOF: i64 = 32;

const AM_IMMEDIATE: i64 = 0;
const AM_BASE: i64 = 1;
const AM_STACK: i64 = 2;
const AM_STACK_MEMORY: i64 = 3;

fn main() {
    let memsize = var("MEMORY")
        .unwrap_or("16777216".to_owned())
        .parse::<usize>()
        .unwrap();
    let bind_address = var("BIND_ADDR").unwrap_or("127.0.0.1".to_owned());
    let port = var("PORT")
        .unwrap_or("2000".to_owned())
        .parse::<u16>()
        .unwrap();
    let max_cores = var("MAX_CORES")
        .unwrap_or("2000".to_owned())
        .parse::<usize>()
        .unwrap();
    let initial = read_to_string(stdin())
        .unwrap()
        .trim()
        .strip_prefix("[")
        .expect("memory doesnt start with [")
        .strip_suffix("]")
        .expect("memory doesnt end in ]")
        .split(",")
        .map(|e| e.trim().parse::<Value>())
        .try_collect::<Vec<Value>>()
        .expect("source invalid");

    let memory = Arc::new(Memory::new(memsize));
    for (i, v) in initial.iter().enumerate() {
        memory.store(i as Value, *v);
    }
    drop(initial);

    let devices = Arc::new(Devices::new((bind_address, port)));
    let (cores, log_rx) = Cores::new(max_cores);

    cores
        .register(Core {
            memory,
            devices: devices.proxy(),
            cores: cores.clone(),
            pc: 0,
            ci: 0,
            sp: memsize as Value,
            bp: 0,
            inst: 0,
            executed: 0,
        })
        .unwrap();

    let mut last_metrics = BTreeMap::<Value, CoreMetrics>::default();
    let rate = 10;
    let mut log = VecDeque::new();
    loop {
        let mut o = String::new();
        write!(o, "\x1b[2J\x1b[1;1H").unwrap();
        let metrics = cores.metrics.read().unwrap().clone();
        writeln!(o, "Active cores:").unwrap();
        for (ci, m) in &metrics {
            let m_last = last_metrics.get(&ci).cloned().unwrap_or_default();
            writeln!(
                o,
                "\tcore{ci}: rate {}/s, total {}, {}",
                (m.executed - m_last.executed) / rate as u128,
                m.executed,
                match m.waiting {
                    Some(WaitReason::Accept) => "waiting: accept".to_string(),
                    Some(WaitReason::Read(di)) => format!("waiting: read device{di}"),
                    Some(WaitReason::Write(di)) => format!("waiting: write device{di}"),
                    None => "running".to_string(),
                }
            )
            .unwrap();
        }

        writeln!(o, "").unwrap();
        writeln!(o, "Devices:").unwrap();
        let devices = devices.accepted.read().unwrap();
        for (di, (_, addr)) in devices.iter() {
            writeln!(o, "\tdev{di}: addr {addr}",).unwrap();
        }
        drop(devices);

        writeln!(o, "").unwrap();
        writeln!(o, "Log:").unwrap();
        log.extend(log_rx.try_iter());
        while log.len() > 16 {
            log.pop_front();
        }
        for (ci, message) in &log {
            writeln!(o, "\tcore{ci}: {message}").unwrap();
        }

        last_metrics = metrics;

        stdout().write_all(o.as_bytes()).unwrap();
        stdout().flush().unwrap();
        thread::sleep(Duration::from_micros(1_000_000 / rate))
    }
}

struct Core {
    memory: Arc<Memory>,
    devices: LocalDevices,
    cores: Arc<Cores>,
    ci: Value,
    pc: Value,
    sp: Value,
    bp: Value,
    inst: Value,
    executed: usize,
}
impl Core {
    pub fn run(&mut self) {
        while self.step() {}
    }
    pub fn arg_out(&mut self, n: i32, res: Value) {
        let mode = (self.inst >> (8 + n * 2)) & 0b11i64;
        let arg = self.memory.load(self.pc);
        self.pc += 1;
        match mode {
            AM_IMMEDIATE => (),
            AM_BASE => self.memory.store(arg + self.bp, res),
            AM_STACK => self.memory.store(arg + self.sp, res),
            AM_STACK_MEMORY => self.memory.store(self.memory.load(arg + self.sp), res),
            _ => (),
        }
    }
    pub fn arg_in(&mut self, n: i32) -> Value {
        let mode = (self.inst >> (8 + n * 2)) & 0b11i64;
        let arg = self.memory.load(self.pc);
        self.pc += 1;
        match mode {
            AM_IMMEDIATE => arg,
            AM_BASE => self.memory.load(arg + self.bp),
            AM_STACK => self.memory.load(arg + self.sp),
            AM_STACK_MEMORY => self.memory.load(self.memory.load(arg + self.sp)),
            _ => 0,
        }
    }
    pub fn set_busy(&mut self, b: Option<WaitReason>) {
        self.cores.submit_metrics(self.ci, self.executed, b);
        self.executed = 0;
    }
    pub fn step(&mut self) -> bool {
        self.inst = self.memory.load(self.pc);
        self.pc += 1;
        self.executed += 1;

        if self.executed > 10_000 {
            self.set_busy(None)
        }

        let op = self.inst & 0xff;

        match op {
            OP_NOA => {}
            OP_ADD => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x + y);
            }
            OP_SUB => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x - y);
            }
            OP_MUL => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x * y);
            }
            OP_DIV => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x / y);
            }
            OP_MOD => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x % y);
            }
            OP_EQUAL => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, (x == y) as Value);
            }
            OP_LESS => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, (x < y) as Value);
            }
            OP_GREATER => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, (x > y) as Value);
            }
            OP_LNOT => {
                let x = self.arg_in(0);
                self.arg_out(1, !((x != 0) as Value));
            }
            OP_BNOT => {
                let x = self.arg_in(0);
                self.arg_out(1, !x);
            }
            OP_XOR => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x ^ y);
            }
            OP_OR => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x | y);
            }
            OP_AND => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x & y);
            }
            OP_SHL => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x << y);
            }
            OP_SHR => {
                let x = self.arg_in(0);
                let y = self.arg_in(1);
                self.arg_out(2, x >> y);
            }
            OP_MOVE => {
                let x = self.arg_in(0);
                self.arg_out(1, x);
            }
            OP_RSP => {
                self.arg_out(0, self.sp);
            }
            OP_MSP => {
                self.sp += self.arg_in(0);
            }
            OP_JUMP => {
                self.pc = self.arg_in(0);
            }
            OP_CJUMP => {
                if self.arg_in(0) != 0 {
                    self.pc = self.arg_in(1);
                } else {
                    self.arg_in(1);
                }
            }
            OP_RPC => {
                self.arg_out(0, self.pc);
            }
            OP_SPAWN => {
                let pc = self.arg_in(0);
                let sp = self.arg_in(1);
                match self.cores.register(Core {
                    memory: self.memory.clone(),
                    devices: self.devices.proxy(),
                    cores: self.cores.clone(),
                    pc,
                    sp,
                    ci: 0,
                    bp: 0,
                    inst: 0,
                    executed: 0,
                }) {
                    Ok(()) => self.arg_out(2, 0),
                    Err(e) => {
                        self.log(format!(
                            "spawn fail {:?}",
                            Error::from_raw_os_error(e).to_string()
                        ));
                        self.arg_out(2, e.into())
                    }
                }
            }
            OP_HALT => {
                return false;
            }
            OP_ACCEPT => {
                self.set_busy(Some(WaitReason::Accept));
                let ci = self.devices.accept().expect("accept failed");
                self.set_busy(None);
                self.arg_out(0, ci);
            }
            OP_READ => {
                let id = self.arg_in(0);
                self.set_busy(Some(WaitReason::Read(id)));
                match self.devices.read(id) {
                    Ok(x) => {
                        self.arg_out(1, x as Value);
                        self.arg_out(2, 0);
                    }
                    Err(e) => {
                        self.log(format!(
                            "read {id} fail {:?}",
                            Error::from_raw_os_error(e).to_string()
                        ));
                        self.arg_out(1, 0);
                        self.arg_out(2, e.into());
                        self.devices.remove(id);
                    }
                }
                self.set_busy(None);
            }
            OP_WRITE => {
                let id = self.arg_in(0);
                let data = self.arg_in(1);
                if id == -1 {
                    self.log(format!("[D] {data:x}"));
                    self.arg_out(2, 0)
                } else {
                    self.set_busy(Some(WaitReason::Write(id)));
                    match self.devices.write(id, data as u8) {
                        Ok(()) => self.arg_out(2, 0),
                        Err(e) => {
                            self.log(format!(
                                "write {id} fail {:?}",
                                Error::from_raw_os_error(e).to_string()
                            ));
                            self.arg_out(2, e.into());
                            self.devices.remove(id);
                        }
                    }
                    self.set_busy(None);
                }
            }
            OP_FADD => {
                let x = f64::from_bits(self.arg_in(0) as u64);
                let y = f64::from_bits(self.arg_in(1) as u64);
                self.arg_out(2, f64::to_bits(x + y) as i64);
            }
            OP_FSUB => {
                let x = f64::from_bits(self.arg_in(0) as u64);
                let y = f64::from_bits(self.arg_in(1) as u64);
                self.arg_out(2, f64::to_bits(x - y) as i64);
            }
            OP_FMUL => {
                let x = f64::from_bits(self.arg_in(0) as u64);
                let y = f64::from_bits(self.arg_in(1) as u64);
                self.arg_out(2, f64::to_bits(x * y) as i64);
            }
            OP_FDIV => {
                let x = f64::from_bits(self.arg_in(0) as u64);
                let y = f64::from_bits(self.arg_in(1) as u64);
                self.arg_out(2, f64::to_bits(x / y) as i64);
            }
            OP_FTOI => {
                let x = f64::from_bits(self.arg_in(0) as u64);
                self.arg_out(1, x as i64);
            }
            OP_ITOF => {
                let x = self.arg_in(0) as f64;
                self.arg_out(1, f64::to_bits(x) as i64);
            }
            op => {
                self.log(format!("crash! invalid instruction {op:x}"));
                return false;
            }
        }

        true
    }
    fn log(&self, message: String) {
        self.cores.log.send((self.ci, message)).unwrap();
    }
}

struct Memory(UnsafeCell<Vec<Value>>);
unsafe impl Send for Memory {}
unsafe impl Sync for Memory {}
impl Memory {
    pub fn new(size: usize) -> Self {
        Self(UnsafeCell::new(vec![0; size]))
    }
    #[inline(always)]
    pub fn load(&self, addr: Value) -> Value {
        unsafe {
            *self
                .0
                .get()
                .as_mut()
                .unwrap()
                .get(addr as usize)
                .unwrap_or(&0xff)
        }
    }
    #[inline(always)]
    pub fn store(&self, addr: Value, value: Value) {
        unsafe {
            *self
                .0
                .get()
                .as_mut()
                .unwrap()
                .get_mut(addr as usize)
                .unwrap_or(&mut 0) = value
        }
    }
}

struct LocalDevices {
    devices: Arc<Devices>,
    local: BTreeMap<Value, (Bytes<BufReader<TcpStream>>, BufWriter<TcpStream>)>,
}
impl LocalDevices {
    pub fn read(&mut self, id: Value) -> Result<u8, i32> {
        match self.local.get_mut(&id) {
            Some((read, _)) => read
                .next()
                .ok_or(32)?
                .map_err(|e| e.raw_os_error().unwrap_or(5)),
            None => {
                self.make_local(id)?;
                self.read(id)
            }
        }
    }
    pub fn write(&mut self, id: Value, byte: u8) -> Result<(), i32> {
        match self.local.get_mut(&id) {
            Some((_, write)) => {
                write
                    .write_all(&[byte])
                    .map_err(|e| e.raw_os_error().unwrap_or(5))?;
                if byte == b'\n' {
                    write.flush().map_err(|e| e.raw_os_error().unwrap_or(5))?;
                }
                Ok(())
            }
            None => {
                self.make_local(id)?;
                self.write(id, byte)
            }
        }
    }
    pub fn accept(&mut self) -> Result<Value, i32> {
        let ci = self.devices.accept()?;
        Ok(ci)
    }
    pub fn remove(&mut self, id: Value) {
        self.devices.remove(id);
        self.local.remove(&id);
        // TODO clear everything
    }
    pub fn proxy(&self) -> LocalDevices {
        self.devices.proxy()
    }
    fn make_local(&mut self, id: Value) -> Result<(), i32> {
        let stream = self
            .devices
            .accepted
            .read()
            .unwrap()
            .get(&id)
            .ok_or(9)? // bad fd
            .0
            .try_clone()
            .map_err(|e| e.raw_os_error().unwrap_or(5))?;
        let stream2 = stream
            .try_clone()
            .map_err(|e| e.raw_os_error().unwrap_or(5))?;
        self.local.insert(
            id,
            (BufReader::new(stream).bytes(), BufWriter::new(stream2)),
        );
        Ok(())
    }
}

struct Devices {
    listener: TcpListener,
    counter: AtomicI64,
    accepted: RwLock<BTreeMap<Value, (TcpStream, SocketAddr)>>,
}
impl Devices {
    pub fn new(addr: impl ToSocketAddrs) -> Self {
        Self {
            listener: TcpListener::bind(addr).unwrap(),
            counter: Default::default(),
            accepted: Default::default(),
        }
    }
    pub fn accept(&self) -> Result<Value, i32> {
        let (stream, addr) = self
            .listener
            .accept()
            .map_err(|e| e.raw_os_error().unwrap_or(5))?;
        let id = self.counter.fetch_add(1, Ordering::Relaxed);
        self.accepted
            .write()
            .unwrap()
            .insert(id, (stream.into(), addr).into());
        Ok(id)
    }
    pub fn remove(&self, id: Value) {
        self.accepted.write().unwrap().remove(&id);
    }
    pub fn proxy(self: &Arc<Self>) -> LocalDevices {
        LocalDevices {
            devices: self.clone(),
            local: Default::default(),
        }
    }
}

struct Cores {
    max_cores: usize,
    counter: AtomicUsize,
    pub log: Sender<(Value, String)>,
    metrics: RwLock<BTreeMap<Value, CoreMetrics>>,
}

#[derive(Default, Clone)]
struct CoreMetrics {
    executed: u128,
    waiting: Option<WaitReason>,
}
#[derive(Debug, Clone, Copy)]
enum WaitReason {
    Read(Value),
    Write(Value),
    Accept,
}
impl Cores {
    fn new(max_cores: usize) -> (Arc<Self>, Receiver<(Value, String)>) {
        let (log, tx) = channel();
        (
            Arc::new(Self {
                max_cores,
                log,
                counter: AtomicUsize::new(0),
                metrics: Default::default(),
            }),
            tx,
        )
    }
    pub fn submit_metrics(&self, ci: Value, executed: usize, waiting: Option<WaitReason>) {
        // TODO maybe improve perf
        let mut m = self.metrics.write().unwrap();
        let m = m.entry(ci).or_default();
        m.executed += executed as u128;
        m.waiting = waiting
    }
    pub fn register(&self, mut core: Core) -> Result<(), i32> {
        match self.add() {
            Ok(ci) => {
                core.ci = ci;
                thread::Builder::new()
                    .name(format!("core{ci}"))
                    .spawn(move || {
                        core.run();
                        core.cores.remove(ci)
                    })
                    .unwrap();
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
    pub fn add(&self) -> Result<Value, i32> {
        let c = self.counter.fetch_add(1, Ordering::Relaxed);
        if c > self.max_cores {
            self.counter.fetch_sub(1, Ordering::Relaxed);
            Err(87) // too many users
        } else {
            Ok(c as Value)
        }
    }
    pub fn remove(&self, ci: Value) {
        self.metrics.write().unwrap().remove(&ci);
        self.counter.fetch_sub(1, Ordering::Relaxed);
    }
}


const OP_NOA = 0;
const OP_ADD = 1;
const OP_SUB = 2;
const OP_MUL = 3;
const OP_DIV = 4;
const OP_MOD = 5;
const OP_EQUAL = 6;
const OP_LESS = 7;
const OP_GREATER = 8;
const OP_LNOT = 9;
const OP_BNOT = 10;
const OP_XOR = 11;
const OP_OR = 12;
const OP_AND = 13;
const OP_SHL = 14;
const OP_SHR = 15;
const OP_MOVE = 16;
const OP_RSP = 17;
const OP_MSP = 18;
const OP_JUMP = 19;
const OP_CJUMP = 20;
const OP_RPC = 21;
const OP_SPAWN = 22;
const OP_HALT = 23;
const OP_ACCEPT = 24;
const OP_READ = 25;
const OP_WRITE = 26;
const OP_FADD = 27;
const OP_FSUB = 28;
const OP_FMUL = 29;
const OP_FDIV = 30;
const OP_FTOI = 31;
const OP_ITOF = 32;

const AM_IMMEDIATE = 0
const AM_BASE = 1
const AM_STACK = 2
const AM_STACK_MEMORY = 3

let step_interval;


let registers_el;

const memsize = 1024
const memory = []
for (let i = 0; i < memsize; i++) {
    memory.push(0)
}
const memory_els = []
const core = new Proxy({
    pc: 0,
    sp: memsize,
    bp: 0,
}, {
    set(a, key, value) {
        memory_els[a[key]]?.classList.remove(key)
        a[key] = value
        registers_el.textContent = `pc = ${a.pc.toString(16)}; sp = ${a.sp.toString(16)}; bp = ${a.bp.toString(16)}`
        memory_els[a[key]]?.classList.add(key)
        return true
    }
})


document.onpaste = (pasteEvent) => {
    const item = pasteEvent.clipboardData?.items[0];
    if (!item) return
    console.log(item);
    if (item.type.indexOf("text/plain") === 0) {
        item.getAsString(s => {
            const asm = JSON.parse(s.replace("\n", ""))
            for (let i = 0; i < asm.length; i++) {
                store(i, asm[i])
            }
            core.pc = 0;
            core.bp = 0;
            core.sp = memsize;
        })
    }
}

function store(addr, value) {
    if (addr < 0 || addr >= memsize) return
    memory[addr] = value
    const el = memory_els[addr];
    el.textContent = value.toString(16).padStart(4, "0")
    if (value == 0) el.classList.add("zero")
    else el.classList.remove("zero")
}
function load(addr) {
    return memory[addr]
}

function step() {
    const inst = load(core.pc++)
    const op = inst & 0xff

    const arg_in = (n) => {
        const arg = load(core.pc)
        const mode = (inst >> (8 + n * 2)) & 0b11
        core.pc += 1
        switch (mode) {
            case AM_IMMEDIATE:
                return arg;
            case AM_BASE:
                return load(arg + core.bp);
            case AM_STACK:
                return load(arg + core.sp);
            case AM_STACK_MEMORY:
                return load(load(arg + core.sp));
        }
    }
    const arg_out = (n, res) => {
        const mode = (inst >> (8 + n * 2)) & 0b11;
        const arg = load(core.pc);
        core.pc += 1;
        switch (mode) {
            case AM_IMMEDIATE:
                return;
            case AM_BASE:
                return store(arg + core.bp, res);
            case AM_STACK:
                return store(arg + core.sp, res);
            case AM_STACK_MEMORY:
                return store(load(arg + core.sp), res);
        }
    }

    switch (op) {
        case OP_NOA:
            log_op(`NOA`)
            break;
        case OP_ADD:
        case OP_FADD:
            log_op(`ADD`)
            arg_out(2, arg_in(0) + arg_in(1));
            break;
        case OP_SUB:
        case OP_FSUB:
            log_op(`SUB`)
            arg_out(2, arg_in(0) - arg_in(1));
            break;
        case OP_MUL:
        case OP_FMUL:
            log_op(`MUL`)
            arg_out(2, arg_in(0) * arg_in(1));
            break;
        case OP_DIV:
            log_op(`DIV`)
            arg_out(2, Math.floor(arg_in(0) / arg_in(1)));
            break;
        case OP_FDIV:
            log_op(`FDIV`)
            arg_out(2, arg_in(0) / arg_in(1));
            break;
        case OP_MOD:
            log_op(`MOD`)
            arg_out(2, arg_in(0) % arg_in(1));
            break;
        case OP_EQUAL:
            log_op(`EQUAL`)
            arg_out(2, +(arg_in(0) == arg_in(1)));
            break;
        case OP_LESS:
            log_op(`LESS`)
            arg_out(2, +(arg_in(0) < arg_in(1)));
            break;
        case OP_GREATER:
            log_op(`GREATER`)
            arg_out(2, +(arg_in(0) > arg_in(1)));
            break;
        case OP_LNOT:
            log_op(`LNOT`)
            arg_out(1, +!arg_in(0));
            break;
        case OP_BNOT:
            log_op(`BNOT`)
            arg_out(1, ~arg_in(0));
            break;
        case OP_XOR:
            log_op(`XOR`)
            arg_out(2, arg_in(0) ^ arg_in(1));
            break;
        case OP_OR:
            log_op(`OR`)
            arg_out(2, arg_in(0) | arg_in(1));
            break;
        case OP_AND:
            log_op(`AND`)
            arg_out(2, arg_in(0) & arg_in(1));
            break;
        case OP_SHL:
            log_op(`SHL`)
            arg_out(2, arg_in(0) << arg_in(1));
            break;
        case OP_SHR:
            log_op(`SHR`)
            arg_out(2, arg_in(0) >> arg_in(1));
            break;
        case OP_MOVE:
        case OP_ITOF:
        case OP_FTOI:
            log_op(`MOVE`)
            arg_out(1, arg_in(0));
            break;
        case OP_RSP:
            log_op(`RSP`)
            arg_out(0, core.sp);
            break;
        case OP_MSP:
            log_op(`MSP`)
            core.sp += arg_in(0);
            break;
        case OP_JUMP:
            log_op(`JUMP`)
            core.pc = arg_in(0);
            break;
        case OP_CJUMP:
            log_op(`CJUMP`)
            if (arg_in(0))
                core.pc = arg_in(1);
            else
                arg_in(1);
            break;
        case OP_RPC:
            log_op(`RPC`)
            arg_out(0, core.pc);
            break;
        case OP_SPAWN:
            arg_in(0);
            arg_in(1);
            arg_in(2);
            break;
        case OP_HALT:
            log_op(`HALT`)
            clearInterval(step_interval)
            set_status("halted")
            return;
        case OP_ACCEPT:
            arg_out(0, 0xff)
            break;
        case OP_READ:
            arg_in(0)
            arg_out(1, 0xff);
            arg_out(2, 0);
            break;
        case OP_WRITE:
            if (arg_in(0) == -1) {
                console.log(arg_in(1))
            } else {
                arg_in(1)
            }
            arg_out(2, 0);
            break;
        default:
            console.error("unknown instruction ", op);
            return;
    }
}

let last_ops_el
function log_op(s) {
    const d = document.createElement("li")
    d.textContent = s
    last_ops_el.prepend(d)
    while (last_ops_el.children.length > 8) {
        last_ops_el.children[last_ops_el.children.length - 1].remove()
    }
}

let status_el
function set_status(status) {
    status_el.classList.remove(status_el.textContent.toLowerCase())
    status_el.textContent = status.toUpperCase()
    status_el.classList.add(status_el.textContent.toLowerCase())
}

document.addEventListener("DOMContentLoaded", () => {
    const step_button = document.createElement("button")
    step_button.textContent = "Step"
    step_button.onclick = step
    document.body.append(step_button)

    const run = speed => {
        set_status("running")
        clearInterval(step_interval)
        step_interval = setInterval(step, 1000 / speed)
    }


    for (const speed of [1, 10, 100, 1000]) {
        const run1_button = document.createElement("button")
        run1_button.textContent = `Run ${speed}Hz`
        run1_button.onclick = () => run(speed)
        document.body.append(run1_button)
    }
    const stop_button = document.createElement("button")
    stop_button.textContent = "Stop"
    stop_button.onclick = () => {
        set_status("stopped")
        clearInterval(step_interval)
    }
    document.body.append(stop_button)

    status_el = document.createElement("span")
    status_el.classList.add("status")
    status_el.textContent = "idle"
    document.body.append(status_el)

    registers_el = document.createElement("ul")
    registers_el.classList.add("regs")
    document.body.append(registers_el)

    set_status("idle")

    const table = document.createElement("table")
    document.body.append(table)
    for (let row = 0; row < 32; row++) {
        const row = document.createElement("tr")
        table.append(row)
        for (let col = 0; col < 32; col++) {
            const el = document.createElement("td")
            memory_els.push(el)
            el.textContent = "0000"
            el.classList.add("zero")
            row.append(el)
        }
    }

    last_ops_el = document.createElement("ul")
    last_ops_el.classList.add("lastops")
    document.body.append(last_ops_el)
})

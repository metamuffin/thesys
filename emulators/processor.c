#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

typedef int64_t value;
enum opcode {
  OP_NOA,
  OP_ADD,
  OP_SUB,
  OP_MUL,
  OP_DIV,
  OP_MOD,
  OP_EQUAL,
  OP_LESS,
  OP_GREATER,
  OP_LNOT,
  OP_BNOT,
  OP_XOR,
  OP_OR,
  OP_AND,
  OP_SHL,
  OP_SHR,
  OP_MOVE,
  OP_RSP,
  OP_MSP,
  OP_JUMP,
  OP_CJUMP,
  OP_RPC,
  OP_SPAWN,
  OP_HALT,
  OP_ACCEPT,
  OP_READ,
  OP_WRITE,
  OP_FADD,
  OP_FSUB,
  OP_FMUL,
  OP_FDIV,
  OP_FTOI,
  OP_ITOF,
};
enum address_mode {
  AM_IMMEDIATE,
  AM_BASE,
  AM_STACK,
  AM_STACK_MEMORY,
};

value *memory;
value memory_size;
int sockfd;
int threads = 1;

value load(value address) {
  if (address >= 0 && address < memory_size)
    return memory[address];
  else {
    printf("WARN: out of bounds load from %li\n", address);
    return ~0;
  }
}
void store(value address, value val) {
  // printf("store addr=%li val=%li\n", address, val);
  if (address >= 0 && address < memory_size)
    memory[address] = val;
  else {
    printf("WARN: out of bounds store to %li\n", address);
  }
}

value load(value address);
void store(value address, value val);

value input_arg(value inst, value *pc, value sp, value bp, int n) {
  enum address_mode mode = (inst >> (8 + n * 2)) & 0b11;
  value arg = load(*pc);
  // printf("input pc=%li sp=%li, n=%i, inst=%li, mode=%i\n", *pc, sp, n, inst,
  //        mode);
  *pc += 1;
  switch (mode) {
  case AM_IMMEDIATE:
    return arg;
  case AM_BASE:
    return load(arg + bp);
  case AM_STACK:
    return load(arg + sp);
  case AM_STACK_MEMORY:
    return load(load(arg + sp));
  }
}
void output_arg(value inst, value *pc, value sp, value bp, int n, value res) {
  enum address_mode mode = (inst >> (8 + n * 2)) & 0b11;
  value arg = load(*pc);
  // printf("input pc=%li sp=%li, n=%i, inst=%li, mode=%i\n", *pc, sp, n, inst,
  //        mode);
  *pc += 1;
  switch (mode) {
  case AM_IMMEDIATE:
    return;
  case AM_BASE:
    return store(arg + bp, res);
  case AM_STACK:
    return store(arg + sp, res);
  case AM_STACK_MEMORY:
    return store(load(arg + sp), res);
  }
}
#define ARG_IN(n) input_arg(inst, &pc, sp, bp, n)
#define ARG_OUT(n, x) output_arg(inst, &pc, sp, bp, n, x)

void core(value pc, value sp, value bp) {
  char buf;
  while (1) {
    value inst = load(pc++);
    enum opcode op = inst & 0xff;
    value x, y, z;
    // printf("op=%i\n", op);
    switch (op) {
    case OP_NOA:
      break;
    case OP_ADD:
      ARG_OUT(2, ARG_IN(0) + ARG_IN(1));
      break;
    case OP_SUB:
      ARG_OUT(2, ARG_IN(0) - ARG_IN(1));
      break;
    case OP_MUL:
      ARG_OUT(2, ARG_IN(0) * ARG_IN(1));
      break;
    case OP_DIV:
      ARG_OUT(2, ARG_IN(0) / ARG_IN(1));
      break;
    case OP_MOD:
      ARG_OUT(2, ARG_IN(0) % ARG_IN(1));
      break;
    case OP_EQUAL:
      ARG_OUT(2, ARG_IN(0) == ARG_IN(1));
      break;
    case OP_LESS:
      ARG_OUT(2, ARG_IN(0) < ARG_IN(1));
      break;
    case OP_GREATER:
      ARG_OUT(2, ARG_IN(0) > ARG_IN(1));
      break;
    case OP_LNOT:
      ARG_OUT(1, !ARG_IN(0));
      break;
    case OP_BNOT:
      ARG_OUT(1, ~ARG_IN(0));
      break;
    case OP_XOR:
      ARG_OUT(2, ARG_IN(0) ^ ARG_IN(1));
      break;
    case OP_OR:
      ARG_OUT(2, ARG_IN(0) | ARG_IN(1));
      break;
    case OP_AND:
      ARG_OUT(2, ARG_IN(0) & ARG_IN(1));
      break;
    case OP_SHL:
      ARG_OUT(2, ARG_IN(0) << ARG_IN(1));
      break;
    case OP_SHR:
      ARG_OUT(2, ARG_IN(0) >> ARG_IN(1));
      break;
    case OP_MOVE:
      ARG_OUT(1, ARG_IN(0));
      break;
    case OP_RSP:
      ARG_OUT(0, sp);
      break;
    case OP_MSP:
      sp += ARG_IN(0);
      break;
    case OP_JUMP:
      pc = ARG_IN(0);
      break;
    case OP_CJUMP:
      if (ARG_IN(0))
        pc = ARG_IN(1);
      else
        ARG_IN(1);
      break;
    case OP_RPC:
      ARG_OUT(0, pc);
      break;
    case OP_SPAWN:
      if (threads < 16) {
        threads++;
      } else {
        fprintf(stderr, "too many threads\n");
        continue;
      }
      x = ARG_IN(0), y = ARG_IN(1);
      z = fork();
      if (z < 0) {
        perror("fork failed");
        return;
      } else if (z == 0) {
        pc = x;
        sp = y;
        bp = 0;
      }
      break;
    case OP_HALT:
      return;
    case OP_ACCEPT:
      x = accept(sockfd, NULL, NULL);
      if (x < 0) {
        perror("accept failed");
        return;
      }
      ARG_OUT(0, x); // TODO dont pass fd
      break;
    case OP_READ:
      x = ARG_IN(0);
      if (x == 0)
        z = read(x, &buf, 1);
      else
        z = recv(x, &buf, 1, 0);
      if (z < 0) {
        perror("read failed");
        close(x);
        return;
      }
      z = z == 0;
      y = buf;
      ARG_OUT(1, y);
      ARG_OUT(2, z);
      break;
    case OP_WRITE:
      x = ARG_IN(0);
      buf = ARG_IN(1);
      if (x == 1 || x == 2)
        z = write(x, &buf, 1);
      else
        z = send(x, &buf, 1, 0);
      if (z < 0) {
        perror("write failed");
        close(x);
        z = 1;
      } else {
        z = 0;
      }
      ARG_OUT(2, z);
      break;
    default:
      fprintf(stderr, "unknown instruction %i\n", op);
      return;
    }
  }
}

int load_initial(char *path) {
  struct stat s;

  int fd = open(path, O_RDONLY);
  if (fd < 0) {
    perror("open failed");
    return -1;
  }

  int res = fstat(fd, &s) < 0;
  if (res < 0) {
    perror("stat failed");
    return -1;
  }
  int size = s.st_size;

  char *source = malloc(size);
  if (source == NULL) {
    perror("malloc failed");
    return -1;
  }
  int size_read = read(fd, source, size);
  if (size_read < 0) {
    perror("read failed");
    return -1;
  }

  if (size < 3) {
    fprintf(stderr, "source too short\n");
    close(fd);
    return -1;
  }
  if (source[0] == '[') {
    source += 1;
    size -= 1;
  }
  if (source[size - 1] == '\n')
    size -= 1;
  if (source[size - 1] != ']')
    size -= 1;

  char *token;
  char *state;
  int position = 0;
  while ((token = strtok_r(source, ",", &state))) {
    value x;
    if (sscanf(token, "%li", &x) != 1) {
      fprintf(stderr, "invalid integer\n");
      close(fd);
      return -1;
    }
    memory[position++] = x;
    if (position >= memory_size) {
      fprintf(stderr, "program does not fit into memory\n");
      close(fd);
      return -1;
    }
    source = NULL;
  }
  return 0;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("usage: processor <port> <initial_program>\n");
    return EXIT_FAILURE;
  }
  struct sockaddr_in addr = {0};
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(argv[1]));
  addr.sin_addr.s_addr = inet_addr("0.0.0.0");
  sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (sockfd < 0) {
    perror("socket failed");
    close(sockfd);
    return EXIT_FAILURE;
  }

  int res = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));
  if (res < 0) {
    perror("bind failed");
    close(sockfd);
    return EXIT_FAILURE;
  }

  res = listen(sockfd, 1);
  if (res < 0) {
    perror("listen failed");
    close(sockfd);
    return EXIT_FAILURE;
  }

  memory_size = 1 << 24;
  memory = malloc(memory_size * sizeof(value));
  if (!memory) {
    fprintf(stderr, "cannot allocate enough memory\n");
    close(sockfd);
    return EXIT_FAILURE;
  }

  for (int i = 0; i < memory_size; i++) {
    memory[i] = 0;
  }

  if (load_initial(argv[2]) < 0) {
    close(sockfd);
    return EXIT_FAILURE;
  }

  core(0, memory_size, 0);
  close(sockfd);
  return EXIT_SUCCESS;
}

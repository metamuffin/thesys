
= Compiler

TODO just notes for now

== Stages
Intermediate AST representation types are noted in between stages.

+ Parse Source code to AST
  // - ```rs Data=(), Symbol=String, TypeSymbol=String```
+ Resolve type symbols
  // - ```rs Data=(), Symbol=String, TypeSymbol=ID```
+ Symbol resolver / type inference loop
  + Resolve value symbols as far as possible
  + Infer types
  // - ```rs Data=Type<Option<ID>>, Symbol=Either<ID, String>, TypeSymbol=ID```
+ Unwrap AST
  // - ```rs Data=Type<ID>, Symbol=ID, TypeSymbol=ID```
+ Code generation

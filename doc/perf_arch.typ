#import "@preview/cetz:0.2.1"

#let bitcount = counter("bits")
#let buffercount = counter("buffers")
#let compcount = counter("comps")

= Performant Architecture <perf_arch>

== Introduction

Current CPUs achieve faster computing speeds by parallely executing instructions with pipelining, speculative execution and SIMD.
This allows the CPU to utilize all internal components at all times, increasing speed if they do useful work. Pipelining and speculative execution especially are prone to hardware bugs and introduce great complexity into the CPU and should therefore be avoided //for Thesys.

_Very Long Instruction Word_ (VLIW) architectures don't perform these optimizations at runtime, elimnating the risk of error (at least in hardware), while concurrently utilizing all components.

== Description

Every clock cycle the CPU loads one instruction that specifies operation of every component during that cycle.
Every component has a buffered output for using the result in the next cycle. Inputs of the components are taken directly from the output buffer of the previous cycle.

== Components

When using all #locate(loc => compcount.final(loc).at(0)) components, the size of one instruction word needs to be at least #locate(loc => bitcount.final(loc).at(0)) bits.
There will be #locate(loc => buffercount.final(loc).at(0)) addressable output buffers (#(locate(loc => (
  calc.ceil(calc.log(buffercount.final(loc).at(0), base: 2))
)))-bit address).

#let k = 4
#let imm = 32

#let categories = (
  (
    title: [Computing],
    components: (
      (name: [Zero Constant], outs: ([Zero],)),
      (name: [One Constant], outs: ([One],)),
      
      (
        name: [ALU: Add/Sub],
        desc: [Performs 64-bit integer addition or subtraction.],
        ins: (
          (w: 1, k: [Subtract mode]),
          (w: k, k: [Left side]),
          (w: k, k: [Right side]),
        ),
        outs: ([Sum],[Is Zero], [Is Positive])
      ),

      (
        name: [ALU: Mul/Div],
        desc: [Performs 64-bit integer multiplication or division.],
        ins: (
          (w: 1, k: [Divide mode]),
          (w: k, k: [Left side]),
          (w: k, k: [Right side]),
        ),
        outs:([Product],)
      ),

      (
        name: [ALU: Float Add/Sub], 
        desc: [Performs IEEE 754 Floating Pointer addition or subtraction in the Binary64 format.],
        ins: (
          (w: 1, k: [Subtract mode]),
          (w: k, k: [Left side]),
          (w: k, k: [Right side]),
        ),
        outs: ([Sum],), 
        latency: 2
      ),

      (
        name: [ALU: Float Mul/Div],
        desc: [Performs IEEE 754 Floating Pointer addition or subtraction in the Binary64 format.],
        ins: (
          (w: 1, k: [Divide mode]),
          (w: k, k: [Left side]),
          (w: k, k: [Right side]),
        ),
        outs: ([Product],),
        latency: 2
      ),

      (
        name: [ALU: Binary operations],
        desc: [
          Modes:
          + Shift Left
          + Shift Right
          + Or
          + And
          + Xor
          + Not
        ],
        ins: (
          (w: 3, k: [Mode]),
          (w: k, k: [Left side]),
          (w: k, k: [Right side]),
        ),
        outs: ([Result],)
      ),
      
      (
        name: [Instruction Immediate Argument],
        desc: [Provides the parameter as an output buffer without any latency.],
        latency: 0,
        ins: ((w:imm, k: [Value]),),
        outs: ([Value],)
      ),
    )
  ),
  (
    title: [Memory],
    desc: [The registers are implemented as the "identity component", taking in a value from somewhere and providing it as-is for the next cycle.],
    components: (
      ..("A", "B", "C", "D", "E", "F").map(e =>
        (name: [Register #e],ins: ((w: k, k: [Value]),), outs: ([Previous Value],))
      ),
      
      ..("A", "B").map(e =>
        (name: [Memory Loader #e], ins: ((w: k, k: [Address]),), outs: ([Value],), latency: 4)
      ),
      
      (
        name: [Memory Storer],
        ins: (
          (w: k, k: [Address]),
          (w: k, k: [Value]),
        ),
        latency: 4
      ),
    )
  ),
  (
    title: [Control Flow],
    components: (
      (
        name: [Program counter],
        outs: ([The Program counter],),
      ),

      (
        name: [Flow controller],
        ins: (
          (w: k, k: [Jump Address]),
          (w: k, k: [Jump Condition]),
        ),
        latency: 4
      ),
    )
  )
).map(c => {
  c.components = c.components.map(x => {
    if "ins" not in x {x.ins = ()}
    if "outs" not in x {x.outs = ()}
    x
  })
  c
})

#let components_flat = categories.map(c => c.components).flatten()

#table(
  columns: (1fr,1fr,1fr),
  stroke: none,
  column-gutter: 5pt,
  gutter: 5pt,
  ..categories.map(c => (
      table.cell(colspan: 3)[
        === #c.title
        #c.at("desc", default: [])
      ],
    c.components.map(c => (
      table.cell(colspan: 3)[
        ==== #c.name
        #compcount.step()
        #c.at("desc", default: [])
      ],
      if c.ins.len() > 0 { table.cell(fill: rgb("00ff0040"), inset: 5pt, [
        Parameters:
        #for i in c.ins [
          - #(i.w)-bit #i.k
          #bitcount.update(x => x + i.w)
        ]
      ])} else { [] },
      if c.outs.len() > 0 { table.cell(fill: rgb("4000f040"), inset: 5pt, [
        Output buffers:
        #for o in c.outs [
          - #o
          #buffercount.step()
        ]
      ])} else { [] },
      if "latency" in c { table.cell(fill: rgb("ff000040"), inset: 5pt, [
        #if c.latency < 1 [No output latency] else if c.latency > 1 [Output latency: #c.latency cycles]
      ])} else { [] },
    )).flatten()
  )).flatten()
)

== Instruction Binary Format

#figure(
  caption: [Thesys Performant Architecture Instruction Binary Format],
  table(
    columns: 3,
    rows: 10pt,
    ..{
      let bitcounter = counter("diag_bit")
      components_flat.filter(c => c.ins.len() > 0).map(c => {
        let tsize = c.ins.map(i => i.w).sum()
        (
          table.cell(x: 0, rowspan: tsize, fill: rgb("ffff0040"), c.name),
          ..c.ins.map(i => table.cell(x: 1, fill: rgb("00ff0040"), rowspan: i.w)[
            #align(left, text(top-edge: if i.w <= 1 {3pt} else {5pt})[#{i.w}-bit #i.k])
          ]),
          ..c.ins.map(i => range(0, i.w).map(b => table.cell(x: 2, [
            #box(width: 30pt, text(size: 9pt, top-edge: 3pt)[Bit #bitcounter.display()])
            #bitcounter.step()
          ]))).flatten(),
        )
      }).flatten()
    }
  )
)

= Simple Architecture <simple_arch>

== Introduction

Thesys "Simple Architecture" uses a minimal traditional instruction set with low-level support for external communications. Instead of registers the stack is used to store intermediate results.

== Description

The Processor has a number of _Cores_ which execute instructions sequencially according to a program and _Memory_ which stores addressable 64-bit integers (_Values_).
The memory has a fixed size. This manifests through a maximum address after which the memory is not writeable.
All cores execute concurrently -- their syncronization is undefined, they may execute at different rates.

Each core has three internal registers: _Stack Pointer_ (SP), _Base Pointer_ (BP) and _Program Counter_ (PC) each 64-bit in size.
These cores repeatedly load an instruction (one value) from memory where PC points.
Hereby the number of cores is not fixed during runtime.
Boolean operations treat 0 as false and all other numbers as true.

After power-up the processor starts with a single core with $"PC" = 0$, $"BP" = 0$ and SP at highest address supported by the current memory.

On each clock cycle the Processor run the following steps:
- Load one Value from Memory at $"PC"$.
- This value will be decoded into _Opcode_ and _Addressing Modes_// and _Operand Sizes_
- The $"PC"$ is incremented by 1
- All arguments are loaded, incrementing $"PC"$ accordingly.
- The instruction is executed according to @instructions where $x$, $y$ and $z$ are arguments 1 through 3 in order. // Operation work with 64-bit values exclusively. Smaller operands are padded/truncated as necessary.

=== Instruction Binary Format

Opcode and Addressing Mode are packed into a single value in the following way:  

#figure(
  table(
    columns: (1fr,) * 32,
    table.header(..range(32).map(e => [#(31 - e)])),
    table.cell(colspan: 18)[Unused],
    table.cell(colspan: 2)[$"AM"_1$],
    table.cell(colspan: 2)[$"AM"_2$],
    table.cell(colspan: 2)[$"AM"_3$],
    // table.cell(colspan: 2)[$"OS"_1$],
    // table.cell(colspan: 2)[$"OS"_2$],
    // table.cell(colspan: 2)[$"OS"_3$],
    table.cell(colspan: 8)[Opcode],
  ),
  caption: "Instruction Binary Format"
)

// === Operand Sizes

// Every arguments can have a different size.

// 0. 8-bit
// 1. 16-bit
// 2. 32-bit
// 3. 64-bit

=== Addressing Modes <addressingmodes>

Every argument ($x$) can be intepreted according to one of four addressing modes:
0. Immediate: $x$
1. Base relative memory pointer: $M("BP"+x)$
2. Stack relative pointer: $M("SP"+x)$
3. Stack relative memory pointer pointer™: $M(M("SP"+x))$

=== Opcodes

#let ic = counter("inst_opcode")
#let c()=[#ic.display() #ic.step()]
#figure(
  caption: [The Thesys Instruction Set],
  table(
    columns: 5,
    table.header[][*Opcode*][*Mnemonic*][*Action*][*Description*],
    [],c(),[`noa`], [],[No action],
    [*Arithmetic*],c(),[`add`],[$z <- x+y$],[Addition],
    [],c(),[`sub`], [$z <- x-y$],[Subtractions],
    [],c(),[`mul`], [$z <- x*y$],[Multiplication],
    [],c(),[`div`], [$z <- x/y$], [Integer Division],
    [],c(),[`mod`], [$z <- x mod y$], [Modulo / Euclidean Remainder],
    [],c(),[`equal`], [$z <- cases(1 quad x = y,0 quad "otherwise")$],[Compare if equal],
    [],c(),[`less`], [$z <- cases(1 quad x < y,0 quad "otherwise")$],[Compare if less],
    [],c(),[`greater`], [$z <- cases(1 quad x > y,0 quad "otherwise")$],[Compare if greater],
    [],c(),[`lnot`], [$y <- not x$],[Logical not],
    [],c(),[`bnot`], [$y <- ~x$],[Bitwise not],
    [],c(),[`xor`], [$z <- x xor y$],[Binary (and also logical) xor],
    [],c(),[`or`], [$z <- x and y$],[Binary (and also logical) or],
    [],c(),[`and`], [$z <- x or y$],[Binary (and also logical) and],
    [],c(),[`shl`], [$z <- x << y$],[Bit shift left],
    [],c(),[`shr`], [$z <- x >> y$],[Bit shift right],
    [*Memory*],c(),[`move`],[$y <- x$], [Move a value],
    [],c(),[`rsp`],[$x <- "SP"$], [Read stack pointer],
    [],c(),[`msp`],[$"SP" <- "SP" + x$], [Move stack pointer],
    [*Control flow*],c(),[`jump`],[$"PC" <- x$],[ Store program counter],
    [],c(),[`cjump`], [$"PC" <- cases(y quad x!=0,"PC" quad x=0)$],[Conditional jump],
    [],c(),[`rpc`],[$x <- "PC"$], [Read program counter],
    [],c(),[`spawn`],[], [Creates a new core with $"PC" = x, "SP" = y$ using error flag #footnote[The error is set to a UNIX error number on error or zero on success] $z$],
    [],c(),[`halt`],[],[Removes this core],
    [*Input / Output*],c(),[`accept`],[$x <- "ID"$],[Waits for a new device. Returns identifier for that device.],
    [],c(),[`read`],[$y,z <- "Read"(x)$],[Wait for new data from device $x$, reads a single value $y$ with error flag $z$],
    [],c(),[`write`],[$z <- "Write"(x,y)$],[Send $y$ to device $x$; May block],
    [*Floating Point* #footnote[Implemented with IEEE Floats in Binary64 format.]],c(),[`fadd`],[$z = x+y$],[Fp. Addition],
    [],c(),[`fsub`], [$z <- x-y$],[Fp. Subtractions],
    [],c(),[`fmul`], [$z <- x*y$],[Fp. Multiplication],
    [],c(),[`fdiv`], [$z <- x/y$], [Fp. Division],
    [],c(),[`ftoi`], [$y <- "(int)" floor(x)$], [Convert float to integer],
    [],c(),[`itof`], [$y <- "(float)" x$], [Convert integer to float],
  )
) <instructions>

== Hardware Emulators

- `processor.rs`: Rust processor implementation. Has some output to view what is happening.
- `processor.c`: C processor implementation. Very portable.
- `processor.{js,html}`: Browser processor implementation with memory and state visualization. No support for I/O and additional cores.


= Software <software>

== Introduction

The Thesys operating system will conceptually work similar to traditional systems with a few changes:

- No syscalls: In hardware everything runs with same priviledges. Memory safety and program permissions are enforced at compile-time.
- No interupts for scheduling: Programs will be forced by the compiler to implement cooperative scheduling i.e. regular calls to `yield()`.
- No async#footnote[As in: Automatically converting functions to a state-machine that polls something to advance]: With the first two changes, threads are somewhat cheap and fast now, there is no need for complexity of async anymore. When some function takes time, it just takes its time; the internal code that blocks will register some callback and return execution to the scheduler until its done.

== Low-level

=== Calling convention

When calling a function the caller first writes the arguments and return PC below SP then jumps into the function.
Then callee first moves the SP down, does its work, moves the result above the locals, then moves the SP back up and jumps to the return PC right below the stack.
The caller can now access the result below the stack.

#figure(
  table(columns: 1,
    pad(y:15pt)[Locals of Function],
    pad(y:5pt)[Arguments of Function],
    pad(y:5pt)[Return values of Function],
    pad(y:-2pt)[Return PC],
    pad(y:15pt)[Locals of Caller],
  ),
  caption: [Stack diagram of the calling convention],
  supplement: "Diagram"
)

=== Slices

When working with slices of a memory region, two values will be used: one memory pointer to mark start (inclusive) and one integer that stores the number of elements (_Length_). The _Size_ is the size of the elements times the length.

== Devices

When devices connect to the processor they first send 4 values (magic byte) to identify their type. After that they start with bidirectional communication.

=== Screen (`SCRN`)

Reads rectangular pixel-graphic frames of size $320 times 180$ with RGBA pixel format and shows them to the user.
If supported, alpha should control the screens transparency.

=== Keyboard (`KYBD`)

Sends ASCII UTF-8 characters after connecting.

=== Browser (`GET `)

Not really a device but by handling HTTP requests a browser can be used to implement various devices. 

#show raw: x => if x.block { pad(x: 50pt, x) } else { x }

= Source Code

Source Code is heavily inspired by Rust.

Compilation is performed in multiple steps:
+ Parsing
+ Symbol resolving
+ Type inferrence
+ #text(fill: gray)[Syntax Lowering (not implemented)]
+ #text(fill: gray)[Borrow checker (not implemented)]
+ Code generation
+ #text(fill: gray)[Assembly Optimization (not implemented)]
+ Object serialization

== Language Consideration

- Static types for global symbols
  - Required to resolve recursive dependencies


== Grammar

This is an informal grammar -- it does not deal with whitespace.

```hs
file = { declaration }
declaration = struct | use | function | assembly
struct = "struct", ident, "{", { ident, ":", type }, "}"
function = visibility, "fn", ident, "(", { ident, ":", type }, ")", block
assembly = "ASSEMBLY {", ?assembly_body?, "}"
use = "use", qualified_ident

expression = block | literal_string | literal_number | literal_bool | binary_operator | let | struct_constructor | parens | while | if | assign | call
block = "{", [ { expression, ";" }, expression, [ ";" ] ] "}"
parens = "(", expression, ")"
while = "while", expression, block
if = "if", expression, block, ["else", block]
struct_constructor = type, "{", { ident, ":", type }, "}"
let = "let", ident, [":", type], "=", expression
assign = ident, "=", expression
call = expression, "(", { expression, "," }, ")"
literal_bool = "true" | "false"
literal_number = [ "-" ], number
literal_string = '"', { string_part } , '"'
  string_part = "\n" | "\r" | "\t" | "\\" | ("\x", hexdigit, hexdigit) | ?char not blackslash not quote?

type = ident, ["<", [ { type, ";" }, type, ";" ],  ">"]
qualified_ident = { ident, "::" }, ident
ident = ?alphanumeric? | "_"
visibility = [ "pub" ]
```

== Symbols

When defining symbols with ```hs let```, they become available within the current scope after declaration.
Defining another variable with the same name is allowed and shadows the previous symbol.
Nesting scopes can be achieved with block statements (and others) -- variables from outside shadowed within are not affected.

== Version 2

```nim
proc parse(x: string): number = 5
proc parse(x: string): string = 0

proc add<T: Number>(x: T, y: T): T = 10

type Thing = object(x: i32, y: i32);

type MaybeThing = enum (
  Yes: Thing,
  No: object()
)

proc show(self: &'a' Thing): string {
  
}


type Parse = fn()
trait Parse = { parse }

fn hello(x: i32, y: string) = x + y.parse()

```

#show ref: x => if counter("main").get().at(0) < 1 [] else {x}
#show raw: x => if x.block { pad(x: 50pt, x) } else { x }

= Assembly <assembly>

The following sections include EBNF grammars for formal syntax definitions. 

== Reference

Assmbly files contain many lines with usually one instruction per line or other definitions like labels, literal data or comments. 
Lines can also be empty, in which case they are ignored.

```hs
file = { line, ?newline? }
line = instruction | label_def | literal_data | comment | ""
```

=== Instructions

Instructions are written with their mnemonic (see @instructions) followed by all arguments.

```hs
instruction = mnemonic [" ", arg [" ", arg [" ", arg]]];
```

=== Arguments

Arguments are prefixed with one character that indicates the access mode and if it should be interpreted as a number or a label.
Label arguments can be suffixed with `+` and a number to offset the location. Numbers use Base-16.

- `#` Immediate
- `@` Immediate label
- ` ` Stack-relative
- `_` Stack-relative label
- `*` Stack-relative pointer 
- `&` Stack-relative pointer label
- `$` Base-relative
- `%` Base-relative label

```hs
arg = immediate | immediate_label | stack_relative | stack_relative_pointer | stack_relative_label | base_relative | base_relative_label;
immediate = "#", number;
immediate_label = "@", ident, [ "+", number ];
stack_relative = number;
stack_relative_label = "_", ident, [ "+", number ];
stack_relative_pointer = "*", number;
stack_relative_pointer_label = "&", ident, [ "+", number ];
base_relative = "$", number;
base_relative_label = "%", ident, [ "+", number ];
```

== Labels

Labels for jumping around in code are defined with a line starting with `@` followed by an identifier and take on the position in the code as a value.
Labels for naming stack offsets are defined similarly on a line starting in `=` followed by an identifier and the value that they should take.

```hs
label_def = "@", ident | "=", ident, number;
```

== Literal Data

Literal data can be inserted with lines starting in `#` (inspired by immediate arguments) followed by a `,`-seperated list of numbers.

```hs
literal_data = '"', { byte_no_newline } | "#", number, { ",", number };
```

== Comments

Comments start with `//`.

```hs
comment = "//", { byte_no_newline };
```

== Additional Definitions

```hs
number = [ "-" ] { digit };
ident = { letter };
letter = "A" | "B" | "C" | "D" | "E" | "F" | "G" 
       | "H" | "I" | "J" | "K" | "L" | "M" | "N"
       | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
       | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
       | "c" | "d" | "e" | "f" | "g" | "h" | "i"
       | "j" | "k" | "l" | "m" | "n" | "o" | "p"
       | "q" | "r" | "s" | "t" | "u" | "v" | "w"
       | "x" | "y" | "z";
digit  = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7"
       | "8" | "9" | "a" | "b" | "c" | "d" | "e" | "f";
```

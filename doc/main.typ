#set heading(numbering: "1.1.1.a -")
#set text(font: "New Computer Modern")
#set document(title: "Thesys")

#let cgrad = gradient.linear(rgb(200,0,100),rgb(100,0,200));
#let cgradrot = gradient.linear(angle: -40deg, rgb(200,0,100),rgb(100,0,200));

#let todo(x) = box(radius: 3pt, fill: red, pad(rest: 5pt, text(fill: white, [*To-do*: #x])))

#[
  #set page(fill: cgradrot, footer: align(right)[built #datetime.today().display()])
  #set text(fill: white)
  #set text(font: " ")
  #align(center + horizon)[
    #text(size: 45pt)[Thesys]
    #v(-20pt)
    _Banging rocks together since 2022_
    #v(100pt)
    by metamuffin
  ]
]

#show "Thesys": x => box(text(
  font: "_",
  fill: cgrad,
  x
))
#show figure: set block(breakable: true)
#show heading: h => if h.level > 1 or h.numbering == none { h } else { 
  pagebreak()
  pad(top: 20pt, bottom: 40pt, align(center, 
    text(weight: 1, size: 20pt)[Thesys _ #h.body _]
  ))
}

#set page(
  number-align: bottom + center,
  numbering: "1 / 1"
)

#pad(x: 30pt, outline(indent: 1em))
#counter("main").update(1)

= Introdruction <introduction>

Thesys is an attempt to design a computing system from the ground up.
The project was started out of frustration with the currently popular software (and hardware) stack's problems.

The name Thesys was derived from "The System" because of its aim in replacing everything.

This document is subdivided into multiple chapters:

- @introduction: _Introduction_ -- You are reading it right now...
- @simple_arch: _Simple Architecture_ -- A simple processor architecture for bootstrapping and easy emulation.
- @perf_arch: _Performant Architecture_ -- A proposed architecture that can be efficiently implemented in hardware.
- @assembly: _Assembly_ -- Reference for the custom assembly syntax used in Simple Architecture.
- @software: _Software_ -- Documenting the standards used for software development.

#include "simple_arch.typ"
#include "perf_arch.typ"
#include "software.typ"
#include "compiler.typ"
#include "assembly.typ"
#include "sourcecode.typ"

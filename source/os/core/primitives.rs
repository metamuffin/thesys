
extern pub fn halt();
ASSEMBLY {
    :Gfun_halt
    halt
}

extern pub fn spawn(sp: i64, pc: i64) -> i64;
ASSEMBLY {
    :Gfun_spawn
    msp #-4
    spawn 0 1 2
    msp #4
    jump -1
}

extern pub fn accept() -> i64;
ASSEMBLY {
    :Gfun_accept
    msp #-2
    accept 0
    msp #2
    jump -1
}

// pub struct ReadResult { data: i64, error: i64 }

// extern pub fn read(dev: i64) -> ReadResult;
// ASSEMBLY {
//     :Gfun_read
//     msp #-4
//     read 0 1 2
//     msp #4
//     jump -1
// }

extern pub fn write(dev: i64, value: i64) -> i64;
ASSEMBLY {
    :Gfun_write
    msp #-4
    write 0 1 2
    msp #4
    jump -1
}

extern pub fn unsafe_memory_write(a: i64, b: i64) -> unit;
ASSEMBLY {
    :Gfun_unsafe_memory_write
    msp #-3
    move 1 *0
    msp #3
    jump -1
}

extern pub fn unsafe_memory_read(a: i64) -> i64;
ASSEMBLY {
    :Gfun_unsafe_memory_read
    msp #-3
    move *0 1
    msp #3
    jump -1
}

extern pub fn transmute_str_i64(a: cstr) -> i64;
ASSEMBLY {
    :Gfun_transmute_str_i64
    msp #-3
    move 0 1
    msp #3
    jump -1
}

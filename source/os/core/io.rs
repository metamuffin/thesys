use primitives::write;
use primitives::transmute_str_i64;
use primitives::unsafe_memory_read;

fn print(s: cstr) {
    let p = transmute_str_i64(s);
    let done = false;
    while done == false {
        let x = unsafe_memory_read(p);
        done = done | x == 0;
        done = done | (write(1, x) == 0) == false;
        p = p + 1;
    }
}


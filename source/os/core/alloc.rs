
struct RawPointer(i64);

struct Box<T> {
    pointer: RawPointer,
}

fn new<T>(value: T) -> Box<T> {
    let pointer = allocate(size_of(value));
    (transmute_pointer_ref_mut(pointer).assign)(value);
    Box { pointer: pointer }
}

fn allocate(size: usize) -> RawPointer {
    
}


extern fn transmute_pointer_ref<T>(ptr: RawPointer) -> ref<T>;
extern fn transmute_pointer_ref_mut<T>(ptr: RawPointer) -> ref_mut<T>;

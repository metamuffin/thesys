use crate::{
    error::{Context, Error, Result},
    serror,
};
use std::{collections::BTreeMap, str::FromStr};

#[derive(Default)]
pub struct LinkableObject {
    pub const_exports: Vec<(String, i64)>,
    pub label_exports: Vec<(String, usize)>,
    pub label_uses: Vec<(String, usize)>,
    pub data: Vec<i64>,
}

fn parse_kv<T: FromStr>(s: &str) -> Result<Vec<(String, T)>>
where
    <T as FromStr>::Err: Send + Sync + std::error::Error + 'static,
{
    s.split(",")
        .filter(|tok| !tok.is_empty())
        .map(|tok| {
            let (name, position) = tok.split_once("=").ok_or(serror!("kv-seperator missing"))?;
            Ok::<_, Error>((
                name.to_owned(),
                position
                    .parse()
                    .map_err(|e| Error::simple(format!("{e}")))
                    .context("value malformed")?,
            ))
        })
        .try_collect()
}

impl LinkableObject {
    pub fn parse(s: &str) -> Result<Self> {
        let (const_exports, rest) = s.split_once("\n").ok_or(serror!("line missing"))?;
        let (label_exports, rest) = rest.split_once("\n").ok_or(serror!("line missing"))?;
        let (label_uses, data) = rest.split_once("\n").ok_or(serror!("line missing"))?;
        let data = data.trim();
        Ok(Self {
            const_exports: parse_kv(const_exports).context("const exports section")?,
            label_exports: parse_kv(label_exports).context("label exports section")?,
            label_uses: parse_kv(label_uses).context("label uses section")?,
            data: data
                .split(",")
                .filter(|tok| !tok.is_empty())
                .map(|tok| tok.parse().map_err(|e| Error::simple(format!("{e}"))))
                .try_collect()
                .context("data section")?,
        })
    }
    pub fn dump(&self) -> String {
        format!(
            "{}\n{}\n{}\n{}",
            self.const_exports
                .iter()
                .map(|(k, v)| format!("{k}={v}"))
                .collect::<Vec<_>>()
                .join(","),
            self.label_exports
                .iter()
                .map(|(k, v)| format!("{k}={v}"))
                .collect::<Vec<_>>()
                .join(","),
            self.label_uses
                .iter()
                .map(|(k, v)| format!("{k}={v}"))
                .collect::<Vec<_>>()
                .join(","),
            self.data
                .iter()
                .map(|v| format!("{v}"))
                .collect::<Vec<_>>()
                .join(","),
        )
    }
}

pub fn link_objects(objects: Vec<(String, LinkableObject)>) -> Result<Vec<i64>> {
    let mut label_values = BTreeMap::new();
    let mut const_values = BTreeMap::new();
    let mut label_uses = Vec::new();

    let mut out = Vec::new();

    for (obname, ob) in objects {
        let offset = out.len();
        const_values.extend(
            ob.const_exports
                .into_iter()
                .map(|(k, v)| (maybe_namespace(&obname, &k), v)),
        );
        label_values.extend(
            ob.label_exports
                .into_iter()
                .map(|(k, v)| (maybe_namespace(&obname, &k), (v + offset) as i64)),
        );
        label_uses.extend(
            ob.label_uses
                .into_iter()
                .map(|(k, v)| (maybe_namespace(&obname, &k), v + offset)),
        );
        out.extend(ob.data)
    }

    for (label, target) in label_uses {
        let value = label_values
            .get(&label)
            .or(const_values.get(&label))
            .ok_or(serror!("label {label:?} does not exist"))?;
        out[target] += value;
    }

    Ok(out)
}

fn maybe_namespace(obname: &str, sname: &str) -> String {
    if sname.starts_with("G") {
        sname.to_string()
    } else {
        format!("__{obname}_{sname}")
    }
}

use crate::{
    ast::{BinaryOp, ExpressionKind, LifetimeSpec, Literal, TypeA},
    bail,
    error::{Context, Error, Result},
    parser::span::Spanned,
    serror,
    symbols::{SymbolFrame, ID},
    Either, Stage3Tree,
};
use std::{
    collections::BTreeMap,
    iter::once,
    sync::atomic::{AtomicUsize, Ordering},
};

static CHANGED: AtomicUsize = AtomicUsize::new(0);

pub type TypeStore = BTreeMap<ID, TypeDecl>;
pub type ValueTypeStore = BTreeMap<ID, TypeA<ID>>;
pub type PartialValueTypeStore = BTreeMap<ID, TypeA<Option<ID>>>;

#[derive(Debug, Clone)]
pub enum TypeDecl {
    Struct {
        fields: Vec<(String, ID, TypeA<ID>)>,
    },
    Unit,
    I64,
    F64,
}

pub struct Types {
    pub unit: TypeA<ID>,
    pub bool: TypeA<ID>,
    pub i64: TypeA<ID>,
    pub f64: TypeA<ID>,
    pub cstr: TypeA<ID>,
    pub function: ID,
    pub reference: ID,
}
impl Types {
    pub fn register(globals: &mut SymbolFrame, s_type: &mut TypeStore) -> Self {
        let unit = globals
            .new_type(Spanned::unspanned("unit".to_string()))
            .unwrap();
        s_type.insert(unit, TypeDecl::Unit);
        let bool = globals
            .new_type(Spanned::unspanned("bool".to_string()))
            .unwrap();
        s_type.insert(bool, TypeDecl::I64);
        let f64 = globals
            .new_type(Spanned::unspanned("f64".to_string()))
            .unwrap();
        s_type.insert(f64, TypeDecl::F64);
        let i64 = globals
            .new_type(Spanned::unspanned("i64".to_string()))
            .unwrap();
        s_type.insert(i64, TypeDecl::I64);
        let cstr = globals
            .new_type(Spanned::unspanned("cstr".to_string()))
            .unwrap();
        s_type.insert(cstr, TypeDecl::I64);
        let function = globals
            .new_type(Spanned::unspanned("function".to_string()))
            .unwrap();
        s_type.insert(function, TypeDecl::I64);
        let reference = globals
            .new_type(Spanned::unspanned("reference".to_string()))
            .unwrap();
        s_type.insert(reference, TypeDecl::I64);
        Self {
            bool: TypeA::simple(bool),
            f64: TypeA::simple(f64),
            i64: TypeA::simple(i64),
            unit: TypeA::simple(unit),
            cstr: TypeA::simple(cstr),
            function,
            reference,
        }
    }
}

pub fn resolve_types(
    ast: &mut Stage3Tree,
    s_type: &TypeStore,
    s_value_ptype: &mut PartialValueTypeStore,
    types: &Types,
    return_type: &TypeA<ID>,
) -> Result<usize> {
    CHANGED.store(0, Ordering::Relaxed);

    ast.data.constrain(
        &return_type.some(),
        ast.span.data("function body"),
        ast.span.data("function return type"),
    )?;

    ast.traverse_pre(&(), &mut |mut data, span, _pc, mut node| {
        match &mut node {
            ExpressionKind::Let {
                name,
                explicit_type,
                value,
            } => {
                value.data.constrain(
                    &explicit_type.data,
                    value.span.data("let value"),
                    explicit_type.data("explicit type requirement"),
                )?;
                data.constrain(
                    &types.unit.some(),
                    span.data("let result"),
                    Spanned::unspanned("unit"),
                )?;
                s_value_ptype
                    .entry(name.data.clone().left().unwrap().to_owned())
                    .or_default()
                    .connect(
                        &mut value.data,
                        Spanned::unspanned("symbol type"),
                        value.span.data("assigned type"),
                    )?;
            }
            ExpressionKind::Assign { name, value } => {
                data.constrain(
                    &types.unit.some(),
                    span.data("assignement result"),
                    Spanned::unspanned("unit"),
                )?;
                if let Either::Left(name) = name.data {
                    if let Some(symval) = s_value_ptype.get_mut(&name) {
                        value.data.connect(
                            symval,
                            value.span.data("assigned type"),
                            Spanned::unspanned("symbol type"),
                        )?;
                    }
                }
            }
            ExpressionKind::Reference(node) => {
                data.constrain(
                    &TypeA {
                        name: Some(types.reference),
                        args: vec![node.data.clone()],
                        lifetimes: vec![LifetimeSpec::Unknown],
                    },
                    span.data("reference value"),
                    node.span.data("referenced valued"),
                )?;
                node.data.constrain(
                    &data.args[0],
                    node.span.data("referenced valued"),
                    span.data("reference value"),
                )?;
            }
            ExpressionKind::Deref(node) => {
                node.data.constrain(
                    &TypeA {
                        name: Some(types.reference),
                        args: vec![data.clone()],
                        lifetimes: vec![LifetimeSpec::Unknown],
                    },
                    node.span.data("reference value"),
                    span.data("dereferenced value"),
                )?;
                data.constrain(
                    &node.data.args[0],
                    span.data("dereferenced value"),
                    node.span.data("reference value"),
                )?;
            }
            ExpressionKind::Symbol(s) => {
                if let Either::Left(s) = s.data {
                    if let Some(t) = s_value_ptype.get_mut(&s) {
                        data.connect(t, span.data("symbol"), Spanned::unspanned("symbol"))?;
                    }
                }
            }
            ExpressionKind::Block(stmts) => {
                if let Some(l) = stmts.last_mut() {
                    l.data.connect(
                        &mut data,
                        l.span.data("last block statement"),
                        span.data("result"),
                    )?;
                } else {
                    data.constrain(
                        &types.unit.some(),
                        span.data("empty block statement"),
                        Spanned::unspanned("unit type"),
                    )?;
                }
            }
            ExpressionKind::Invoke {
                function,
                arguments,
            } => {
                function.data.constrain(
                    &TypeA {
                        name: Some(types.function),
                        args: once(data.clone())
                            .chain(arguments.iter().map(|a| a.data.clone()))
                            .collect(),
                        lifetimes: vec![],
                    },
                    function.span.data("function"),
                    span.data("used function"),
                )?;
                data.constrain(
                    &function.data.args[0],
                    span.data("actual return"),
                    Spanned::unspanned("expected return"),
                )
                .context("the return value")?;
                for (i, arg) in arguments.iter_mut().enumerate() {
                    arg.data
                        .constrain(
                            &function.data.args[i + 1],
                            arg.span.data("provided arg"),
                            Spanned::unspanned("expected arg"),
                        )
                        .context(&format!("in the {} argument", i + 1))?;
                }
            }
            ExpressionKind::If {
                condition,
                cond_true,
                cond_false,
            } => {
                condition.data.constrain(
                    &types.bool.some(),
                    condition.span.data("condition"),
                    Spanned::unspanned("bool"),
                )?;
                cond_false.data.connect(
                    &mut data,
                    cond_true.span.data("true case"),
                    span.data("result"),
                )?;
                cond_true.data.connect(
                    &mut data,
                    cond_false.span.data("false case"),
                    span.data("result"),
                )?;
            }
            ExpressionKind::While { condition, body: _ } => {
                condition.data.constrain(
                    &types.bool.some(),
                    condition.span.data("condition"),
                    Spanned::unspanned("bool"),
                )?;
                data.constrain(
                    &types.unit.some(),
                    span.data("while loop result"),
                    Spanned::unspanned("unit"),
                )?;
            }
            ExpressionKind::Literal(lit) => data.constrain(
                &match lit {
                    Literal::Int(_) => types.i64.some(),
                    Literal::Float(_) => types.f64.some(),
                    Literal::String(_) => types.cstr.some(),
                    Literal::Bool(_) => types.bool.some(),
                },
                span.data("expected value"),
                span.data("literal"),
            )?,
            ExpressionKind::BinaryOp { op, lhs, rhs } => {
                lhs.data.connect(
                    &mut rhs.data,
                    lhs.span.data("left operand"),
                    rhs.span.data("right operand"),
                )?;
                use BinaryOp::*;
                match op {
                    Add | Sub | Mul | Div | Mod | And | Or | Xor => {
                        data.connect(&mut lhs.data, span.data("result"), lhs.span.data("param"))?
                    }
                    Less | Greater | Equal | NotEqual => data.constrain(
                        &types.bool.some(),
                        span.data("result"),
                        lhs.span.data("bool"),
                    )?,
                    Index => todo!(),
                }
            }
            ExpressionKind::StructConstructor { fields, r#type } => {
                if let Ok(ti) = <TypeA<Option<ID>> as Clone>::clone(&r#type.data).unwrap() {
                    data.constrain(&ti.some(), span.data("result"), r#type.data("constructor"))?;

                    let Some(TypeDecl::Struct { fields: sfields }) = s_type.get(&ti.name) else {
                        bail!("struct constructor of a non-struct");
                    };

                    for (fsym, fval) in fields {
                        if let Either::Left(fsym) = fsym.data {
                            let (_, _, ftype) = sfields
                                .iter()
                                .find(|(_, f, _)| *f == fsym)
                                .ok_or(serror!("field does not exist"))?;
                            fval.data.constrain(
                                &ftype.some(),
                                fval.span.data("field value"),
                                Spanned::unspanned("field type"),
                            )?;
                        }
                    }
                }
            }
            ExpressionKind::Field { base, field } => {
                if let Some(name) = base.data.name {
                    if let Either::Left(field) = field.data {
                        let tdecl = &s_type[&name];
                        let TypeDecl::Struct { fields } = tdecl else {
                            bail!("field access on a non-struct");
                        };
                        let ftype = fields
                            .iter()
                            .find(|(_, f, _)| *f == field)
                            .unwrap()
                            .2
                            .clone();

                        data.constrain(
                            &ftype.some(),
                            span.data("result"),
                            Spanned::unspanned("field type"),
                        )?;
                    }
                }
            }
        }
        Ok(())
    })?;

    Ok(CHANGED.load(Ordering::Relaxed))
}

impl TypeA<ID> {
    pub fn size(&self, s_type: &TypeStore) -> usize {
        match s_type
            .get(&self.name)
            .expect(&format!("no type decl for {}", self.name))
        {
            TypeDecl::Struct { fields } => {
                fields.iter().map(|(_, _, ftype)| ftype.size(s_type)).sum()
            }
            TypeDecl::I64 | TypeDecl::F64 => 1,
            TypeDecl::Unit => 0,
        }
    }
}
impl TypeA<Option<ID>> {
    pub fn has_holes(&self) -> bool {
        self.name.is_none() || self.args.iter().any(|a| a.has_holes())
    }
    pub fn connect(
        &mut self,
        rhs: &mut Self,
        llabel: Spanned<&str>,
        rlabel: Spanned<&str>,
    ) -> Result<()> {
        self.constrain(&rhs, llabel.clone(), rlabel.clone())?;
        rhs.constrain(&self, rlabel, llabel)?;
        Ok(())
    }
    pub fn constrain(
        &mut self,
        rhs: &Self,
        llabel: Spanned<&str>,
        rlabel: Spanned<&str>,
    ) -> Result<()> {
        if let Some(rname) = &rhs.name {
            if let Some(lname) = &self.name {
                if lname != rname {
                    return Err(Error::simple("type mismatch")
                        .with_ref(llabel.map(|l| format!("{l} has type {self}")))
                        .with_ref(rlabel.map(|r| format!("{r} has type {rhs}"))));
                }
            } else {
                CHANGED.fetch_add(1, Ordering::Relaxed);
                self.name = Some(rname.clone());
                self.args = vec![
                    TypeA {
                        name: None,
                        args: vec![],
                        lifetimes: vec![],
                    };
                    rhs.args.len()
                ]
            }

            if self.args.len() != rhs.args.len() {
                bail!(
                    "type arg length mismatch: {} vs {}",
                    self.args.len(),
                    rhs.args.len()
                );
            }

            for (i, (larg, rarg)) in self.args.iter_mut().zip(rhs.args.iter()).enumerate() {
                larg.constrain(rarg, llabel.clone(), rlabel.clone())
                    .context(&format!("in {} generic argument", num_to_ordinal(i)))?
            }
        }
        Ok(())
    }
}

impl<T> TypeA<Option<T>> {
    pub fn unwrap(self) -> Result<TypeA<T>> {
        Ok(TypeA {
            name: self.name.ok_or(serror!("found hole"))?,
            args: self
                .args
                .into_iter()
                .enumerate()
                .map(|(i, a)| {
                    a.unwrap()
                        .context(&format!("in {} argument", num_to_ordinal(i)))
                })
                .try_collect()?,
            lifetimes: vec![],
        })
    }
}
impl<T: Clone> TypeA<T> {
    pub fn some(&self) -> TypeA<Option<T>> {
        TypeA {
            name: Some(self.name.clone()),
            args: self.args.iter().map(TypeA::some).collect(),
            lifetimes: vec![],
        }
    }
}

pub fn num_to_ordinal(i: usize) -> &'static str {
    // TODO spelling?
    match i {
        0 => "first",
        1 => "second",
        2 => "third",
        3 => "fourth",
        4 => "fith",
        5 => "sixth",
        6 => "seventh",
        7 => "eighth",
        8 => "nineth",
        9 => "tenth",
        _ => "some",
    }
}

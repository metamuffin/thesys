use crate::parser::span::Spanned;
use std::{
    fmt::{Display, Formatter},
    path::PathBuf,
};

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub struct Error {
    message: String,
    refs: Vec<Spanned<String>>,
    context: Vec<Spanned<String>>,
}

impl Error {
    pub fn simple(s: impl ToString) -> Self {
        Self {
            message: s.to_string(),
            refs: vec![],
            context: vec![],
        }
    }
    pub fn with_ref(mut self, r: Spanned<String>) -> Self {
        self.refs.push(r);
        self
    }
    pub fn with_context(mut self, r: Spanned<String>) -> Self {
        self.context.push(r);
        self
    }
}

const RESET: &'static str = "\x1b[0m";
const RED: &'static str = "\x1b[31m";
const BLUE: &'static str = "\x1b[34m";
const METADATA: &'static str = "\x1b[38;2;150;150;200m\x1b[1m";
const BOLD: &'static str = "\x1b[1m";

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut state = SpanShowState::default();
        writeln!(f, "{RED}{BOLD}Error: {RESET}{BOLD}{}{RESET}", self.message)?;
        for c in self.context.iter().rev() {
            show_spanned(f, &mut state, c, BLUE)?;
        }
        for r in &self.refs {
            show_spanned(f, &mut state, r, RED)?;
        }
        Ok(())
    }
}

#[derive(Default)]
struct SpanShowState {
    line: Option<usize>,
    file: Option<PathBuf>,
}
fn show_spanned<T: Display>(
    f: &mut Formatter<'_>,
    state: &mut SpanShowState,
    s: &Spanned<T>,
    color: &str,
) -> std::fmt::Result {
    if let Some(span) = s.span() {
        let (linum, col) = span.source.linemap.off_to_line_col(span.range.start);
        if state.file != Some(span.source.path.clone()) {
            writeln!(
                f,
                "{METADATA}    --> {}{RESET}",
                span.source.path.file_name().unwrap().to_str().unwrap()
            )?;
            state.file = Some(span.source.path.clone());
            state.line = None;
        }
        if state.line != Some(linum) {
            state.line = Some(linum);
            let line = &span.source.code[span.source.linemap.line_to_range(linum)];
            writeln!(f, "{METADATA} {linum: >3} |{RESET} {line}")?;
        }
        writeln!(
            f,
            "{METADATA}     |{RESET} {: >pad$}{color}{BOLD}{:^>len$} {}{RESET}",
            "",
            "",
            s.data,
            pad = col - 1,
            len = span.range.len()
        )?;
    } else {
        writeln!(f, "{METADATA}     |{RESET} {color}{BOLD}{}{RESET}", s.data)?;
    }
    Ok(())
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Error::simple(format!("{value}"))
    }
}

#[macro_export]
macro_rules! serror {
    ($($args:expr),*) => {
        crate::error::Error::simple(format!($($args),*))
    };
}
#[macro_export]
macro_rules! bail {
    ($($args:expr),*) => {
        return Err(crate::serror!($($args),*))
    };
}

#[macro_export]
macro_rules! debug {
    ($($args:expr),*) => {
        if cfg!(feature = "debug") {
            eprintln!($($args),*)
        }
    };
}

pub trait Context {
    fn context(self, s: &str) -> Self;
    fn context_spanned(self, s: Spanned<&str>) -> Self;
}
impl<T> Context for Result<T> {
    fn context(self, s: &str) -> Self {
        self.map_err(|mut e| {
            e.context.push(Spanned::unspanned(s.to_string()));
            e
        })
    }

    fn context_spanned(self, s: Spanned<&str>) -> Self {
        self.map_err(|mut e| {
            e.context.push(s.map(|s| s.to_string()));
            e
        })
    }
}

impl From<std::fmt::Error> for Error {
    fn from(value: std::fmt::Error) -> Self {
        panic!("{value}")
    }
}

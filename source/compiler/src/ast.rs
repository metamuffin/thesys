use crate::{
    error::{Context, Error, Result},
    parser::span::Spanned,
    symbols::ID,
};
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, Clone)]
pub struct Expression<Data, Symbol, TypeSymbol> {
    pub data: Data,
    pub span: Spanned<()>,
    pub k: ExpressionKind<Data, Symbol, TypeSymbol>,
}

#[derive(Debug, Clone)]
pub enum ExpressionKind<Data, Symbol, TypeSymbol> {
    Let {
        name: Symbol,
        explicit_type: Spanned<Box<TypeA<Option<TypeSymbol>>>>,
        value: Box<Expression<Data, Symbol, TypeSymbol>>,
    },
    Assign {
        name: Symbol,
        value: Box<Expression<Data, Symbol, TypeSymbol>>,
    },
    Symbol(Symbol),
    Block(Vec<Expression<Data, Symbol, TypeSymbol>>),
    Invoke {
        function: Box<Expression<Data, Symbol, TypeSymbol>>,
        arguments: Vec<Expression<Data, Symbol, TypeSymbol>>,
    },
    If {
        condition: Box<Expression<Data, Symbol, TypeSymbol>>,
        cond_true: Box<Expression<Data, Symbol, TypeSymbol>>,
        cond_false: Box<Expression<Data, Symbol, TypeSymbol>>,
    },
    While {
        condition: Box<Expression<Data, Symbol, TypeSymbol>>,
        body: Box<Expression<Data, Symbol, TypeSymbol>>,
    },
    StructConstructor {
        r#type: Spanned<Box<TypeA<Option<TypeSymbol>>>>,
        fields: Vec<(Symbol, Expression<Data, Symbol, TypeSymbol>)>,
    },
    Reference(Box<Expression<Data, Symbol, TypeSymbol>>),
    Deref(Box<Expression<Data, Symbol, TypeSymbol>>),
    Literal(Literal),
    BinaryOp {
        op: BinaryOp,
        lhs: Box<Expression<Data, Symbol, TypeSymbol>>,
        rhs: Box<Expression<Data, Symbol, TypeSymbol>>,
    },
    Field {
        base: Box<Expression<Data, Symbol, TypeSymbol>>,
        field: Symbol,
    },
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BinaryOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Less,
    Greater,
    Equal,
    NotEqual,
    And,
    Or,
    Xor,
    Index,
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct TypeA<Symbol> {
    pub name: Symbol,
    pub args: Vec<TypeA<Symbol>>,
    pub lifetimes: Vec<LifetimeSpec>,
}

#[derive(Debug, Clone, PartialEq, Default)]
pub enum LifetimeSpec {
    Generic(ID),
    Local(ID),
    #[default]
    Unknown,
}

pub struct Function<Data, Symbol, TypeSymbol> {
    pub visibility: Visibility,
    pub generics: GenericParams<String>,
    pub name: Symbol,
    pub arguments: Vec<(Spanned<String>, TypeA<Option<TypeSymbol>>)>,
    pub return_type: TypeA<Option<TypeSymbol>>,
    pub body: Option<Expression<Data, Symbol, TypeSymbol>>, // none for extern
}

pub struct Struct<Symbol, TypeSymbol> {
    pub generics: GenericParams<String>,
    pub visibility: Visibility,
    pub name: Symbol,
    pub fields: Vec<(Spanned<String>, TypeA<Option<TypeSymbol>>)>,
}

pub struct Use<Symbol> {
    pub visibility: Visibility,
    pub item: Qualified<Symbol>,
}

pub enum Visibility {
    Private,
    Public,
}

pub struct Impl<Data, Symbol, TypeSymbol> {
    pub generics: GenericParams<TypeSymbol>,
    pub r#type: TypeA<TypeSymbol>,
    pub r#trait: Option<TypeA<TypeSymbol>>,
    pub inner: Vec<Spanned<Declaration<Data, Symbol, TypeSymbol>>>,
}

pub enum Declaration<Data, Symbol, TypeSymbol> {
    Use(Use<Symbol>),
    Assembly(String),
    Impl(Impl<Data, Symbol, TypeSymbol>),
    Function(Function<Data, Symbol, TypeSymbol>),
    Struct(Struct<Symbol, TypeSymbol>),
}

pub struct Qualified<Symbol> {
    pub absolute: bool,
    pub qual: Vec<Symbol>,
    pub fin: Symbol,
}

#[derive(Debug, Default)]
pub struct GenericParams<TypeSymbol> {
    pub types: Vec<TypeSymbol>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Int(i64),
    Bool(bool),
    Float(f64),
    String(String),
}

impl Display for Visibility {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Visibility::Private => Ok(()),
            Visibility::Public => f.write_str("pub "),
        }
    }
}
impl<Symbol: Display> Display for Qualified<Symbol> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.absolute {
            write!(f, "::")?;
        }
        for qual in &self.qual {
            write!(f, "{qual}::")?;
        }
        write!(f, "{}", self.fin)
    }
}
impl<Data: Display, Symbol: Display, TypeSymbol: Display> Declaration<Data, Symbol, TypeSymbol> {
    pub fn name(&self) -> Option<&Symbol> {
        match self {
            Declaration::Function(f) => Some(&f.name),
            Declaration::Struct(s) => Some(&s.name),
            Declaration::Assembly(_) | Declaration::Use(_) => None,
            Declaration::Impl(_) => None,
        }
    }
}

impl<Data: Display, Symbol: Display, TypeSymbol: Display> Display
    for Declaration<Data, Symbol, TypeSymbol>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Declaration::Use(sym) => write!(f, "use {sym}"),
            Declaration::Function(fun) => write!(f, "{fun}"),
            Declaration::Struct(Struct {
                fields,
                name,
                generics: _,
                visibility,
            }) => {
                writeln!(f, "{visibility}struct {name}")?;
                for (fname, ftype) in fields {
                    writeln!(f, "  {fname}: {ftype}")?;
                }
                Ok(())
            }
            Declaration::Assembly(_) => write!(f, "assembly block"),
            Declaration::Impl(_) => write!(f, "impl"),
        }
    }
}
impl<Symbol: Display> Display for Use<Symbol> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Self { item, visibility } = self;
        write!(f, "{visibility}use {item};")
    }
}
impl<Data: Display, Symbol: Display, TypeSymbol: Display> Display
    for Function<Data, Symbol, TypeSymbol>
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Self {
            arguments: _, // TODO
            body,
            return_type,
            name,
            visibility,
            generics,
        } = self;
        writeln!(f, "{visibility}fn {name}{generics}(...) -> {return_type}")?;
        if let Some(body) = body {
            body.format_with_indent(f, 1)
        } else {
            writeln!(f, "  extern")
        }
    }
}
impl<Symbol: Display> Display for GenericParams<Symbol> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if !self.types.is_empty() {
            write!(f, "<")?;
            write!(f, "...")?; // TODO
            write!(f, ">")?;
        }
        Ok(())
    }
}
impl<Data: Display, Symbol: Display, TypeSymbol: Display> Expression<Data, Symbol, TypeSymbol> {
    fn format_with_indent(&self, f: &mut Formatter<'_>, indent: usize) -> std::fmt::Result {
        fn print_node<Data: Display, Symbol: Display, TypeSymbol: Display>(
            indent: usize,
            f: &mut std::fmt::Formatter<'_>,
            label: Option<&str>,
            node: &Expression<Data, Symbol, TypeSymbol>,
        ) -> std::fmt::Result {
            for _ in 0..indent * 2 {
                write!(f, " ")?;
            }
            if let Some(label) = label {
                write!(f, "{label}: ")?;
            }
            match &node.k {
                ExpressionKind::Let {
                    name,
                    explicit_type: _,
                    value,
                } => {
                    writeln!(f, "let {name} = - {}", &node.data)?;
                    print_node(indent + 1, f, None, &value)?;
                }
                ExpressionKind::Assign { name, value } => {
                    writeln!(f, "assign {name} = - {}", &node.data)?;
                    print_node(indent + 1, f, None, &value)?;
                }
                ExpressionKind::Symbol(sym) => {
                    writeln!(f, "symbol {sym} - {}", &node.data)?;
                }
                ExpressionKind::Block(content) => {
                    writeln!(f, "block - {}", &node.data)?;
                    for s in content {
                        print_node(indent + 1, f, None, &s)?;
                    }
                }
                ExpressionKind::Reference(node) => {
                    writeln!(f, "reference - {}", &node.data)?;
                    print_node(indent + 1, f, None, &node)?;
                }
                ExpressionKind::Deref(node) => {
                    writeln!(f, "deref - {}", &node.data)?;
                    print_node(indent + 1, f, None, &node)?;
                }
                ExpressionKind::Invoke {
                    function,
                    arguments,
                } => {
                    writeln!(f, "invoke - {}", &node.data)?;
                    print_node(indent + 1, f, Some("function"), &function)?;
                    for s in arguments {
                        print_node(indent + 1, f, Some("argument"), &s)?;
                    }
                }
                ExpressionKind::If {
                    condition,
                    cond_true,
                    cond_false,
                } => {
                    writeln!(f, "if - {}", &node.data)?;
                    print_node(indent + 1, f, Some("condition"), &condition)?;
                    print_node(indent + 1, f, Some("cond_true"), &cond_true)?;
                    print_node(indent + 1, f, Some("cond_false"), &cond_false)?;
                }
                ExpressionKind::While { condition, body } => {
                    writeln!(f, "while - {}", &node.data)?;
                    print_node(indent + 1, f, Some("condition"), &condition)?;
                    print_node(indent + 1, f, Some("body"), &body)?;
                }
                ExpressionKind::Literal(lit) => {
                    writeln!(f, "literal {lit:?} - {}", &node.data)?;
                }
                ExpressionKind::BinaryOp { op, lhs, rhs } => {
                    writeln!(f, "bop {op:?} - {}", &node.data)?;
                    print_node(indent + 1, f, Some("lhs"), &lhs)?;
                    print_node(indent + 1, f, Some("rhs"), &rhs)?;
                }
                ExpressionKind::StructConstructor { r#type, fields } => {
                    writeln!(f, "struct constructor {} - {}", r#type, &node.data)?;
                    for (key, value) in fields {
                        print_node(indent + 1, f, Some(&format!("{key}")), value)?;
                    }
                }
                ExpressionKind::Field { base, field } => {
                    writeln!(f, "field access {field} - {}", &node.data)?;
                    print_node(indent + 1, f, Some("base"), &base)?;
                }
            }
            Ok(())
        }
        print_node(indent, f, None, self)
    }
}
impl<Data: Display, Symbol: Display, TypeSymbol: Display> Display
    for Expression<Data, Symbol, TypeSymbol>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.format_with_indent(f, 0)
    }
}

impl<Data, Symbol, TypeSymbol> Expression<Data, Symbol, TypeSymbol> {
    pub fn map<DataTo>(self, f: impl Fn(Data) -> DataTo) -> Expression<DataTo, Symbol, TypeSymbol> {
        self.try_map(&|x| Ok(f(x))).unwrap()
    }
    pub fn try_map<DataTo>(
        self,
        f: &impl Fn(Data) -> Result<DataTo>,
    ) -> Result<Expression<DataTo, Symbol, TypeSymbol>> {
        Ok(Expression {
            data: f(self.data)?,
            span: self.span,
            k: match self.k {
                ExpressionKind::Let {
                    name,
                    explicit_type,
                    value,
                } => ExpressionKind::Let {
                    name,
                    explicit_type,
                    value: Box::new(value.try_map(f).context("let value")?),
                },
                ExpressionKind::Assign { name, value } => ExpressionKind::Assign {
                    name,
                    value: Box::new(value.try_map(f).context("assign value")?),
                },
                ExpressionKind::Symbol(s) => ExpressionKind::Symbol(s),
                ExpressionKind::Block(stmts) => {
                    ExpressionKind::Block(stmts.into_iter().map(|e| e.try_map(f)).try_collect()?)
                }
                ExpressionKind::Reference(node) => {
                    ExpressionKind::Reference(Box::new(node.try_map(f).context("reference")?))
                }
                ExpressionKind::Deref(node) => {
                    ExpressionKind::Deref(Box::new(node.try_map(f).context("deref")?))
                }
                ExpressionKind::Invoke {
                    function,
                    arguments,
                } => ExpressionKind::Invoke {
                    function: Box::new(function.try_map(f).context("invokation function")?),
                    arguments: arguments
                        .into_iter()
                        .map(|e| e.try_map(f))
                        .try_collect()
                        .context("invokation argument")?,
                },
                ExpressionKind::If {
                    condition,
                    cond_true,
                    cond_false,
                } => ExpressionKind::If {
                    condition: Box::new(condition.try_map(f).context("if condition")?),
                    cond_true: Box::new(cond_true.try_map(f).context("if true case")?),
                    cond_false: Box::new(cond_false.try_map(f).context("if false case")?),
                },
                ExpressionKind::While { condition, body } => ExpressionKind::While {
                    condition: Box::new(condition.try_map(f).context("while loop condition")?),
                    body: Box::new(body.try_map(f).context("while body")?),
                },
                ExpressionKind::Literal(lit) => ExpressionKind::Literal(lit),
                ExpressionKind::BinaryOp { op, lhs, rhs } => ExpressionKind::BinaryOp {
                    op,
                    lhs: Box::new(lhs.try_map(f).context("left operand")?),
                    rhs: Box::new(rhs.try_map(f).context("right operand")?),
                },
                ExpressionKind::StructConstructor { fields, r#type } => {
                    ExpressionKind::StructConstructor {
                        r#type,
                        fields: fields
                            .into_iter()
                            .map(|(k, v)| Ok::<_, Error>((k, v.try_map(f)?)))
                            .try_collect()
                            .context("struct constructor field")?,
                    }
                }
                ExpressionKind::Field { base, field } => ExpressionKind::Field {
                    base: Box::new(base.try_map(f).context("field access base")?),
                    field,
                },
            },
        })
    }
    pub fn map_symbols<SymbolTo, TypeSymbolTo>(
        self,
        f: impl Fn(Symbol) -> SymbolTo,
        tf: &impl Fn(TypeSymbol) -> TypeSymbolTo,
    ) -> Expression<Data, SymbolTo, TypeSymbolTo> {
        self.try_map_symbols(&|t| Ok(f(t)), &|t| Ok(tf(t))).unwrap()
    }
    pub fn try_map_symbols<SymbolTo, TypeSymbolTo>(
        self,
        f: &impl Fn(Symbol) -> Result<SymbolTo>,
        tf: &impl Fn(TypeSymbol) -> Result<TypeSymbolTo>,
    ) -> Result<Expression<Data, SymbolTo, TypeSymbolTo>> {
        Ok(Expression {
            data: self.data,
            span: self.span,
            k: match self.k {
                ExpressionKind::Let {
                    name,
                    explicit_type,
                    value,
                } => ExpressionKind::Let {
                    name: f(name)?,
                    explicit_type: explicit_type
                        .try_map(|et| Ok(Box::new(et.try_map_symbols(tf)?)))?,
                    value: Box::new(value.try_map_symbols(f, tf).context("let value")?),
                },
                ExpressionKind::Assign { name, value } => ExpressionKind::Assign {
                    name: f(name)?,
                    value: Box::new(value.try_map_symbols(f, tf).context("assign value")?),
                },
                ExpressionKind::Symbol(s) => ExpressionKind::Symbol(f(s)?),
                ExpressionKind::Block(stmts) => ExpressionKind::Block(
                    stmts
                        .into_iter()
                        .map(|e| e.try_map_symbols(f, tf))
                        .try_collect()?,
                ),
                ExpressionKind::Reference(node) => ExpressionKind::Reference(Box::new(
                    node.try_map_symbols(f, tf).context("reference")?,
                )),
                ExpressionKind::Deref(node) => {
                    ExpressionKind::Deref(Box::new(node.try_map_symbols(f, tf).context("deref")?))
                }
                ExpressionKind::Invoke {
                    function,
                    arguments,
                } => ExpressionKind::Invoke {
                    function: Box::new(
                        function
                            .try_map_symbols(f, tf)
                            .context("invokation function")?,
                    ),
                    arguments: arguments
                        .into_iter()
                        .map(|e| e.try_map_symbols(f, tf))
                        .try_collect()
                        .context("invokation argument")?,
                },
                ExpressionKind::If {
                    condition,
                    cond_true,
                    cond_false,
                } => ExpressionKind::If {
                    condition: Box::new(condition.try_map_symbols(f, tf).context("if condition")?),
                    cond_true: Box::new(cond_true.try_map_symbols(f, tf).context("if true case")?),
                    cond_false: Box::new(
                        cond_false.try_map_symbols(f, tf).context("if false case")?,
                    ),
                },
                ExpressionKind::While { condition, body } => ExpressionKind::While {
                    condition: Box::new(
                        condition
                            .try_map_symbols(f, tf)
                            .context("while loop condition")?,
                    ),
                    body: Box::new(body.try_map_symbols(f, tf).context("while body")?),
                },
                ExpressionKind::Literal(lit) => ExpressionKind::Literal(lit),
                ExpressionKind::BinaryOp { op, lhs, rhs } => ExpressionKind::BinaryOp {
                    op,
                    lhs: Box::new(lhs.try_map_symbols(f, tf).context("left operand")?),
                    rhs: Box::new(rhs.try_map_symbols(f, tf).context("right operand")?),
                },
                ExpressionKind::StructConstructor { fields, r#type } => {
                    ExpressionKind::StructConstructor {
                        r#type: r#type.try_map(|t| Ok(Box::new(t.try_map_symbols(tf)?)))?,
                        fields: fields
                            .into_iter()
                            .map(|(k, v)| Ok::<_, Error>((f(k)?, v.try_map_symbols(f, tf)?)))
                            .try_collect()
                            .context("struct constructor field")?,
                    }
                }
                ExpressionKind::Field { base, field } => ExpressionKind::Field {
                    base: Box::new(base.try_map_symbols(f, tf).context("field access base")?),
                    field: f(field)?,
                },
            },
        })
    }
}

impl<Data, Symbol, TypeSymbol> Expression<Data, Symbol, TypeSymbol> {
    pub fn traverse_pre<Context>(
        &mut self,
        c: &Context,
        f: &mut impl FnMut(
            &mut Data,
            &Spanned<()>,
            &Context,
            &mut ExpressionKind<Data, Symbol, TypeSymbol>,
        ) -> Result<Context>,
    ) -> Result<()> {
        let cc = f(&mut self.data, &self.span, &c, &mut self.k)?;
        match &mut self.k {
            ExpressionKind::Let { value, .. } => value.traverse_pre(&cc, f).context("let value")?,
            ExpressionKind::Assign { value, .. } => {
                value.traverse_pre(&cc, f).context("assigned value")?
            }
            ExpressionKind::Block(stmts) => {
                for s in stmts {
                    s.traverse_pre(&cc, f)?;
                }
            }
            ExpressionKind::Reference(node) => {
                node.traverse_pre(&cc, f).context("reference")?;
            }
            ExpressionKind::Deref(node) => {
                node.traverse_pre(&cc, f).context("deref")?;
            }
            ExpressionKind::Invoke {
                function,
                arguments,
            } => {
                function.traverse_pre(&cc, f).context("in function")?;
                for a in arguments {
                    a.traverse_pre(&cc, f).context("in argument")?;
                }
            }
            ExpressionKind::If {
                condition,
                cond_true,
                cond_false,
            } => {
                condition.traverse_pre(&cc, f).context("in if condition")?;
                cond_true.traverse_pre(&cc, f).context("in true case")?;
                cond_false.traverse_pre(&cc, f).context("in false case")?;
            }
            ExpressionKind::While { condition, body } => {
                condition
                    .traverse_pre(&cc, f)
                    .context("in while condition")?;
                body.traverse_pre(&cc, f).context("in while body")?;
            }
            ExpressionKind::BinaryOp { lhs, rhs, .. } => {
                lhs.traverse_pre(&cc, f)
                    .context("in left operand of binary operator")?;
                rhs.traverse_pre(&cc, f)
                    .context("in right operand of binary operator")?;
            }
            ExpressionKind::Symbol(_) | ExpressionKind::Literal(_) => (),
            ExpressionKind::StructConstructor { fields, .. } => {
                for (_, field) in fields {
                    field
                        .traverse_pre(&cc, f)
                        .context("in struct constructor")?;
                }
            }
            ExpressionKind::Field { base, .. } => {
                base.traverse_pre(&cc, f).context("field access base")?;
            }
        }
        Ok(())
    }
}

impl Display for TypeA<ID> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Self {
            args,
            name,
            lifetimes,
        } = self;
        if args.is_empty() {
            write!(f, "{name}")
        } else {
            write!(
                f,
                "{name}<{}>",
                args.iter()
                    .map(|a| format!("{a}"))
                    .chain(lifetimes.iter().map(|l| format!("'{l}")))
                    .collect::<Vec<_>>()
                    .join(", ")
            )
        }
    }
}
impl<T> TypeA<T> {
    pub fn simple(name: T) -> Self {
        Self {
            name,
            args: vec![],
            lifetimes: vec![],
        }
    }
}
impl<Symbol: Display> Display for TypeA<Option<Symbol>> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Self {
            args,
            name,
            lifetimes,
        } = self;
        if let Some(name) = name {
            if args.is_empty() {
                write!(f, "{name}")
            } else {
                write!(
                    f,
                    "{name}<{}>",
                    args.iter()
                        .map(|a| a.to_string())
                        .chain(lifetimes.iter().map(|a| a.to_string()))
                        .collect::<Vec<_>>()
                        .join(", ")
                )
            }
        } else {
            write!(f, "_")
        }
    }
}

impl Display for LifetimeSpec {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            LifetimeSpec::Generic(id) => write!(f, "'{id}"),
            LifetimeSpec::Local(id) => write!(f, "'local@{id}"),
            LifetimeSpec::Unknown => write!(f, "'unknown"),
        }
    }
}

impl<Symbol> TypeA<Option<Symbol>> {
    pub fn hole() -> Self {
        Self {
            name: None,
            args: vec![],
            lifetimes: vec![],
        }
    }
}
impl<Symbol> TypeA<Option<Symbol>> {
    pub fn try_map_symbols<SymbolTo>(
        self,
        f: &impl Fn(Symbol) -> Result<SymbolTo>,
    ) -> Result<TypeA<Option<SymbolTo>>> {
        Ok(TypeA {
            name: self.name.map(f).transpose()?,
            args: self
                .args
                .into_iter()
                .map(|t| t.try_map_symbols(f))
                .try_collect()?,
            lifetimes: Vec::new(),
        })
    }
}

// impl<Data, Symbol> ExprNode<A> {
//     pub fn map<B: AstStage>(
//         self,
//         f_data: impl Fn(&ExprNodeK<A>, A::Data) -> B::Data,
//         f_sym: impl Fn(&ExprNodeK<A>, A::Symbol) -> B::Symbol,
//         f_typesym: impl Fn(&ExprNodeK<A>, A::TypeSymbol) -> B::TypeSymbol,
//     ) -> ExprNode<B> {
//         let data = f_data(&self.k, self.data);
//         match self.k.clone() {
//             ExprNodeK::Let {
//                 name,
//                 explicit_type,
//                 value,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::Let {
//                     name: f_sym(&self.k, name),
//                     explicit_type: explicit_type.map(|_| todo!()),
//                     value: Box::new(value.map(f_data, f_sym, f_typesym)),
//                 },
//             },
//             ExprNodeK::Symbol(s) => ExprNode {
//                 data,
//                 k: ExprNodeK::Symbol(f_sym(&self.k, s)),
//             },
//             ExprNodeK::Block(b) => ExprNode {
//                 data,
//                 k: ExprNodeK::Block(
//                     b.into_iter()
//                         .map(|n| n.map(&f_data, &f_sym, &f_typesym))
//                         .collect(),
//                 ),
//             },
//             ExprNodeK::Invoke {
//                 function,
//                 arguments,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::Invoke {
//                     function: Box::new(function.map(&f_data, &f_sym, &f_typesym)),
//                     arguments: arguments
//                         .into_iter()
//                         .map(|n| n.map(&f_data, &f_sym, &f_typesym))
//                         .collect(),
//                 },
//             },
//             ExprNodeK::If {
//                 condition,
//                 cond_true,
//                 cond_false,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::If {
//                     condition: Box::new(condition.map(&f_data, &f_sym, &f_typesym)),
//                     cond_true: Box::new(cond_true.map(&f_data, &f_sym, &f_typesym)),
//                     cond_false: Box::new(cond_false.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//             ExprNodeK::While { condition, body } => ExprNode {
//                 data,
//                 k: ExprNodeK::While {
//                     condition: Box::new(condition.map(&f_data, &f_sym, &f_typesym)),
//                     body: Box::new(body.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//             ExprNodeK::Literal(x) => ExprNode {
//                 data,
//                 k: ExprNodeK::Literal(x),
//             },
//             ExprNodeK::BinaryOp { op, lhs, rhs } => ExprNode {
//                 data,
//                 k: ExprNodeK::BinaryOp {
//                     op,
//                     lhs: Box::new(lhs.map(&f_data, &f_sym, &f_typesym)),
//                     rhs: Box::new(rhs.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//         }
//     }
// }

#[derive(Debug, Clone, Copy)]
pub struct Unit;
impl Display for Unit {
    fn fmt(&self, _f: &mut Formatter<'_>) -> std::fmt::Result {
        Ok(())
    }
}

use crate::symbols::ID;
use std::fmt::Display;

pub type Value = i64;

#[derive(Debug, Clone)]
pub enum Instruction {
    Noa,
    Add(Argument, Argument, Argument),
    Sub(Argument, Argument, Argument),
    Mul(Argument, Argument, Argument),
    Div(Argument, Argument, Argument),
    Mod(Argument, Argument, Argument),
    Equal(Argument, Argument, Argument),
    Less(Argument, Argument, Argument),
    Greater(Argument, Argument, Argument),
    Lnot(Argument, Argument),
    Bnot(Argument, Argument),
    Xor(Argument, Argument, Argument),
    Or(Argument, Argument, Argument),
    And(Argument, Argument, Argument),
    Shl(Argument, Argument, Argument),
    Shr(Argument, Argument, Argument),
    Move(Argument, Argument),
    Rsp(Argument),
    Msp(Argument),
    Jump(Argument),
    Cjump {
        cond: Argument,
        pc: Argument,
    },
    Rpc(Argument),
    Spawn {
        pc: Argument,
        sp: Argument,
        error: Argument,
    },
    Halt,
    Accept {
        id: Argument,
    },
    Read {
        id: Argument,
        value: Argument,
        error: Argument,
    },
    Write {
        id: Argument,
        value: Argument,
        error: Argument,
    },

    _Label(Label),
    _LabelValue(Label, Value),
    _Literal(Value),
}

#[derive(Debug, Clone)]
pub enum Argument {
    Immediate(Value),
    Base(Value),
    Stack(Value),
    StackPointer(Value),
    _LabelImmediate(Label, Value),
    _LabelBase(Label, Value),
    _LabelStack(Label, Value),
    _LabelStackPointer(Label, Value),
}

impl Argument {
    pub fn offset(&self, n: usize) -> Argument {
        match self {
            Argument::Base(o) => Argument::Base(o + n as i64),
            Argument::Stack(o) => Argument::Stack(o + n as i64),
            Argument::StackPointer(o) => Argument::StackPointer(o + n as i64),
            Argument::_LabelBase(l, o) => Argument::_LabelBase(l.clone(), o + n as i64),
            Argument::_LabelStack(l, o) => Argument::_LabelStack(l.clone(), o + n as i64),
            Argument::_LabelStackPointer(l, o) => {
                Argument::_LabelStackPointer(l.clone(), o + n as i64)
            }
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum Label {
    ID(ID),
    String(String),
}
impl Label {
    pub fn new_named(s: String) -> Self {
        Self::ID(ID::new_named(s))
    }
    pub fn new() -> Self {
        Self::ID(ID::new())
    }
    pub fn from_name(s: String) -> Self {
        Self::String(s)
    }
}
impl Display for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Label::ID(x) => write!(f, "L{x}"),
            Label::String(x) => write!(f, "{x}"),
        }
    }
}

impl Instruction {
    pub fn map(self, mut f: impl FnMut(Argument) -> Argument) -> Self {
        match self {
            Instruction::Noa => Instruction::Noa,
            Instruction::Add(x, y, z) => Instruction::Add(f(x), f(y), f(z)),
            Instruction::Sub(x, y, z) => Instruction::Sub(f(x), f(y), f(z)),
            Instruction::Mul(x, y, z) => Instruction::Mul(f(x), f(y), f(z)),
            Instruction::Div(x, y, z) => Instruction::Div(f(x), f(y), f(z)),
            Instruction::Mod(x, y, z) => Instruction::Mod(f(x), f(y), f(z)),
            Instruction::Equal(x, y, z) => Instruction::Equal(f(x), f(y), f(z)),
            Instruction::Less(x, y, z) => Instruction::Less(f(x), f(y), f(z)),
            Instruction::Greater(x, y, z) => Instruction::Greater(f(x), f(y), f(z)),
            Instruction::Xor(x, y, z) => Instruction::Xor(f(x), f(y), f(z)),
            Instruction::Or(x, y, z) => Instruction::Or(f(x), f(y), f(z)),
            Instruction::And(x, y, z) => Instruction::And(f(x), f(y), f(z)),
            Instruction::Shl(x, y, z) => Instruction::Shl(f(x), f(y), f(z)),
            Instruction::Shr(x, y, z) => Instruction::Shr(f(x), f(y), f(z)),
            Instruction::Lnot(x, y) => Instruction::Lnot(f(x), f(y)),
            Instruction::Bnot(x, y) => Instruction::Bnot(f(x), f(y)),
            Instruction::Move(x, y) => Instruction::Move(f(x), f(y)),
            Instruction::Rsp(x) => Instruction::Rsp(f(x)),
            Instruction::Msp(x) => Instruction::Msp(f(x)),
            Instruction::Jump(x) => Instruction::Jump(f(x)),
            Instruction::Rpc(x) => Instruction::Rpc(f(x)),
            Instruction::Cjump { cond, pc } => Instruction::Cjump {
                cond: f(cond),
                pc: f(pc),
            },
            Instruction::Spawn { pc, sp, error } => Instruction::Spawn {
                pc: f(pc),
                sp: f(sp),
                error: f(error),
            },
            Instruction::Halt => Instruction::Halt,
            Instruction::Accept { id } => Instruction::Accept { id: f(id) },
            Instruction::Read { id, value, error } => Instruction::Read {
                id: f(id),
                value: f(value),
                error: f(error),
            },
            Instruction::Write { id, value, error } => Instruction::Write {
                id: f(id),
                value: f(value),
                error: f(error),
            },
            x => x,
        }
    }
    pub fn index(&self) -> Value {
        match self {
            Instruction::Noa => 0,
            Instruction::Add(_, _, _) => 1,
            Instruction::Sub(_, _, _) => 2,
            Instruction::Mul(_, _, _) => 3,
            Instruction::Div(_, _, _) => 4,
            Instruction::Mod(_, _, _) => 5,
            Instruction::Equal(_, _, _) => 6,
            Instruction::Less(_, _, _) => 7,
            Instruction::Greater(_, _, _) => 8,
            Instruction::Lnot(_, _) => 9,
            Instruction::Bnot(_, _) => 10,
            Instruction::Xor(_, _, _) => 11,
            Instruction::Or(_, _, _) => 12,
            Instruction::And(_, _, _) => 13,
            Instruction::Shl(_, _, _) => 14,
            Instruction::Shr(_, _, _) => 15,
            Instruction::Move(_, _) => 16,
            Instruction::Rsp(_) => 17,
            Instruction::Msp(_) => 18,
            Instruction::Jump(_) => 19,
            Instruction::Cjump { .. } => 20,
            Instruction::Rpc(_) => 21,
            Instruction::Spawn { .. } => 22,
            Instruction::Halt => 23,
            Instruction::Accept { .. } => 24,
            Instruction::Read { .. } => 25,
            Instruction::Write { .. } => 26,
            _ => unreachable!(),
        }
    }
}

impl Argument {
    pub fn mode_index(&self) -> usize {
        match self {
            Argument::Immediate(_) => 0,
            Argument::Base(_) => 1,
            Argument::Stack(_) => 2,
            Argument::StackPointer(_) => 3,
            _ => unreachable!(),
        }
    }
    pub fn value(&self) -> Value {
        match self {
            Argument::Immediate(x) => *x,
            Argument::Base(x) => *x,
            Argument::Stack(x) => *x,
            Argument::StackPointer(x) => *x,
            Argument::_LabelImmediate(_, _) => 0,
            Argument::_LabelStack(_, _) => 0,
            Argument::_LabelStackPointer(_, _) => 0,
            Argument::_LabelBase(_, _) => 0,
        }
    }
}
impl Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Instruction::Noa => write!(f, "noa"),
            Instruction::Add(a, b, c) => write!(f, "add {a} {b} {c}"),
            Instruction::Sub(a, b, c) => write!(f, "sub {a} {b} {c}"),
            Instruction::Mul(a, b, c) => write!(f, "mul {a} {b} {c}"),
            Instruction::Div(a, b, c) => write!(f, "div {a} {b} {c}"),
            Instruction::Mod(a, b, c) => write!(f, "mod {a} {b} {c}"),
            Instruction::Equal(a, b, c) => write!(f, "equal {a} {b} {c}"),
            Instruction::Less(a, b, c) => write!(f, "less {a} {b} {c}"),
            Instruction::Greater(a, b, c) => write!(f, "greater {a} {b} {c}"),
            Instruction::Lnot(a, b) => write!(f, "lnot {a} {b}"),
            Instruction::Bnot(a, b) => write!(f, "bnot {a} {b}"),
            Instruction::Xor(a, b, c) => write!(f, "xor {a} {b} {c}"),
            Instruction::Or(a, b, c) => write!(f, "or {a} {b} {c}"),
            Instruction::And(a, b, c) => write!(f, "and {a} {b} {c}"),
            Instruction::Shl(a, b, c) => write!(f, "shl {a} {b} {c}"),
            Instruction::Shr(a, b, c) => write!(f, "shr {a} {b} {c}"),
            Instruction::Move(a, b) => write!(f, "move {a} {b}"),
            Instruction::Rsp(a) => write!(f, "rsp {a}"),
            Instruction::Msp(a) => write!(f, "msp {a}"),
            Instruction::Jump(a) => write!(f, "jump {a}"),
            Instruction::Cjump { cond, pc } => write!(f, "cjump {cond} {pc}"),
            Instruction::Rpc(a) => write!(f, "rpc {a}"),
            Instruction::Spawn { pc, sp, error: bp } => write!(f, "spawn {pc} {sp} {bp}"),
            Instruction::Halt => write!(f, "halt"),
            Instruction::Accept { id } => write!(f, "accept {id}"),
            Instruction::Read { id, value, error } => write!(f, "read {id} {value} {error}"),
            Instruction::Write { id, value, error } => write!(f, "write {id} {value} {error}"),
            Instruction::_Label(l) => write!(f, ": {l}"),
            Instruction::_Literal(x) => write!(f, "'{x:02x}"),
            Instruction::_LabelValue(l, v) => write!(f, "= {l} {v}"),
        }
    }
}
impl Display for Argument {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Argument::Immediate(x) => write!(f, "#{x:}"),
            Argument::Base(x) => write!(f, "${x:}"),
            Argument::Stack(x) => write!(f, "{x:}"),
            Argument::StackPointer(x) => write!(f, "*{x:}"),
            Argument::_LabelImmediate(x, 0) => write!(f, "@{x}"),
            Argument::_LabelImmediate(x, o) => write!(f, "@{x}+{o}"),
            Argument::_LabelStack(x, 0) => write!(f, "_{x}"),
            Argument::_LabelStack(x, o) => write!(f, "_{x}+{o}"),
            Argument::_LabelStackPointer(x, 0) => write!(f, "&{x}"),
            Argument::_LabelStackPointer(x, o) => write!(f, "&{x}+{o}"),
            Argument::_LabelBase(x, o) => write!(f, "%{x}+{o}"),
        }
    }
}

use crate::{
    ast::{BinaryOp, ExpressionKind, Literal, TypeA},
    error::{Error, Result},
    instructions::{self, Argument, Instruction, Label},
    symbols::{ScopedStore, ID},
    types::{TypeDecl, TypeStore, ValueTypeStore},
    Stage4Tree,
};
use std::{collections::BTreeMap, iter::once};
use Argument::*;
use Instruction::*;

pub type ValueStore<'a> = ScopedStore<'a, ID, Argument>;
pub type DataStore = BTreeMap<Vec<i64>, Label>;

pub fn generate_code(
    label: Label,
    ast: &Stage4Tree,
    args: &[ID],
    rettype: &TypeA<ID>,
    s_type: &TypeStore,
    s_value_type: &ValueTypeStore,
    s_symbol_value: &ValueStore,
    s_data: &mut DataStore,
) -> Result<Vec<Instruction>> {
    fn traverse(
        ast: &Stage4Tree,
        s_type: &TypeStore,
        s_value_type: &ValueTypeStore,
        s_symbol_value: &mut ValueStore,
        s_data: &mut DataStore,
        stack: &mut Stack,
    ) -> Result<(Vec<Instruction>, Argument)> {
        let mut traverse = |value, stack: &mut Stack| {
            traverse(value, s_type, s_value_type, s_symbol_value, s_data, stack)
        };

        let mut insts = Vec::new();
        Ok(match &ast.k {
            ExpressionKind::Let { name, value, .. } => {
                let (is, r) = traverse(&value, stack)?;
                insts.extend(is);
                let var = stack.allocate(&value.data, s_type);
                let size = value.data.size(s_type);
                insts.extend(move_many(size, r, var.clone()));
                s_symbol_value.insert(**name, var);
                (insts, Argument::Immediate(0))
            }
            ExpressionKind::Assign { name, value } => {
                let (is, r) = traverse(&value, stack)?;
                insts.extend(is);
                let var = s_symbol_value.get(name).unwrap();
                let size = value.data.size(s_type);
                insts.extend(move_many(size, r, var.clone()));
                (insts, Argument::Immediate(0))
            }
            ExpressionKind::Symbol(s) => (insts, {
                s_symbol_value
                    .get(s)
                    .ok_or(
                        Error::simple("symbol has no value")
                            .with_ref(ast.span.data("this symbol".to_string())),
                    )?
                    .clone()
            }),
            ExpressionKind::Block(a) => {
                let mut result = Argument::Immediate(0);
                for a in a {
                    let (is, r) = traverse(a, stack)?;
                    insts.extend(is);
                    result = r;
                }
                (insts, result)
            }
            ExpressionKind::Invoke {
                arguments,
                function,
            } => {
                let mut args = Vec::new();
                let mut asize = 0;
                for arg in arguments {
                    asize += arg.data.size(s_type);
                    let (ais, arg) = traverse(&arg, stack)?;
                    insts.extend(ais);
                    args.push(arg);
                }

                let (fis, fun) = traverse(function, stack)?;
                insts.extend(fis);
                let ret = stack.allocate(&ast.data, s_type);
                let rsize = ast.data.size(s_type);

                let ret_label = Label::new_named("ret".into());

                assert_eq!(function.data.size(s_type), 1, "function size is one");

                insts.push(Move(_LabelImmediate(ret_label.clone(), 0), Stack(-1)));
                let mut aoff = 0;
                for (arg, arg_location) in arguments.iter().zip(args) {
                    insts.extend(move_many(
                        asize,
                        arg_location,
                        Stack(-1 - asize as i64 + aoff as i64 - rsize as i64),
                    ));
                    aoff += arg.data.size(s_type);
                }
                insts.push(Jump(fun));
                insts.push(_Label(ret_label));
                insts.extend(move_many(rsize, Stack(-1 - rsize as i64), ret.clone()));

                (insts, ret)
            }
            ExpressionKind::Literal(Literal::Int(n)) => (insts, Immediate(*n)),
            ExpressionKind::Literal(Literal::Bool(true)) => (insts, Immediate(1)),
            ExpressionKind::Literal(Literal::Bool(false)) => (insts, Immediate(0)),
            ExpressionKind::Literal(Literal::String(data)) => {
                let data = data
                    .as_bytes()
                    .iter()
                    .map(|b| *b as i64)
                    .chain(once(0))
                    .collect::<Vec<_>>();
                let label = s_data
                    .entry(data)
                    .or_insert_with(|| Label::new_named("strlit".to_owned()))
                    .to_owned();
                (insts, _LabelImmediate(label, 0))
            }
            ExpressionKind::BinaryOp { op, lhs, rhs } => {
                let (lhs_is, lhs) = traverse(&lhs, stack)?;
                let (rhs_is, rhs) = traverse(&rhs, stack)?;
                insts.extend(lhs_is);
                insts.extend(rhs_is);
                let var = stack.allocate(&ast.data, s_type);
                let var2 = var.clone();
                insts.push(match op {
                    BinaryOp::Add => Add(lhs, rhs, var),
                    BinaryOp::Sub => Sub(lhs, rhs, var),
                    BinaryOp::Mul => Mul(lhs, rhs, var),
                    BinaryOp::Div => Div(lhs, rhs, var),
                    BinaryOp::Mod => Mod(lhs, rhs, var),
                    BinaryOp::Less => Less(lhs, rhs, var),
                    BinaryOp::Greater => Greater(lhs, rhs, var),
                    BinaryOp::Equal => Equal(lhs, rhs, var),
                    BinaryOp::And => And(lhs, rhs, var),
                    BinaryOp::Or => Or(lhs, rhs, var),
                    BinaryOp::Xor => Xor(lhs, rhs, var),
                    BinaryOp::Index => todo!(),
                    BinaryOp::NotEqual => todo!(),
                });
                (insts, var2)
            }
            ExpressionKind::If {
                condition,
                cond_true,
                cond_false,
            } => {
                let (cis, cond) = traverse(&condition, stack)?;
                let (ctis, cond_true) = traverse(&cond_true, stack)?;
                let (cfis, cond_false) = traverse(&cond_false, stack)?;

                let res = stack.allocate(&ast.data, s_type);

                let l_true = Label::new_named("ctrue".into());
                let l_end = Label::new_named("cend".into());

                insts.extend(cis);
                insts.push(Cjump {
                    cond,
                    pc: _LabelImmediate(l_true.clone(), 0),
                });
                insts.extend(cfis);
                insts.push(Move(cond_false, res.clone()));
                insts.push(Jump(_LabelImmediate(l_end.clone(), 0)));
                insts.push(_Label(l_true));
                insts.extend(ctis);
                insts.push(Move(cond_true, res.clone()));
                insts.push(_Label(l_end));

                (insts, res)
            }
            ExpressionKind::While { condition, body } => {
                let (cis, cond) = traverse(&condition, stack)?;
                let (bis, _body) = traverse(&body, stack)?;

                let l_cond = Label::new_named("wcond".into());
                let l_top = Label::new_named("wtop".into());

                insts.push(Jump(_LabelImmediate(l_cond.clone(), 0)));
                insts.push(_Label(l_top.clone()));
                insts.extend(bis);
                insts.push(_Label(l_cond));
                insts.extend(cis);
                insts.push(Cjump {
                    cond,
                    pc: _LabelImmediate(l_top.clone(), 0),
                });

                (insts, Argument::Immediate(0))
            }
            ExpressionKind::StructConstructor { fields, .. } => {
                let var = stack.allocate(&ast.data, s_type);

                let offsets = field_offsets(s_type, &ast.data);
                for (fid, fval) in fields {
                    let (vis, value) = traverse(fval, stack)?;
                    insts.extend(vis);

                    let offset = offsets.iter().find(|(i, _)| *i == **fid).unwrap().1;
                    insts.extend(move_many(fval.data.size(s_type), value, var.offset(offset)))
                }

                (insts, var)
            }
            ExpressionKind::Field { base, field } => {
                let (bis, baseval) = traverse(&base, stack)?;
                insts.extend(bis);

                let offsets = field_offsets(s_type, &base.data);
                let offset = offsets.iter().find(|(i, _)| *i == **field).unwrap().1;

                (insts, baseval.offset(offset))
            }
            ExpressionKind::Reference(inner) => {
                let (iis, innerval) = traverse(&inner, stack)?;
                insts.extend(iis);
                (insts, innerval) // TODO
            }
            ExpressionKind::Deref(inner) => {
                let (iis, innerval) = traverse(&inner, stack)?;
                insts.extend(iis);
                (insts, innerval) // TODO
            }
            x => todo!("{x:?}"),
        })
    }

    let mut s_symbol_value = s_symbol_value.create_child();

    let arg_label = Label::new_named("arg".into());
    let asize = {
        let mut offset = 0;
        for a in args {
            let size = s_value_type.get(a).unwrap().size(s_type);
            s_symbol_value.insert(*a, Argument::_LabelStack(arg_label.clone(), offset as i64));
            offset += size;
        }
        offset
    };
    let rsize = rettype.size(s_type);

    let mut insts = Vec::new();
    let mut stack = Stack::default();
    let (body_insts, result) = traverse(
        &ast,
        s_type,
        s_value_type,
        &mut s_symbol_value,
        s_data,
        &mut stack,
    )?;

    let lsize = stack.size as instructions::Value;

    insts.push(_Label(label));
    insts.push(Msp(Immediate(-lsize - 1 - asize as i64 - rsize as i64)));
    insts.extend(body_insts);
    insts.extend(move_many(rsize, result, Stack(lsize + asize as i64)));
    insts.push(Msp(Immediate(lsize + 1 + asize as i64 + rsize as i64)));
    insts.push(Jump(Stack(-1)));

    insts.push(_LabelValue(arg_label, lsize));

    Ok(insts)
}

#[derive(Debug, Default)]
pub struct Stack {
    // pub symbols: Vec<Arc<StackElement>>,
    pub size: usize,
}

impl Stack {
    pub fn allocate(&mut self, r#type: &TypeA<ID>, s_type: &TypeStore) -> Argument {
        let s = Argument::Stack(self.size.try_into().unwrap());
        self.size += r#type.size(s_type);
        // self.symbols.push(s.clone());
        s
    }
}
fn move_many(amount: usize, from: Argument, to: Argument) -> Vec<Instruction> {
    let map_arg = |i: i64, a: &Argument| match a {
        Immediate(x) => Immediate(*x),
        Base(x) => Base(x + i),
        Stack(x) => Stack(x + i),
        StackPointer(x) => StackPointer(x + i),
        _LabelImmediate(l, o) => _LabelImmediate(l.clone(), o + i),
        _LabelStack(l, o) => _LabelStack(l.clone(), o + i),
        _LabelStackPointer(l, o) => _LabelStackPointer(l.clone(), o + i),
        _LabelBase(l, o) => _LabelBase(l.clone(), o + i),
    };
    (0..amount)
        .map(|i| Move(map_arg(i as i64, &from), map_arg(i as i64, &to)))
        .collect()
}

fn field_offsets(s_type: &TypeStore, r#type: &TypeA<ID>) -> Vec<(ID, usize)> {
    // TODO
    let TypeDecl::Struct { fields } = &s_type[&r#type.name] else {
        panic!("field access on non struct type: {type}")
    };

    let mut offset = 0;
    let mut out = Vec::new();
    for (_, fid, ftype) in fields {
        out.push((*fid, offset));
        offset += ftype.size(s_type)
    }
    out
}

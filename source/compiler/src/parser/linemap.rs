use std::{ops::Range, path::PathBuf};

#[derive(Debug, Clone)]
pub struct Source {
    pub path: PathBuf,
    pub code: String,
    pub linemap: Linemap,
}
impl Source {
    pub fn new(path: PathBuf, code: &str) -> Self {
        Self {
            path,
            code: code.to_owned(),
            linemap: Linemap::new(code),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Linemap {
    off_line: Vec<usize>,
}

impl Linemap {
    pub fn new(s: &str) -> Self {
        let mut off_line = Vec::new();
        off_line.push(0); // offset indexing by one
        off_line.push(0); //? why off by 2?
        for (pos, c) in s.char_indices() {
            if c == '\n' {
                off_line.push(pos + 1)
            }
        }
        off_line.push(s.len());
        Self { off_line }
    }
    pub fn off_to_line_col(&self, p: usize) -> (usize, usize) {
        let mut l = (0, 0);
        for (i, pk) in self.off_line.iter().enumerate() {
            if *pk > p {
                break;
            }
            l = (i, *pk);
        }
        (l.0, p - l.1 + 1)
    }
    pub fn line_to_range(&self, l: usize) -> Range<usize> {
        self.off_line[l]..self.off_line[l + 1] - 1
    }
}

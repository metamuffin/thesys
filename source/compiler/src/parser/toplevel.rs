use super::{
    exact, ident, lazy, many, many_sep,
    nodes::{parse_block, parse_type},
    opt, ows, qident,
    span::{ParserSpanExt, Spanned},
    ws, Parser, ParserMapExt, ParserUnwrapExt,
};
use super::{parse_anything, ParserContextExt};
use crate::{
    ast::{Declaration, Function, GenericParams, Struct, TypeA, Use, Visibility},
    Stage1Declaration,
};

pub fn parse_file() -> Parser<Vec<Stage1Declaration>> {
    many(ows() >> (parse_fn_decl() | parse_struct_decl() | parse_use_decl() | parse_assembly()))
        << ows()
}

pub fn parse_use_decl() -> Parser<Stage1Declaration> {
    let syn = parse_vis() & exact("use") >> ows() >> qident() << exact(";");
    syn.map(|(visibility, item)| Declaration::Use(Use { visibility, item }))
        .span()
}

pub fn parse_vis() -> Parser<Visibility> {
    let syn = opt(exact("pub") >> ws());
    syn.map(|o| {
        if o.is_some() {
            Visibility::Public
        } else {
            Visibility::Private
        }
    })
}

pub fn parse_struct_decl() -> Parser<Stage1Declaration> {
    let syn_tuple = parse_vis()
        & exact("struct") >> ws() >> ident().span()
        & parse_generics_decl()
        & ows() >> exact("(") >> many_sep(ows() >> parse_type() << ows(), exact(","))
            << ows()
            << exact(");");
    let syn_tuple = syn_tuple
        .map(|(((visibility, name), generics), fields)| {
            Declaration::Struct(Struct {
                visibility,
                generics,
                name,
                fields: fields
                    .into_iter()
                    .enumerate()
                    .map(|(i, f)| (Spanned::unspanned(format!("{i}")), f))
                    .collect(),
            })
        })
        .span()
        .context("tuple struct declartion");

    let syn_fields = parse_vis()
        & exact("struct") >> ws() >> ident().span()
        & parse_generics_decl()
        & ows()
            >> exact("{")
            >> many_sep(
                ows() >> ident().span() & (exact(":") >> ows() >> parse_type() << ows()),
                exact(","),
            )
            << ows()
            << exact("}");
    let syn_fields = syn_fields
        .map(|(((visibility, name), generics), fields)| {
            Declaration::Struct(Struct {
                visibility,
                name,
                fields,
                generics,
            })
        })
        .span()
        .context("struct declaration");

    syn_fields | syn_tuple
}

fn parse_fn_head() -> Parser<(
    (
        ((Visibility, Spanned<String>), GenericParams<String>),
        Vec<(Spanned<String>, TypeA<Option<String>>)>,
    ),
    TypeA<Option<String>>,
)> {
    parse_vis()
        & exact("fn") >> ws() >> ident().span()
        & parse_generics_decl()
        & exact("(")
            >> many_sep(
                ows() >> ident().span() & exact(":") >> ows() >> lazy(parse_type),
                exact(","),
            )
        & ows()
            >> exact(")")
            >> opt(ows() >> exact("->") >> ows() >> parse_type()).unwrap_or_else(|| TypeA {
                name: Some("unit".to_string()),
                args: vec![],
                lifetimes: vec![],
            })
}

pub fn parse_fn_decl() -> Parser<Stage1Declaration> {
    let regular_fn = parse_fn_head() & ows() >> parse_block();
    let regular_fn = regular_fn
        .map(
            |(((((visibility, name), generics), arguments), return_type), body)| {
                Declaration::Function(Function {
                    generics,
                    visibility,
                    name,
                    arguments,
                    return_type,
                    body: Some(body),
                })
            },
        )
        .span()
        .context("function declaration");

    let extern_fn = exact("extern") >> ws() >> parse_fn_head() << ows() << exact(";");
    let extern_fn = extern_fn
        .map(
            |((((visibility, name), generics), arguments), return_type)| {
                Declaration::Function(Function {
                    generics,
                    visibility,
                    name,
                    arguments,
                    return_type,
                    body: None,
                })
            },
        )
        .span()
        .context("extern function declaration");

    regular_fn | extern_fn
}

pub fn parse_assembly() -> Parser<Stage1Declaration> {
    let syn = exact("ASSEMBLY {") >> parse_anything('}') << exact("}");

    syn.map(Declaration::Assembly)
        .span()
        .context("assembly block")
}

pub fn parse_generics_decl() -> Parser<GenericParams<String>> {
    let syn_generic_part = ident();
    let syn = opt(
        exact("<") >> many_sep(ows() >> syn_generic_part << ows(), exact(","))
            << ows()
            << exact(">"),
    );

    syn.map(|x| x.map(|types| GenericParams { types }).unwrap_or_default())
}

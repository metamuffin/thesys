use super::span::Spanned;
use crate::error::Error as CompilerError;

#[derive(Debug)]
pub struct Error {
    start: usize,
    message: Spanned<String>,
    context: Vec<Spanned<String>>,
    other: Vec<Error>,
}

impl Error {
    pub fn new(message: Spanned<String>) -> Self {
        Self {
            start: message.span().unwrap().range.start,
            message,
            context: vec![],
            other: vec![],
        }
    }
    pub fn combine(x: Error, y: Error) -> Self {
        if x.start > y.start {
            x
        } else {
            y
        }
    }
    pub fn merge(mut x: Error, y: Option<Error>) -> Self {
        x.other.extend(y);
        x
    }
    pub fn context(mut self, s: Spanned<&str>) -> Self {
        self.context.push(s.map(|s| s.to_string()));
        self
    }
}

impl From<Error> for CompilerError {
    fn from(value: Error) -> Self {
        let mut e = CompilerError::simple("parsing failed").with_ref(value.message);
        for o in value.other {
            e = e.with_ref(o.message);
        }
        e
    }
}

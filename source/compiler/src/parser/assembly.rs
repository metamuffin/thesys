use crate::{
    bail,
    error::{Context, Error, Result},
    instructions::{Argument, Instruction, Label, Value},
    serror,
};

pub fn parse_assembly(s: &str) -> Result<Vec<Instruction>> {
    let mut out = Vec::new();
    for (linum, line) in s.lines().enumerate() {
        parse_line(&mut out, line).context(&format!("line {}", linum + 1))?;
    }
    Ok(out)
}

fn parse_line(out: &mut Vec<Instruction>, line: &str) -> Result<()> {
    let line = line.trim();
    let (line, _) = line.split_once("//").unwrap_or((line, ""));
    if line.is_empty() {
        return Ok(());
    }
    if let Some(label) = line.strip_prefix(":") {
        out.push(Instruction::_Label(Label::from_name(
            label.trim().to_string(),
        )));
        return Ok(());
    }
    if let Some(e) = line.strip_prefix("=") {
        let (label, value) = e
            .trim()
            .split_once(" ")
            .ok_or(serror!("malformed value label"))?;
        out.push(Instruction::_LabelValue(
            Label::from_name(label.to_string()),
            parse_num(value)?,
        ));
        return Ok(());
    }
    if let Some(text) = line.strip_prefix("\"") {
        for b in text.as_bytes() {
            out.push(Instruction::_Literal(*b as Value))
        }
        return Ok(());
    }
    if let Some(text) = line.strip_prefix("#") {
        for b in text.split(",") {
            out.push(Instruction::_Literal(parse_num(b)?))
        }
        return Ok(());
    }

    let mut tokens = line.split(" ");

    let mnemonic = tokens.next().unwrap();
    let x = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("first argument")
        .map(|e| e.ok_or(serror!("no first argument")))
        .flatten();
    let y = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("second argument")
        .map(|e| e.ok_or(serror!("no second argument")))
        .flatten();
    let z = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("third argument")
        .map(|e| e.ok_or(serror!("no third argument")))
        .flatten();

    out.push(match mnemonic.to_uppercase().as_str() {
        "NOA" => Instruction::Noa,
        "ADD" => Instruction::Add(x?, y?, z?),
        "SUB" => Instruction::Sub(x?, y?, z?),
        "MUL" => Instruction::Mul(x?, y?, z?),
        "DIV" => Instruction::Div(x?, y?, z?),
        "MOD" => Instruction::Mod(x?, y?, z?),
        "EQUAL" => Instruction::Equal(x?, y?, z?),
        "LESS" => Instruction::Less(x?, y?, z?),
        "GREATER" => Instruction::Greater(x?, y?, z?),
        "LNOT" => Instruction::Lnot(x?, y?),
        "BNOT" => Instruction::Bnot(x?, y?),
        "XOR" => Instruction::Xor(x?, y?, z?),
        "OR" => Instruction::Or(x?, y?, z?),
        "AND" => Instruction::And(x?, y?, z?),
        "SHL" => Instruction::Shl(x?, y?, z?),
        "SHR" => Instruction::Shr(x?, y?, z?),
        "MOVE" => Instruction::Move(x?, y?),
        "RSP" => Instruction::Rsp(x?),
        "MSP" => Instruction::Msp(x?),
        "JUMP" => Instruction::Jump(x?),
        "CJUMP" => Instruction::Cjump { cond: x?, pc: y? },
        "RPC" => Instruction::Rpc(x?),
        "SPAWN" => Instruction::Spawn {
            pc: x?,
            sp: y?,
            error: z?,
        },
        "HALT" => Instruction::Halt,
        "ACCEPT" => Instruction::Accept { id: x? },
        "READ" => Instruction::Read {
            id: x?,
            value: y?,
            error: z?,
        },
        "WRITE" => Instruction::Write {
            id: x?,
            value: y?,
            error: z?,
        },
        _ => bail!("what is {mnemonic}?"),
    });
    Ok(())
}

fn parse_arg(s: &str) -> Result<Argument> {
    let (s, offset) = s.split_once("+").unwrap_or((s, "0"));
    if s.is_empty() {
        bail!("argument too short")
    }
    Ok(match s.chars().nth(0).unwrap() {
        '\'' => Argument::Immediate(s.chars().nth(1).ok_or(serror!("no char"))? as u8 as i64),
        '#' => Argument::Immediate(parse_num(&s[1..])?),
        '@' => Argument::_LabelImmediate(Label::from_name(s[1..].to_owned()), parse_num(offset)?),
        '$' => Argument::Base(parse_num(&s[1..])?),
        '%' => Argument::_LabelBase(Label::from_name(s[1..].to_owned()), parse_num(offset)?),
        '*' => Argument::StackPointer(parse_num(&s[1..])?),
        '&' => {
            Argument::_LabelStackPointer(Label::from_name(s[1..].to_owned()), parse_num(offset)?)
        }
        '_' => Argument::_LabelStack(Label::from_name(s[1..].to_owned()), parse_num(offset)?),
        _ => Argument::Stack(parse_num(s)?),
    })
}
fn parse_num(s: &str) -> Result<Value> {
    if s.starts_with("-") {
        Value::from_str_radix(s, 16)
    } else {
        u64::from_str_radix(s, 16).map(|x| x as Value)
    }
    .map_err(|e| Error::simple(format!("{e}")).into())
}

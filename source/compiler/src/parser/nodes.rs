use super::{
    char_where, exact, exact_alts, ident, lazy, many, many_sep, maybe_extend, opt, ows,
    span::{ExpressionParserSpanExt, ParserSpanExt, Spanned},
    ws, Parser, ParserContextExt, ParserMapExt,
};
use crate::{
    ast::{BinaryOp, Expression, ExpressionKind, Literal, TypeA, Unit},
    parser::error::Error,
    Stage1Tree,
};

pub fn parse_node() -> Parser<Stage1Tree> {
    let syn = lazy(parse_block)
        | lazy(parse_let)
        | lazy(parse_assign)
        | lazy(parse_parens)
        | lazy(parse_literal_number)
        | lazy(parse_literal_string)
        | lazy(parse_literal_bool)
        | lazy(parse_while)
        | lazy(parse_if_else)
        | lazy(parse_struct_constructor)
        | lazy(parse_ref)
        | lazy(parse_deref)
        | lazy(parse_symbol);

    // TODO loop
    let syn = maybe_extend(syn, parse_invoke);
    let syn = maybe_extend(syn, parse_field);
    let syn = maybe_extend(syn, parse_bop);

    syn.context("node")
}

pub fn parse_parens() -> Parser<Stage1Tree> {
    exact("(") >> lazy(parse_node) << exact(")")
}

pub fn parse_ref() -> Parser<Stage1Tree> {
    let syn = exact("&") >> lazy(parse_node);
    syn.map(|node| ExpressionKind::Reference(Box::new(node)))
        .with_span()
        .context("reference")
}
pub fn parse_deref() -> Parser<Stage1Tree> {
    let syn = exact("*") >> lazy(parse_node);
    syn.map(|node| ExpressionKind::Deref(Box::new(node)))
        .with_span()
        .context("deref")
}

pub fn parse_struct_constructor() -> Parser<Stage1Tree> {
    let syn = parse_type().map(Box::new).span()
        & ows()
            >> exact("{")
            >> many_sep(
                ows() >> ident().span() & exact(":") >> ows() >> parse_node() << ows(),
                exact(","),
            )
            << ows()
            << exact("}");

    syn.map(|(r#type, fields)| ExpressionKind::StructConstructor { r#type, fields })
        .with_span()
        .context("struct constructor")
}

pub fn parse_if_else() -> Parser<Stage1Tree> {
    let syn = exact("if") >> ws() >> parse_node()
        & ows() >> parse_block()
        & opt(ows() >> exact("else") >> ows() >> parse_block());

    syn.map(|((cond, body_true), body_false)| ExpressionKind::If {
        condition: Box::new(cond),
        cond_false: Box::new(body_false.unwrap_or(Expression {
            data: Unit,
            span: Spanned::unspanned(()),
            k: ExpressionKind::Block(vec![]),
        })),
        cond_true: Box::new(body_true),
    })
    .with_span()
    .context("if statement")
}

pub fn parse_while() -> Parser<Stage1Tree> {
    let syn = exact("while") >> ows() >> parse_node() & ows() >> parse_block();

    syn.map(|(cond, body)| ExpressionKind::While {
        condition: Box::new(cond),
        body: Box::new(body),
    })
    .with_span()
    .context("while loop")
}

pub fn parse_field(lhs: Stage1Tree) -> Parser<Stage1Tree> {
    let syn = exact(".") >> ident().span();

    syn.map(move |field| ExpressionKind::Field {
        base: Box::new(lhs.clone()),
        field,
    })
    .with_span()
}

pub fn parse_index_bop(lhs: Stage1Tree) -> Parser<Stage1Tree> {
    let syn = exact("[") >> lazy(parse_node) << exact("]");

    syn.map(move |rhs| ExpressionKind::BinaryOp {
        op: BinaryOp::Index,
        lhs: Box::new(lhs.clone()),
        rhs: Box::new(rhs),
    })
    .with_span()
}

pub fn parse_bop(lhs: Stage1Tree) -> Parser<Stage1Tree> {
    let syn =
        ws() >> exact_alts(&[
            "+", "-", "*", "/", "%", "<", ">", "==", "!=", "&", "&&", "||", "|",
        ]) & ws() >> lazy(parse_node);

    syn.map(move |(op, rhs)| ExpressionKind::BinaryOp {
        op: match op {
            "+" => BinaryOp::Add,
            "-" => BinaryOp::Sub,
            "*" => BinaryOp::Mul,
            "/" => BinaryOp::Div,
            "%" => BinaryOp::Mod,
            "<" => BinaryOp::Less,
            ">" => BinaryOp::Greater,
            "==" => BinaryOp::Equal,
            "!=" => BinaryOp::NotEqual,
            "&" => BinaryOp::And,
            "|" => BinaryOp::Or,
            _ => unreachable!(),
        },
        lhs: Box::new(lhs.clone()),
        rhs: Box::new(rhs),
    })
    .with_span()
    .context("binary operator")
}

pub fn parse_symbol() -> Parser<Stage1Tree> {
    ident()
        .span()
        .map(|name| ExpressionKind::Symbol(name))
        .with_span()
        .context("symbol")
}

pub fn parse_invoke(base: Stage1Tree) -> Parser<Stage1Tree> {
    let syn = exact("(") >> many_sep(ows() >> parse_node() << ows(), exact(",")) << exact(")");

    syn.map(move |arguments| ExpressionKind::Invoke {
        function: Box::new(base.clone()),
        arguments,
    })
    .with_span()
    .context("function invokation")
}

pub fn parse_literal_number() -> Parser<Stage1Tree> {
    signed_integer()
        .map(|n| ExpressionKind::Literal(Literal::Int(n)))
        .with_span()
        .context("literal number")
}

pub fn parse_literal_bool() -> Parser<Stage1Tree> {
    (exact("true").map(|_| true) | exact("false").map(|_| false))
        .map(|n| ExpressionKind::Literal(Literal::Bool(n)))
        .with_span()
        .context("literal number")
}

pub fn parse_literal_string() -> Parser<Stage1Tree> {
    let char_not_special = char_where(|c| c != '"' && c != '\\');
    let char_not_special = char_not_special
        .map(|c| c as u8)
        .context("expected non-quote non-backslash character");

    let escape_byte = exact("\\x") >> char_where(|c| c.is_numeric())
        & char_where(|c| matches!(c, '0'..='9' | 'a'..='f' | 'A'..='F'));
    let escape_byte = escape_byte.map(|(a, b)| u8::from_str_radix(&format!("{a}{b}"), 16).unwrap());

    let escape_special = exact("\\") >> char_where(|c| matches!(c, 'n' | '"' | '\\'));
    let escape_special = escape_special.map(|c| match c {
        'n' => '\n',
        't' => '\t',
        'r' => '\r',
        '\\' => '\\',
        '"' => '"',
        _ => unreachable!(),
    } as u8);

    let syn_char = char_not_special | escape_byte | escape_special;
    let syn = exact("\"") >> many(syn_char) << exact("\"");
    syn.map(|s| ExpressionKind::Literal(Literal::String(String::from_utf8(s).unwrap())))
        .with_span()
}

pub fn parse_let() -> Parser<Stage1Tree> {
    let syn = exact("let") >> ws() >> ident().span()
        & opt(exact(":") >> ows() >> parse_type().map(Box::new).span())
        & ows() >> exact("=") >> ows() >> parse_node().map(Box::new);

    syn.map(|((name, explicit_type), value)| ExpressionKind::Let {
        name,
        explicit_type: explicit_type.unwrap_or_else(|| {
            Spanned::unspanned(Box::new(TypeA {
                name: None,
                args: vec![],
                lifetimes: vec![],
            }))
        }),
        value,
    })
    .with_span()
    .context("let statement")
}

pub fn parse_assign() -> Parser<Stage1Tree> {
    let syn = ident().span() & ows() >> exact("=") >> ows() >> parse_node().map(Box::new);

    syn.map(|(name, value)| ExpressionKind::Assign { name, value })
        .with_span()
        .context("variable assignment")
}

pub fn parse_type() -> Parser<TypeA<Option<String>>> {
    let syn = ident()
        & opt(exact("<") >> many_sep(ows() >> lazy(parse_type) << ows(), exact(",")) << exact(">"));
    let syn = syn
        .map(|(name, args)| TypeA {
            name: Some(name),
            args: args.unwrap_or_default(),
            lifetimes: vec![],
        })
        .context("type");

    let syn_hole = exact("_");
    let syn_hole = syn_hole
        .map(|_| TypeA {
            args: vec![],
            name: None,
            lifetimes: vec![],
        })
        .context("type hole");

    syn_hole | syn
}

pub fn parse_block() -> Parser<Stage1Tree> {
    let syn =
        exact("{") >> many_sep(ows() >> parse_node() << ows(), exact(";")) << ows() << exact("}");

    syn.map(|nodes| ExpressionKind::Block(nodes))
        .with_span()
        .context("block")
}

pub fn unsigned_integer() -> Parser<u64> {
    Parser(Box::new(move |s| {
        let mut k = 0;
        for (i, c) in s.data.char_indices() {
            if !c.is_numeric() && c != '_' {
                k = i;
                break;
            }
        }
        if k < 1 {
            return Err(Error::new(s.data("expected integer".to_owned())));
        }
        let (a, b) = s.split_at(k);
        Ok((b, a.data.replace("_", "").parse().unwrap(), None))
    }))
}
pub fn signed_integer() -> Parser<i64> {
    let syn = opt(exact("-")) & unsigned_integer();
    syn.map(|(sign, num)| num as i64 * if sign.is_some() { -1 } else { 1 })
}

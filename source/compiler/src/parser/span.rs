use super::{linemap::Source, Parser};
use crate::ast::{Expression, ExpressionKind, Unit};
use crate::error::Result;
use std::fmt::Debug;
use std::iter::once;
use std::{
    fmt::Display,
    ops::{Deref, DerefMut, Range},
    sync::Arc,
};

#[derive(Clone)]
pub struct Span {
    pub range: Range<usize>,
    pub source: Arc<Source>,
}

#[derive(Clone)]
pub struct Spanned<T> {
    pub data: T,
    span: Option<Span>,
}
impl<T> Deref for Spanned<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}
impl<T> DerefMut for Spanned<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl<T: PartialEq> PartialEq for Spanned<T> {
    fn eq(&self, other: &Self) -> bool {
        self.data == other.data
    }
}
impl<T: Eq> Eq for Spanned<T> {}
impl<T: PartialOrd> PartialOrd for Spanned<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.data.partial_cmp(&other.data)
    }
}
impl<T: Ord> Ord for Spanned<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.data.cmp(other)
    }
}

impl<'a> Spanned<&'a str> {
    pub fn new(source: Source, data: &'a str) -> Self {
        Self {
            data,
            span: Some(Span {
                range: 0..source.code.len(),
                source: Arc::new(source.to_owned()),
            }),
        }
    }
    pub fn err_maxlen(&self, len: usize) -> Self {
        self.split_at(len.min(self.len())).0
    }
}
impl<T> Spanned<T> {
    pub fn unspanned(data: T) -> Self {
        Self { data, span: None }
    }
    pub fn data<U>(&self, data: U) -> Spanned<U> {
        Spanned {
            data,
            span: self.span.clone(),
        }
    }
    pub fn map<U>(self, f: impl FnOnce(T) -> U) -> Spanned<U> {
        Spanned {
            data: f(self.data),
            span: self.span,
        }
    }
    pub fn try_map<U>(self, f: impl FnOnce(T) -> Result<U>) -> Result<Spanned<U>> {
        Ok(Spanned {
            data: f(self.data)?,
            span: self.span,
        })
    }
    pub fn span(&self) -> Option<Span> {
        self.span.clone()
    }
}

impl<T: Display> Display for Spanned<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.data)
    }
}
impl<T: Debug> Debug for Spanned<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{:?}[{}]",
            self.data,
            self.span
                .as_ref()
                .map(|s| format!("{:?}", s.range))
                .unwrap_or_default()
        )
    }
}

fn parse_with_span<T: 'static>(parser: Parser<T>) -> Parser<Spanned<T>> {
    Parser(Box::new(move |s| {
        let (rest, value, error) = parser(s.clone())?;
        let source_span = s.span.clone().unwrap(); // TODO perf
        let rest_span = rest.span.clone().unwrap();
        Ok((
            rest,
            Spanned {
                data: value,
                span: Some(Span {
                    source: source_span.source,
                    range: source_span.range.start..rest_span.range.start,
                }),
            },
            error,
        ))
    }))
}

pub trait ParserSpanExt<T> {
    fn span(self) -> Parser<Spanned<T>>;
}
impl<T: Sized + 'static> ParserSpanExt<T> for Parser<T> {
    fn span(self) -> Parser<Spanned<T>> {
        parse_with_span(self)
    }
}

pub trait ExpressionParserSpanExt<Data, Symbol> {
    fn with_span(self) -> Parser<Expression<Data, Symbol, String>>;
}

impl<Symbol: 'static> ExpressionParserSpanExt<Unit, Symbol>
    for Parser<ExpressionKind<Unit, Symbol, String>>
{
    fn with_span(self) -> Parser<Expression<Unit, Symbol, String>> {
        Parser(Box::new(move |s| {
            let (rest, value, error) = self(s.clone())?;
            let source_span = s.span.clone().unwrap(); // TODO perf
            let rest_span = rest.span.clone().unwrap();
            Ok((
                rest,
                Expression {
                    data: Unit,
                    span: Spanned {
                        data: (),
                        span: Some(Span {
                            source: source_span.source,
                            range: source_span.range.start..rest_span.range.start,
                        }),
                    },
                    k: value,
                },
                error,
            ))
        }))
    }
}

impl Spanned<&str> {
    pub fn split_at(&self, mid: usize) -> (Self, Self) {
        let (a, b) = self.data.split_at(mid);
        (
            Self {
                data: a,
                span: self.span.as_ref().map(|s| Span {
                    source: s.source.clone(),
                    range: s.range.start..s.range.start + mid,
                }),
            },
            Self {
                data: b,
                span: self.span.as_ref().map(|s| Span {
                    source: s.source.clone(),
                    range: s.range.start + mid..s.range.end,
                }),
            },
        )
    }
    pub fn take_while(&self, mut f: impl FnMut(char, char) -> bool) -> (Self, Self) {
        let mut last = self.data.len();
        for ((i, c), nc) in self
            .data
            .char_indices()
            .zip(self.data.chars().skip(1).chain(once('\x00')))
        {
            if !f(c, nc) {
                last = i;
                break;
            }
        }
        self.split_at(last)
    }
}

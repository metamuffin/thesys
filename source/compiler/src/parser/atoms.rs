use super::{error::Error, Parser, ParserFactory, ParserInner};
use std::{
    ops::{BitAnd, BitOr, Deref, Shl, Shr},
    sync::Arc,
};

impl<T> Deref for Parser<T> {
    type Target = ParserInner<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: 'static, U: 'static> Shr<Parser<U>> for Parser<T> {
    type Output = Parser<U>;
    fn shr(self, rhs: Parser<U>) -> Self::Output {
        seq(self, rhs).second()
    }
}
impl<T: 'static, U: 'static> Shl<Parser<U>> for Parser<T> {
    type Output = Parser<T>;
    fn shl(self, rhs: Parser<U>) -> Self::Output {
        seq(self, rhs).first()
    }
}
impl<T: 'static, U: 'static> BitAnd<Parser<U>> for Parser<T> {
    type Output = Parser<(T, U)>;
    fn bitand(self, rhs: Parser<U>) -> Self::Output {
        seq(self, rhs)
    }
}
impl<T: 'static> BitOr<Parser<T>> for Parser<T> {
    type Output = Parser<T>;
    fn bitor(self, rhs: Parser<T>) -> Self::Output {
        alt(self, rhs)
    }
}

fn alt<T: 'static>(a: Parser<T>, b: Parser<T>) -> Parser<T> {
    Parser(Box::new(move |s| match a(s.clone()) {
        Ok(x) => Ok(x),
        Err(a_err) => match b(s) {
            Ok(x) => Ok(x),
            Err(b_err) => Err(Error::combine(a_err, b_err)),
        },
    }))
}
fn seq<A: 'static, B: 'static>(a: Parser<A>, b: Parser<B>) -> Parser<(A, B)> {
    Parser(Box::new(move |s| {
        let (s, av, ae) = a(s)?;
        let (s, bv, be) = b(s)?;
        Ok((
            s,
            (av, bv),
            match (ae, be) {
                (None, None) => None,
                (None, Some(x)) => Some(x),
                (Some(x), None) => Some(x),
                (Some(x), Some(y)) => Some(Error::combine(x, y)),
            },
        ))
    }))
}

pub fn opt<T: 'static>(a: Parser<T>) -> Parser<Option<T>> {
    Parser(Box::new(move |s| match a(s.clone()) {
        Ok((rest, value, e)) => Ok((rest, Some(value), e)),
        Err(e) => Ok((s, None, Some(e))),
    }))
}
pub fn many<T: 'static>(p: Parser<T>) -> Parser<Vec<T>> {
    Parser(Box::new(move |mut s| {
        let mut values = Vec::new();
        let mut errors = Vec::new();
        loop {
            match p(s.clone()) {
                Ok((rest, v, e)) => {
                    s = rest;
                    values.push(v);
                    errors.extend(e)
                }
                Err(e) => {
                    let ke = errors.into_iter().reduce(|a, b| Error::combine(a, b));
                    return Ok((s, values, Some(Error::merge(e, ke))));
                }
            }
        }
    }))
}

pub fn exact(pat: &'static str) -> Parser<String> {
    Parser(Box::new(move |s| {
        if s.data.starts_with(&pat) {
            let (_, s) = s.split_at(pat.len());
            Ok((s, pat.to_owned(), None))
        } else {
            return Err(Error::new(s.data(format!("expected {pat:?}",))));
        }
    }))
}
pub fn make_factory<T: 'static>(p: Parser<T>) -> ParserFactory<T> {
    let p = Arc::new(p);
    Box::new(move || {
        let p = p.clone();
        Parser(Box::new(move |s| p(s)))
    })
}
pub fn exact_alts(pats: &'static [&'static str]) -> Parser<&'static str> {
    Parser(Box::new(move |s| {
        for pat in pats {
            if s.data.starts_with(pat) {
                let (_, s) = s.split_at(pat.len());
                return Ok((s, pat, None));
            }
        }
        return Err(Error::new(s.data(format!("expected one of {pats:?}",))));
    }))
}

pub trait ParserMapExt {
    type T;
    fn map<U: 'static, F: 'static + Fn(Self::T) -> U>(self, f: F) -> Parser<U>;
}
impl<T: 'static> ParserMapExt for Parser<T> {
    type T = T;
    fn map<U: 'static, F: 'static + Fn(Self::T) -> U>(self, f: F) -> Parser<U> {
        Parser(Box::new(move |s| {
            let (s, v, e) = self(s)?;
            Ok((s, f(v), e))
        }))
    }
}

pub trait ParserUnwrapExt {
    type T;
    fn unwrap_or_else<F: 'static + Fn() -> Self::T>(self, f: F) -> Parser<Self::T>;
}
impl<T: 'static> ParserUnwrapExt for Parser<Option<T>> {
    type T = T;
    fn unwrap_or_else<F: 'static + Fn() -> Self::T>(self, f: F) -> Parser<Self::T> {
        Parser(Box::new(move |s| {
            let (s, v, e) = self(s)?;
            Ok((
                s,
                match v {
                    Some(x) => x,
                    None => f(),
                },
                e,
            ))
        }))
    }
}

pub trait ParserTupleExt {
    type A;
    type B;
    fn first(self) -> Parser<Self::A>;
    fn second(self) -> Parser<Self::B>;
}
impl<A: 'static, B: 'static> ParserTupleExt for Parser<(A, B)> {
    type A = A;
    type B = B;
    fn first(self) -> Parser<Self::A> {
        self.map(|(a, _b)| a)
    }
    fn second(self) -> Parser<Self::B> {
        self.map(|(_a, b)| b)
    }
}

pub trait ParserContextExt {
    type T;
    fn context(self, context: &'static str) -> Parser<Self::T>;
}
impl<T: 'static> ParserContextExt for Parser<T> {
    type T = T;
    fn context(self, context: &'static str) -> Parser<Self::T> {
        Parser(Box::new(move |s| {
            let span = s.data(());
            let (s, v, e) = self(s).map_err(|e| e.context(span.data(context)))?;
            Ok((s, v, e.map(|e| e.context(span.data(context)))))
        }))
    }
}

// TODO extend multiple times until it stops working.
pub fn maybe_extend<T: Clone + 'static>(p: Parser<T>, ex: fn(T) -> Parser<T>) -> Parser<T> {
    Parser(Box::new(move |s| {
        let (rest, base, err) = p(s)?;
        match ex(base.clone())(rest.clone()) {
            Ok((a, b, c)) => Ok((a, b, c)),
            Err(_e) => Ok((rest, base, err)), // TODO handle error?
        }
    }))
}

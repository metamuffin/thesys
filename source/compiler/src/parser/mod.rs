use crate::{ast::Qualified, Stage1Declaration};
use error::Error;
use linemap::Source;
use span::{ParserSpanExt, Spanned};
use std::sync::LazyLock;
use toplevel::parse_file;

pub mod assembly;
pub mod atoms;
pub mod error;
pub mod linemap;
pub mod nodes;
pub mod span;
pub mod toplevel;

pub use atoms::*;

pub type ParserFactory<T> = Box<dyn Fn() -> Parser<T>>;
pub type ParserInner<T> =
    Box<dyn Fn(Spanned<&str>) -> Result<(Spanned<&str>, T, Option<Error>), Error>>;

pub struct Parser<T>(pub ParserInner<T>);

fn lazy<T: 'static>(pg: impl FnOnce() -> Parser<T> + 'static) -> Parser<T> {
    let l = LazyLock::new(pg);
    Parser(Box::new(move |s| l(s)))
}

fn ows() -> Parser<()> {
    Parser(Box::new(move |s| match ws()(s.clone()) {
        Ok((rest, (), _)) => Ok((rest, (), None)),
        Err(_) => Ok((s, (), None)),
    }))
}

fn ws() -> Parser<()> {
    enum Comment {
        Line,
        Block,
    }
    Parser(Box::new(move |s| {
        let mut last_c = '\x00';
        let mut comment = None;
        let (ws, rest) = s.take_while(|c, cn| {
            if comment.is_none() && c == '/' && cn == '/' {
                comment = Some(Comment::Line)
            }
            if comment.is_none() && c == '/' && cn == '*' {
                comment = Some(Comment::Block)
            }
            let ignore = c.is_whitespace() || comment.is_some();
            if match comment {
                Some(Comment::Block) => last_c == '*' && c == '/',
                Some(Comment::Line) => c == '\n',
                None => false,
            } {
                comment = None;
            }
            last_c = c;
            ignore
        });
        if ws.data.is_empty() {
            return Err(Error::new(
                s.data(format!("expected whitespace found {}", rest.err_maxlen(5))),
            ));
        }
        Ok((rest, (), None))
    }))
}

pub fn char_where(f: fn(char) -> bool) -> Parser<char> {
    Parser(Box::new(move |s| {
        let (c, rest) = s.split_at(1);
        let cc = c.chars().next().unwrap();
        if f(cc) {
            Ok((rest, cc, None))
        } else {
            Err(Error::new(c.data("char wasnt expected".to_string())))
        }
    }))
}

pub fn parse_anything(end: char) -> Parser<String> {
    Parser(Box::new(move |s| {
        let (stuff, rest) = s.take_while(|c, _| c != end);
        Ok((rest, stuff.data.to_string(), None))
    }))
}

fn many_sep<T: 'static, U: 'static>(p: Parser<T>, sep: Parser<U>) -> Parser<Vec<T>> {
    let p = make_factory(p);
    let sep = make_factory(sep);
    let syn = opt(p() & opt(many(sep() >> p()) & opt(sep())));
    syn.map(|e| {
        e.map(|(first, rest)| {
            let (mut v, _last_sep) = rest.unwrap_or_default();
            v.insert(0, first);
            v
        })
        .unwrap_or_default()
    })
    // many(p << opt(sep)) // lax parser
}

pub fn qident() -> Parser<Qualified<Spanned<String>>> {
    let syn = opt(exact("::")) & many(ident().span() << exact("::")) & ident().span();
    syn.map(|((abs, qual), fin)| Qualified {
        absolute: abs.is_some(),
        qual,
        fin,
    })
}

pub fn ident() -> Parser<String> {
    Parser(Box::new(move |s| {
        let mut k = 0;
        for (i, c) in s.data.char_indices() {
            if !c.is_alphanumeric() && c != '_' {
                k = i;
                break;
            }
        }
        if k == 0 {
            return Err(Error::new(s.data("expected identifier".to_owned())));
        }
        let (a, b) = s.split_at(k);
        Ok((b, a.data.to_string(), None))
    }))
}
pub fn scoped_ident() -> Parser<Vec<String>> {
    many_sep(ident(), exact("::"))
}

pub fn parse_source(source: Source) -> Result<Vec<Stage1Declaration>, Error> {
    let s = Spanned::new(source.clone(), &source.code);

    let (rest, decls, err) = parse_file()(s)?;
    if rest.len() > 0 {
        if let Some(err) = err {
            Err(err)?
        } else {
            unreachable!()
        }
    }
    Ok(decls)
}

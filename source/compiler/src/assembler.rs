use crate::{
    error::Result,
    instructions::{Argument, Instruction, Value},
    linker::LinkableObject,
};

pub fn assemble(insts: Vec<Instruction>) -> Result<LinkableObject> {
    let mut ob = LinkableObject::default();
    for i in insts {
        let position = ob.data.len();
        match i {
            Instruction::_Label(l) => ob.label_exports.push((l.to_string(), position)),
            Instruction::_LabelValue(l, v) => ob.const_exports.push((l.to_string(), v)),
            Instruction::_Literal(x) => ob.data.push(x),
            i => {
                let mut opcode = i.index() as Value;
                let mut am_offset = 8;
                ob.data.push(0);
                i.clone().map(|arg| {
                    let arg = match arg {
                        Argument::_LabelImmediate(label, offset) => {
                            ob.label_uses.push((label.to_string(), ob.data.len()));
                            Argument::Immediate(offset)
                        }
                        Argument::_LabelStack(label, offset) => {
                            ob.label_uses.push((label.to_string(), ob.data.len()));
                            Argument::Stack(offset)
                        }
                        Argument::_LabelStackPointer(label, offset) => {
                            ob.label_uses.push((label.to_string(), ob.data.len()));
                            Argument::StackPointer(offset)
                        }
                        Argument::_LabelBase(label, offset) => {
                            ob.label_uses.push((label.to_string(), ob.data.len()));
                            Argument::Base(offset)
                        }
                        am => am,
                    };
                    opcode |= (arg.mode_index() << am_offset) as Value;
                    ob.data.push(arg.value());
                    am_offset += 2;
                    arg
                });
                ob.data[position] = opcode;
            }
        }
    }
    Ok(ob)
}

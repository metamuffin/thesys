use compiler::main_lib::actual_main;
use std::process::exit;

fn main() {
    match actual_main() {
        Ok(_) => (),
        Err(e) => {
            println!("{e}");
            exit(1)
        }
    }
}

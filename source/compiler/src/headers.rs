use crate::{
    ast::{Declaration, Function, Struct, Use},
    error::Result,
    parser::{linemap::Source, parse_source},
};
use std::fmt::Write;

pub fn generate_header(source: Source) -> Result<String> {
    let decls = parse_source(source)?;

    let mut out = String::new();

    for decl in decls {
        match decl.data {
            Declaration::Function(Function {
                name,
                arguments,
                return_type,
                body: _,
                visibility: _,
                generics,
            }) => {
                write!(out, "extern fn {name}{generics}(")?;
                for (name, ty) in arguments {
                    write!(out, "{name}: {ty}, ")?;
                }
                write!(out, ") -> {return_type};")?;
            }
            Declaration::Struct(Struct {
                fields,
                name,
                visibility,
                generics,
            }) => {
                write!(out, "{visibility}struct {name}{generics} {{ ")?;
                for (name, ty) in fields {
                    write!(out, "{name}: {ty}, ")?;
                }
                write!(out, "}}")?;
            }
            Declaration::Use(Use {
                item,
                visibility: _,
            }) => {
                write!(out, "use {item}")?;
            }
            Declaration::Assembly(_) => (),
            Declaration::Impl(_) => todo!(),
        }
        writeln!(out)?;
    }

    Ok(out)
}

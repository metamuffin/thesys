#![feature(result_flattening, iterator_try_collect, try_trait_v2)]
use crate::{
    ast::Declaration, code::generate_code, error::Result, symbols::resolve_symbols,
    types::resolve_types,
};
use assembler::assemble;
use ast::{Expression, Function, TypeA, Unit};
use code::{DataStore, ValueStore};
use error::{Context, Error};
use instructions::{Argument, Instruction, Label};
use linker::LinkableObject;
use parser::{assembly::parse_assembly, linemap::Source, parse_source, span::Spanned};
use std::{collections::BTreeMap, fmt::Display};
use symbols::{resolve_inner_type, resolve_type_symbols, SymbolFrame, ID};
use types::{PartialValueTypeStore, TypeDecl, TypeStore, Types, ValueTypeStore};

pub mod assembler;
pub mod ast;
pub mod code;
pub mod error;
pub mod headers;
pub mod instructions;
pub mod linker;
pub mod main_lib;
pub mod ownership;
pub mod parser;
pub mod symbols;
pub mod types;

pub type Stage1Declaration = Spanned<Declaration<Unit, Spanned<String>, String>>;
pub type Stage1Tree = Expression<Unit, Spanned<String>, String>;
pub type Stage2Tree = Expression<Unit, Spanned<String>, Either<ID, String>>;
pub type Stage3Tree = Expression<TypeA<Option<ID>>, Spanned<Either<ID, String>>, ID>;
pub type Stage4Tree = Expression<TypeA<ID>, Spanned<ID>, ID>;
pub type Stage4bTree = Expression<(TypeA<ID>, Vec<ID>), Spanned<ID>, ID>;

pub fn program_entry() -> LinkableObject {
    let label_halt = Label::new_named("halt".to_string());
    let mut out = Vec::new();
    out.push(Instruction::Move(
        Argument::_LabelImmediate(label_halt.clone(), 0),
        Argument::Stack(-1),
    ));
    out.push(Instruction::Jump(Argument::_LabelImmediate(
        Label::from_name("Gfun_main".to_string()),
        0,
    )));
    out.push(Instruction::_Label(label_halt));
    out.push(Instruction::Halt);
    assemble(out).unwrap()
}

pub type LabelStore = BTreeMap<ID, Label>;

pub fn compile(source: Source, headers: BTreeMap<String, Source>) -> Result<Vec<Instruction>> {
    let decls = parse_source(source)?;
    let headers = headers
        .into_iter()
        .map(|(k, v)| Ok::<_, Error>((k, parse_source(v)?)))
        .try_collect::<BTreeMap<_, _>>()?;

    let mut globals = SymbolFrame::root();
    let mut s_type = TypeStore::default();
    let mut s_value_type = ValueTypeStore::default();
    let mut s_function_labels = LabelStore::default();
    let mut s_symbol_value = ValueStore::default();

    let types = Types::register(&mut globals, &mut s_type);

    for decl in &decls {
        register_decl(
            decl,
            &headers,
            &mut s_function_labels,
            &mut s_type,
            &mut s_symbol_value,
            &mut s_value_type,
            &types,
            &mut globals,
        )?;
    }

    let mut out = Vec::new();
    let mut s_data = DataStore::new();

    for decl in decls {
        match decl.data {
            Declaration::Function(func) => {
                out.extend(
                    compile_function(
                        &func,
                        &s_function_labels,
                        &s_type,
                        &mut s_value_type,
                        &mut s_data,
                        &s_symbol_value,
                        &globals,
                        &types,
                    )
                    .context_spanned(func.name.data("within this function"))?,
                );
            }
            Declaration::Assembly(assembly) => {
                let assembly = parse_assembly(&assembly).context("parsing assembly block")?;
                out.extend(assembly);
            }
            _ => (),
        }
    }

    for (data, label) in s_data {
        out.push(Instruction::_Label(label));
        out.extend(data.iter().map(|b| Instruction::_Literal(*b)))
    }

    debug!(
        "{}",
        out.iter()
            .map(|i| format!("{i}"))
            .collect::<Vec<String>>()
            .join("\n")
    );
    Ok(out)
}

fn register_decl(
    decl: &Stage1Declaration,
    headers: &BTreeMap<String, Vec<Stage1Declaration>>,
    s_function_labels: &mut LabelStore,
    s_type: &mut TypeStore,
    s_symbol_value: &mut ValueStore,
    s_value_type: &mut ValueTypeStore,
    types: &Types,
    globals: &mut SymbolFrame,
) -> Result<()> {
    match &decl.data {
        Declaration::Use(us) => {
            let he = headers.get(&us.item.qual[0].data).ok_or(
                Error::simple("header not found")
                    .with_ref(decl.data("required for this use statement".to_owned())),
            )?;
            let decl = he
                .iter()
                .find(|d| {
                    d.name()
                        .map(|s| s.data == us.item.fin.data)
                        .unwrap_or_default()
                })
                .ok_or(
                    Error::simple("Header does not contain the declaration.")
                        .with_ref(decl.data("required for this use statement".to_owned())),
                )?;
            register_decl(
                decl,
                headers,
                s_function_labels,
                s_type,
                s_symbol_value,
                s_value_type,
                types,
                globals,
            )?;
        }
        Declaration::Function(func) => {
            debug!("-- global decl: function {}", func.name);
            let id = globals.new_new_value(func.name.clone())?;

            let label = Label::from_name(format!("Gfun_{}", func.name.data));
            let args = func
                .arguments
                .iter()
                .map(|(_, a)| {
                    let arg = resolve_inner_type(a.to_owned(), &globals)?;
                    let arg = arg.unwrap()?;
                    Ok::<_, Error>(arg)
                })
                .try_collect::<Vec<_>>()?;
            let ret = resolve_inner_type(func.return_type.clone(), &globals)?;
            let ret = ret.unwrap()?;

            let mut typeargs = Vec::new();
            typeargs.push(ret);
            typeargs.extend(args);

            s_symbol_value.insert(id, Argument::_LabelImmediate(label.clone(), 0));
            s_function_labels.insert(id, label);
            s_value_type.insert(
                id,
                TypeA {
                    name: types.function,
                    args: typeargs,
                    lifetimes: Vec::new(),
                },
            );
        }
        Declaration::Struct(stru) => {
            debug!("-- global decl: struct {:?}", stru.name);
            let id = globals.new_type(stru.name.clone())?;
            s_type.insert(
                id,
                TypeDecl::Struct {
                    fields: stru
                        .fields
                        .clone()
                        .into_iter()
                        .map(|(k, v)| {
                            Ok::<_, Error>((
                                k.data.clone(),
                                ID::new_named(k.data),
                                resolve_inner_type(v, &globals)?.unwrap()?,
                            ))
                        })
                        .try_collect()?,
                },
            );
        }
        Declaration::Assembly(_) => (),
        Declaration::Impl(_) => todo!(),
    }
    Ok(())
}

fn compile_function(
    func: &Function<Unit, Spanned<String>, String>,
    s_function_labels: &LabelStore,
    s_type: &TypeStore,
    s_value_type: &mut ValueTypeStore,
    s_data: &mut DataStore,
    s_symbol_value: &ValueStore,
    globals: &SymbolFrame,
    types: &Types,
) -> Result<Vec<Instruction>> {
    let Some(body) = &func.body else {
        return Ok(vec![]);
    };

    debug!("-- compile: function {:?}", func.name);
    debug!("{func}");

    let label = s_function_labels
        .get(&globals.get_value(&func.name).unwrap())
        .unwrap()
        .clone();

    let mut s_value_ptype = PartialValueTypeStore::default();
    s_value_ptype.extend(s_value_type.iter().map(|(k, v)| (k.to_owned(), v.some())));

    let rettype = resolve_inner_type(func.return_type.clone(), &globals)
        .context("resolving return type")?
        .unwrap()
        .context("in func return type")?;

    let mut body: Stage2Tree = body.clone().map_symbols(|s| s, &|s| Either::Right(s));

    resolve_type_symbols(&mut body, globals)?;

    let mut body: Stage3Tree = body
        .clone()
        .map(|Unit| TypeA::hole())
        .map_symbols(|s| s.map(Either::Right), &|s| s.left().unwrap());

    let mut scope = globals.create_child();
    let mut args = Vec::new();
    for (name, tn) in &func.arguments {
        let val = scope.new_value(name.clone());
        let tn = resolve_inner_type(tn.clone(), &mut scope)?;
        s_value_ptype.insert(val.clone(), tn);
        args.push(val);
    }

    loop {
        debug!("resolving symbols...");
        let sr = resolve_symbols(&mut body, &mut scope, s_type)?;

        debug!("{body}");

        debug!("inferring types...");
        let tr = resolve_types(&mut body, &s_type, &mut s_value_ptype, &types, &rettype)?;

        debug!("{body}");

        if tr + sr == 0 {
            break;
        }
    }

    let body: Stage4Tree = body
        .try_map_symbols(
            &|s| s.try_map(|s| s.clone().left().ok_or(serror!("unresolved symbol: {s}"))),
            &|x| Ok(x),
        )?
        .try_map(&|t| Ok(t.unwrap()?))?;

    for (k, v) in s_value_ptype {
        s_value_type.insert(k, v.unwrap()?);
    }

    let insts = generate_code(
        label,
        &body,
        &args,
        &rettype,
        &s_type,
        &s_value_type,
        &s_symbol_value,
        s_data,
    )
    .context("during code generation")?;
    debug!(
        "{}",
        insts
            .iter()
            .map(|i| format!("{i}"))
            .collect::<Vec<String>>()
            .join("\n")
    );
    debug!("--------------------------------------------");

    Ok(insts)
}

#[derive(Debug, Clone, Copy)]
pub enum Either<A, B> {
    Left(A),
    Right(B),
}
impl<A: Display, B: Display> Display for Either<A, B> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Either::Left(a) => write!(f, "{a}"),
            Either::Right(b) => write!(f, "{b}"),
        }
    }
}
impl<A, B> Either<A, B> {
    pub fn left(self) -> Option<A> {
        match self {
            Either::Left(a) => Some(a),
            Either::Right(_) => None,
        }
    }
    pub fn try_to_left(&mut self, f: impl FnOnce(&B) -> Result<A>) -> Result<()> {
        match self {
            Either::Left(_) => Ok(()),
            Either::Right(x) => {
                *self = Either::Left(f(x)?);
                Ok(())
            }
        }
    }
}

use crate::{
    assembler::assemble,
    compile,
    error::{Context, Error, Result},
    headers::generate_header,
    linker::{link_objects, LinkableObject},
    parser::{assembly::parse_assembly, linemap::Source},
    program_entry, serror,
};
use std::{
    collections::BTreeMap,
    fs::{read_to_string, File},
    io::Write,
    path::PathBuf,
    str::FromStr,
};

pub fn actual_main() -> Result<()> {
    let mut args = std::env::args();
    let bname = args.next().unwrap_or("compiler".to_owned());
    match args.next().unwrap_or_default().as_str() {
        "help" => {
            eprintln!("USGAE:");
            eprintln!("{bname} help");
            eprintln!("\tGet this help");
            eprintln!("{bname} -c <source> <object> {{-H<headername>:<path>}}");
            eprintln!("\tCompile rusty source code");
            eprintln!("{bname} -ch <source> <header>");
            eprintln!("\tGenerate \"header\" from source code");
            eprintln!("{bname} -s <source> <object>");
            eprintln!("\tAssemble stuff");
            eprintln!("{bname} -l <objects..> <program>");
            eprintln!("\tLink objects to form a program");
        }
        "-ch" => {
            let sourcepath = PathBuf::from_str(&args.next().expect("expected source")).unwrap();
            let source = read_to_string(&sourcepath)?;
            let output = args.next().expect("expected output");
            let header = generate_header(Source::new(sourcepath, &source))?;
            File::create(output)?.write_all(header.as_bytes())?;
        }
        "-c" => {
            let sourcepath = PathBuf::from_str(&args.next().expect("expected source")).unwrap();
            let source = read_to_string(&sourcepath)?;
            let output = args.next().expect("expected output");
            let headers = args
                .map(|arg| {
                    let an = arg.strip_prefix("-H").unwrap();
                    let (hname, hpath) = an.split_once(":").unwrap();
                    let hsource = read_to_string(&hpath)?;
                    Ok::<_, Error>((hname.to_owned(), Source::new(hpath.into(), &hsource)))
                })
                .try_collect::<BTreeMap<_, _>>()?;
            let insts = compile(Source::new(sourcepath, &source), headers)?;
            let object = assemble(insts)?;
            File::create(output)?.write_all(object.dump().as_bytes())?;
        }
        "-s" => {
            let source = read_to_string(args.next().expect("expected sourcee"))?;
            let output = args.next().expect("expected output");
            let insts = parse_assembly(&source)?;
            let object = assemble(insts)?;
            File::create(output)?.write_all(object.dump().as_bytes())?;
        }
        "-l" => {
            let mut objects = Vec::new();
            let mut args = args.collect::<Vec<_>>();
            let output = args.pop().ok_or(serror!("output missing"))?;
            objects.push(("_entry".to_string(), program_entry()));
            for a in args {
                let a = PathBuf::from_str(&a).map_err(|_| serror!(""))?;
                let source = read_to_string(&a)?;
                objects.push((
                    a.file_stem().unwrap().to_str().unwrap().to_string(),
                    LinkableObject::parse(&source).context(&format!("while parsing {a:?}"))?,
                ));
            }
            let program = link_objects(objects).context("during linking")?;
            File::create(output)?.write_all(format!("{program:?}").as_bytes())?;
        }
        _ => panic!("usage: {bname} help"),
    }

    Ok(())
}

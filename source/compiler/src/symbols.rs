use crate::{
    ast::{ExpressionKind, TypeA},
    bail,
    error::{Error, Result},
    parser::span::Spanned,
    serror,
    types::{TypeDecl, TypeStore},
    Either, Stage2Tree, Stage3Tree,
};
use std::{
    borrow::Borrow,
    collections::BTreeMap,
    fmt::{Debug, Display, Formatter},
    hash::Hash,
    sync::{
        atomic::{AtomicU64, Ordering},
        RwLock,
    },
};

static ID_COUNTER: AtomicU64 = AtomicU64::new(0);
static ID_NAMES: RwLock<BTreeMap<ID, String>> = RwLock::new(BTreeMap::new());

#[derive(Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ID(u64);
impl Display for ID {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}~{}",
            ID_NAMES
                .read()
                .unwrap()
                .get(self)
                .map(|s| s.as_str())
                .unwrap_or("S"),
            self.0
        )
    }
}
impl Debug for ID {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{self}"))
    }
}
impl ID {
    pub fn new() -> Self {
        Self(ID_COUNTER.fetch_add(1, Ordering::Relaxed))
    }
    pub fn new_named(name: String) -> Self {
        let id = ID::new();
        ID_NAMES.write().unwrap().insert(id, name);
        id
    }
    pub fn get_name(&self) -> String {
        ID_NAMES.read().unwrap().get(&self).unwrap().to_string()
    }
}

pub fn resolve_type_symbols(ast: &mut Stage2Tree, globals: &SymbolFrame) -> Result<()> {
    ast.traverse_pre(&(), &mut |_data, _span, _pc: &(), kind| {
        match kind {
            ExpressionKind::Let { explicit_type, .. } => {
                resolve_inner_type_mut(explicit_type.data.as_mut(), globals)?;
            }
            ExpressionKind::StructConstructor { r#type, .. } => {
                resolve_inner_type_mut(r#type.data.as_mut(), globals)?;
            }
            _ => (),
        }
        Ok(())
    })
}

pub fn resolve_inner_type(
    a: TypeA<Option<String>>,
    scope: &SymbolFrame,
) -> Result<TypeA<Option<ID>>> {
    Ok(TypeA {
        name: a
            .name
            .as_ref()
            .map(|n| {
                scope
                    .get_type(&n)
                    .map(|s| s.data)
                    .ok_or(serror!("could not resolve type symbol {:?}", a.name))
            })
            .transpose()?,
        args: a
            .args
            .into_iter()
            .map(|a| resolve_inner_type(a, scope))
            .try_collect()?,
        lifetimes: vec![],
    })
}

pub fn resolve_inner_type_mut(
    a: &mut TypeA<Option<Either<ID, String>>>,
    scope: &SymbolFrame,
) -> Result<()> {
    if let Some(n) = &mut a.name {
        n.try_to_left(|name| {
            scope
                .get_type(&name)
                .map(|s| s.data)
                .ok_or(serror!("could not resolve type symbol {:?}", name).into())
        })?
    };
    for a in &mut a.args {
        resolve_inner_type_mut(a, scope)?;
    }
    Ok(())
}

pub fn resolve_symbols(
    ast: &mut Stage3Tree,
    scope: &mut SymbolFrame,
    s_type: &TypeStore,
) -> Result<usize> {
    let resolve_inner =
        |a: &mut Stage3Tree, scope: &mut SymbolFrame| resolve_symbols(a, scope, s_type);

    match &mut ast.k {
        ExpressionKind::Let {
            name,
            explicit_type: _,
            value,
        } => {
            resolve_inner(value.as_mut(), scope)?;
            scope.register(name);
        }
        ExpressionKind::Assign { name, value } => {
            name.try_to_left(|name| {
                Ok(*scope
                    .get_value(&name)
                    .ok_or(serror!("could not resolve symbol {name}"))?)
            })?;
            resolve_inner(value, &mut scope.create_child())?;
        }
        ExpressionKind::Symbol(name) => {
            name.try_to_left(|name| {
                Ok(*scope.get_value(name.as_str()).ok_or(
                    Error::simple(format!("could not resolve symbol {:?}", name))
                        .with_ref(ast.span.data("What's this?!".to_owned())),
                )?)
            })?;
        }
        ExpressionKind::Reference(node) => {
            resolve_inner(node.as_mut(), scope)?;
        }
        ExpressionKind::Deref(node) => {
            resolve_inner(node.as_mut(), scope)?;
        }
        ExpressionKind::Block(content) => {
            for e in content {
                resolve_inner(e, scope)?;
            }
        }
        ExpressionKind::Invoke {
            function,
            arguments,
        } => {
            resolve_inner(function.as_mut(), scope)?;
            for a in arguments {
                resolve_inner(a, scope)?;
            }
        }
        ExpressionKind::If {
            condition,
            cond_true,
            cond_false,
        } => {
            resolve_inner(condition, &mut scope.create_child())?;
            resolve_inner(cond_true, &mut scope.create_child())?;
            resolve_inner(cond_false, &mut scope.create_child())?;
        }
        ExpressionKind::While { condition, body } => {
            resolve_inner(condition, &mut scope.create_child())?;
            resolve_inner(body, &mut scope.create_child())?;
        }
        ExpressionKind::Literal(_) => (),
        ExpressionKind::BinaryOp { op: _, lhs, rhs } => {
            resolve_inner(lhs, &mut scope.create_child())?;
            resolve_inner(rhs, &mut scope.create_child())?;
        }
        ExpressionKind::StructConstructor { r#type, fields } => {
            if let Some(r#type) = &r#type.name {
                let tdecl = &s_type[&r#type];
                let TypeDecl::Struct {
                    fields: decl_fields,
                } = tdecl
                else {
                    bail!("trying to construct a non-struct");
                };
                for (key, value) in fields {
                    key.try_to_left(|name| {
                        Ok(decl_fields
                            .iter()
                            .find(|(n, _, _)| n == name)
                            .ok_or(serror!("field does not exist"))
                            .map(|(_, id, _)| *id)?)
                    })?;
                    resolve_inner(value, &mut scope.create_child())?;
                }
            }
        }
        ExpressionKind::Field { base, field } => {
            resolve_inner(base, &mut scope.create_child())?;
            if let Some(r#type) = base.data.name {
                let tdecl = &s_type[&r#type];
                let TypeDecl::Struct {
                    fields: decl_fields,
                } = tdecl
                else {
                    bail!("trying to construct a non-struct");
                };
                field.try_to_left(|name| {
                    Ok(decl_fields
                        .iter()
                        .find(|(n, _, _)| n == name)
                        .ok_or(serror!("field does not exist"))
                        .map(|(_, id, _)| *id)?)
                })?;
            }
        }
    }
    Ok(0) // TODO
}

#[derive(Debug)]
pub struct SymbolFrame<'a> {
    types: ScopedStore<'a, String, Spanned<ID>>,
    values: ScopedStore<'a, String, Spanned<ID>>,
}
impl<'a> SymbolFrame<'a> {
    pub fn root() -> Self {
        Self {
            types: ScopedStore::root(),
            values: ScopedStore::root(),
        }
    }
    pub fn create_child(&'a self) -> Self {
        Self {
            types: self.types.create_child(),
            values: self.values.create_child(),
        }
    }
    pub fn new_new_value(&mut self, name: Spanned<String>) -> Result<ID> {
        if let Some(x) = self.values.get(&name.data) {
            return Err(Error::simple(format!("{name} is defined multiple times"))
                .with_context(x.data("first declaration is here".to_string()))
                .with_ref(name.data("second declaration is here".to_string())));
        }
        let symbol = name.clone().map(ID::new_named);
        self.values.insert(name.data, symbol.clone());
        Ok(symbol.data)
    }
    pub fn new_value(&mut self, name: Spanned<String>) -> ID {
        let symbol = name.clone().map(ID::new_named);
        self.values.insert(name.data, symbol.clone());
        symbol.data
    }
    pub fn register(&mut self, name: &mut Spanned<Either<ID, String>>) -> ID {
        match &name.data {
            Either::Left(id) => {
                self.values.insert(id.get_name(), name.data(*id));
                *id
            }
            Either::Right(s) => {
                let id = self.new_value(name.data(s.to_string()));
                name.data = Either::Left(id);
                id
            }
        }
    }
    pub fn new_type(&mut self, name: Spanned<String>) -> Result<ID> {
        if let Some(x) = self.types.get(&name.data) {
            return Err(Error::simple(format!("{name} is defined multiple times"))
                .with_context(x.data("first declaration is here".to_string()))
                .with_ref(name.data("second declaration is here".to_string())));
        }
        let symbol = name.clone().map(ID::new_named);
        self.types.insert(name.data, symbol.clone());
        Ok(symbol.data)
    }
    pub fn get_value(&self, name: &str) -> Option<Spanned<ID>> {
        self.values.get(name).cloned()
    }
    pub fn get_type(&self, name: &str) -> Option<Spanned<ID>> {
        self.types.get(name).cloned()
    }
}

#[derive(Debug)]
pub struct ScopedStore<'a, K, V> {
    parent: Option<&'a ScopedStore<'a, K, V>>,
    values: BTreeMap<K, V>,
}
impl<'a, K, V> Default for ScopedStore<'a, K, V> {
    fn default() -> Self {
        Self {
            parent: Default::default(),
            values: Default::default(),
        }
    }
}
impl<'a, K: Ord + Eq + Clone, V> ScopedStore<'a, K, V> {
    pub fn root() -> Self {
        Self {
            parent: None,
            values: BTreeMap::new(),
        }
    }
    pub fn insert(&mut self, key: K, value: V) -> &V {
        self.values.insert(key.clone(), value);
        self.get(&key).unwrap()
    }
    pub fn get<'b, Q: ?Sized>(&'b self, key: &Q) -> Option<&'b V>
    where
        K: Borrow<Q>,
        Q: Ord + Eq,
    {
        self.values
            .get(key)
            .or_else(|| self.parent.map(|p| p.get(key)).flatten())
    }
    pub fn create_child(&'a self) -> Self {
        Self {
            parent: Some(self),
            values: BTreeMap::new(),
        }
    }
}

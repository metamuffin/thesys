use crate::{
    ast::{Expression, ExpressionKind as EK, TypeA},
    error::Error,
    parser::span::Spanned,
    symbols::ID,
    Stage4Tree, Stage4bTree,
};
use std::collections::{BTreeMap, BTreeSet};

struct Scope {
    owned: BTreeSet<ID>,
    needs_free: bool,
}

pub fn analyse_ownership(tree: Stage4Tree) -> Result<Stage4bTree, Error> {
    // let mut tree = tree.map(|d| {
    //     (
    //         d,
    //         Scope {
    //             owned: BTreeSet::new(),
    //             needs_free: false,
    //         },
    //     )
    // });
    // fn traverse(node: &mut Expression<(TypeA<ID>, Scope), Spanned<ID>, ID>) {
    //     let scope = &mut node.data.1;
    //     match &mut node.k {
    //         EK::Let {
    //             name,
    //             explicit_type,
    //             value,
    //         } => {
    //             scope.owned.insert(name.data);
    //         }
    //         EK::Assign { name, value } => {
    //             // traverse(node);
    //             todo!()
    //         }
    //         EK::Symbol(symbol) => {
    //             scope.owned.remove(&symbol.data);
    //         }
    //         EK::Block(elems) => {
    //             for e in elems {
    //                 traverse(e);
    //             }
    //         }
    //         EK::Reference(node) => todo!(),
    //         EK::Invoke {
    //             function,
    //             arguments,
    //         } => todo!(),
    //         EK::If {
    //             condition,
    //             cond_true,
    //             cond_false,
    //         } => {}
    //         EK::While { condition, body } => {}
    //         EK::StructConstructor { r#type, fields } => todo!(),
    //         EK::Literal(literal) => (),
    //         EK::BinaryOp { op, lhs, rhs } => {
    //             // everything should be copy
    //         }
    //         EK::Field { base, field } => {}
    //     }
    // }
    // traverse(&mut tree);

    //  let mut alive = BTreeMap::new();

    // fn traverse(node: &Stage4Tree) -> Value {
    //     match &node.k {
    //         EK::Let {
    //             name,
    //             explicit_type,
    //             value,
    //         } => {}
    //         EK::Assign { name, value } => todo!(),
    //         EK::Symbol(_) => todo!(),
    //         EK::Block(vec) => todo!(),
    //         EK::Invoke {
    //             function,
    //             arguments,
    //         } => todo!(),
    //         EK::If {
    //             condition,
    //             cond_true,
    //             cond_false,
    //         } => todo!(),
    //         EK::While { condition, body } => todo!(),
    //         EK::StructConstructor { r#type, fields } => todo!(),
    //         EK::Reference(expression) => todo!(),
    //         EK::Literal(literal) => todo!(),
    //         EK::BinaryOp { op, lhs, rhs } => todo!(),
    //         EK::Field { base, field } => todo!(),
    //     }
    // }

    Ok(tree.map(|d| (d, vec![])))
}

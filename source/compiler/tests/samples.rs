macro_rules! sample {
    ($($a:meta)?, $($b:meta)?,$name:ident) => {
        mod $name {
            fn source() -> compiler::parser::linemap::Source {
                use std::str::FromStr;
                compiler::parser::linemap::Source::new(
                    std::path::PathBuf::from_str(
                        concat!("../test_src/", stringify!($name), ".rs",),
                    )
                    .unwrap(),
                    include_str!(concat!("../test_src/", stringify!($name), ".rs")),
                )
            }
            #[test] $(#[$a])?
            fn parse() {
                if let Err(e) = compiler::parser::parse_source(source()) {
                    eprintln!("{}", compiler::error::Error::from(e));
                    panic!("parse failed");
                };
            }
            #[test] $(#[$b])?
            fn compile() {
                if let Err(e) = compiler::compile(source(), std::collections::BTreeMap::new()) {
                    eprintln!("{e}");
                    panic!("compilation failed");
                };
            }
        }
    };
}

// Implemented, Should work

sample!(,,simple);
sample!(,,hello_world);
sample!(,,add);
sample!(,,structfields);
sample!(,,comment);
sample!(,,string);
sample!(,,reference);

sample!(,should_panic,type_conflict);
sample!(,should_panic,double_decl);
sample!(should_panic, should_panic, parse_error);

// To be implemented

sample!(,ignore,generics);
sample!(ignore,ignore,closures);
sample!(ignore,ignore,traits);
sample!(ignore,ignore,impls);

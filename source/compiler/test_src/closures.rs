fn do_thing<U, T: Fn<U, i64>>(x: T) -> U {
    T.call(10)
}
fn main() {
    let y = do_thing(|x| 2 * x);
}

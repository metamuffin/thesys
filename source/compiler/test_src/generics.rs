struct Thing<T> {}
fn magic<T>(x: Thing<T>) -> T {}
fn main() {
    magic(5)
}

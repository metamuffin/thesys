struct Thing {
    height: i64,
    importance: i64,
    useless: i64,
}

fn main() {
    let magic_box = Thing {
        height: 5,
        importance: 15,
        useless: 0,
    };
    let volume = magic_box.height * 3;
    let importance_density = magic_box.importance / volume;
}

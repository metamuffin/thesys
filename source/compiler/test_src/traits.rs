

trait Printable {
    fn print(&self);
}

struct Hello;
impl Printable for Hello {
    fn print(&self) {
        // wrong: no print
    }
}

fn main() {
    Hello{}.print()
}

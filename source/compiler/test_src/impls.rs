
struct Hello {x: i64}
impl Hello {
    pub fn new() -> Hello {
        Hello { x: 10 }
    }
    pub fn print(self: &Hello) {
        // wrong: no print
    }
}

fn main() {
    Hello::new().print()
}


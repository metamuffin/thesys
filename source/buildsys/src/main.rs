#![feature(exit_status_error)]
use error::{ErrorContextExt, Result};
use std::{
    collections::{BTreeMap, HashMap, VecDeque},
    env::{args, current_dir},
    fs::{create_dir_all, read_to_string, remove_dir_all},
    hash::{Hash, Hasher},
    path::{Path, PathBuf},
    process::{exit, Command, Stdio},
    time::Instant,
};

pub mod error;

fn main() {
    if let Err(e) = main_inner() {
        eprintln!("{e}");
        exit(1);
    }
}

fn main_inner() -> Result<()> {
    let mut args = args().skip(1);
    match args.next().expect("expected action").as_str() {
        "build" => {
            create_dir_all(target_dir().join("headers")).unwrap();
            create_dir_all(target_dir().join("objects")).unwrap();
            create_dir_all(target_dir().join("bin")).unwrap();
            let sp = std::env::current_dir()
                .unwrap()
                .join(PathBuf::from(args.next().expect("expected source path")));
            build_path(&sp)?;
        }
        "clean" => {
            status(
                "Clean",
                format!("deleting {}", target_dir().to_str().unwrap()),
            );
            remove_dir_all(target_dir()).unwrap();
        }
        _ => {
            eprintln!("USAGE:");
            eprintln!("\tbuildsys build [source path]");
            eprintln!("\tbuildsys clean")
        }
    }
    Ok(())
}

fn build_path(root: &Path) -> Result<()> {
    let t = Instant::now();

    let mut deps = HashMap::new();
    let mut deps_pending = VecDeque::new();
    deps_pending.push_back(root.to_owned());
    while let Some(dep) = deps_pending.pop_front() {
        if deps.contains_key(&dep) {
            continue;
        }
        let hi = compile_header(&dep).context(&format!("during header compilation of {dep:?}"))?;
        deps_pending.extend(hi.deps.clone().into_values());
        deps.insert(dep, hi);
    }

    let mut objects = Vec::new();
    for (source, hi) in deps {
        let ob = compile(&source, hi.deps).context(&format!("during compilation of {source:?}"))?;
        objects.push(ob)
    }
    link(
        objects,
        root.file_stem().unwrap().to_str().unwrap().to_string(),
    )
    .context("during linking")?;

    status(
        "Finished",
        format!(
            "`dev` profile [unoptimized + unsafe + probably not working] target(s) in {:.02}s",
            t.elapsed().as_secs_f32()
        ),
    );
    Ok(())
}

#[derive(Debug)]
struct HeaderInfo {
    deps: BTreeMap<String, PathBuf>,
}

fn compile_header(sourcepath: &Path) -> Result<HeaderInfo> {
    let out = target_path("headers", sourcepath);
    let mut command = Command::new("compiler")
        .arg("-ch")
        .arg(sourcepath)
        .arg(&out)
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();
    command.wait().unwrap().exit_ok()?;

    let out = read_to_string(out)?;

    let mut deps = BTreeMap::new();
    let dir = sourcepath.parent().unwrap();
    for line in out.split("\n") {
        if let Some(qi) = line.strip_prefix("use ") {
            let dep = qi.split("::").next().unwrap();
            deps.insert(dep.to_owned(), dir.join(dep).with_extension("rs"));
        }
    }

    Ok(HeaderInfo { deps })
}

fn compile(sourcepath: &Path, deps: BTreeMap<String, PathBuf>) -> Result<PathBuf> {
    status(
        "Compiling",
        format!(
            "{} ({})",
            sourcepath.file_stem().unwrap().to_str().unwrap(),
            sourcepath.to_str().unwrap()
        ),
    );

    let input: &Path = &sourcepath;
    let tp = target_path("objects", input);
    let mut command = Command::new("compiler")
        .arg("-c")
        .arg(input)
        .arg(&tp)
        .args(
            deps.iter()
                .map(|(k, v)| format!("-H{k}:{}", v.to_str().unwrap()))
                .collect::<Vec<_>>(),
        )
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();
    command.wait().unwrap().exit_ok()?;

    Ok(tp)
}

fn link(objects: Vec<PathBuf>, progname: String) -> Result<PathBuf> {
    let tp = target_dir().join("bin").join(&progname).with_extension("p");
    status(
        "Linking",
        format!("{} ({})", progname, tp.to_str().unwrap()),
    );
    let mut command = Command::new("compiler")
        .arg("-l")
        .args(objects)
        .arg(&tp)
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();
    command.wait().unwrap().exit_ok()?;
    Ok(tp)
}

fn status(main: &str, message: String) {
    eprintln!("\x1b[1m\x1b[32m{main: >12}\x1b[0m {message}");
}
fn target_dir() -> PathBuf {
    current_dir().unwrap().join("target").join("thesys")
}
fn target_path(kind: &str, sourcepath: &Path) -> PathBuf {
    let mut h = &mut Xorshift(0);
    sourcepath.hash(&mut h);
    let hd = format!(
        "{}-{:016x}",
        sourcepath.file_stem().unwrap().to_str().unwrap(),
        h.finish()
    );
    target_dir().join(kind).join(hd)
}

pub struct Xorshift(u64);
impl Hasher for Xorshift {
    fn finish(&self) -> u64 {
        self.0
    }
    fn write(&mut self, bytes: &[u8]) {
        for b in bytes {
            self.0 += *b as u64;
            self.0 ^= self.0 << 13;
            self.0 ^= self.0 >> 7;
            self.0 ^= self.0 << 17;
        }
    }
}

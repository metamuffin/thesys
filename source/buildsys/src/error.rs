use std::{
    fmt::{Display, Formatter},
    process::ExitStatusError,
};

const RESET: &'static str = "\x1b[0m";
const RED: &'static str = "\x1b[31m";
const BOLD: &'static str = "\x1b[1m";

pub type Result<T, E = Error> = ::std::result::Result<T, E>;

#[derive(Debug)]
pub struct Error {
    message: String,
    context: Vec<String>,
}
impl Error {
    pub fn new(message: String) -> Self {
        Self {
            context: vec![],
            message,
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{RED}{BOLD}Build failed:{RESET}{BOLD} {}", self.message)?;
        for c in self.context.iter().rev() {
            writeln!(f, "\t{c}")?;
        }
        Ok(())
    }
}

pub trait ErrorContextExt {
    fn context(self, message: &str) -> Self;
}
impl<T> ErrorContextExt for Result<T, Error> {
    fn context(self, message: &str) -> Self {
        self.map_err(|mut e| {
            e.context.push(message.to_string());
            e
        })
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::new(value.to_string())
    }
}

impl From<ExitStatusError> for Error {
    fn from(value: ExitStatusError) -> Self {
        Self::new(value.to_string())
    }
}

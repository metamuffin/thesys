# Compiler

1. documentation
1. Refactor parser  
    1. left recursion  
        1. generic call expr  
        1. infix bops  
        1. index operator  
        1. field access  
    1. scoped identifiers  
    1. for loops  
    1. closures  

1. Actual RAII
1. Copy / Clone / Drop semantics
1. References and mutability
1. Traits
1. Generics
1. Closures

1. std  
    1. `Vec<T>`  
    1. `Box<T>`
    1. slice possible?  
    1. `Rc<T>`  

1. Stack unwinding
1. Borrowchecker

### qol
1. ';'
	1. remove ';' after '}'
	1. allow ';' on final statement in non-function blocks


# Processor
1. documentation
1. make it so that memory can be dumped into a `.mem` binary file.

# OS
1. documentation
1. Basic filesystem
	1. directories
	1. implement better file lookup
	1. dynamic sizing
1. use standard errors


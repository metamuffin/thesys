# devices

The C and Rust emulators accept devices as TCP connections. The `read` and
`write` instruction directly interact with the stream. The usual port is 2000.

## Current Devices

Device|Magic Bytes
---|---
0|`ZERO`
browser|`GET `
keyboard|`KYBD`
screen|`SCRN`
terminal|`TTY0`

## Usage

0. `cargo build`
0. `./target/debug/compiler.exe assemble source/<.tsasm file> | ./target/debug/processor.exe`
0. Optional: run other drivers with `./devices/<devicename>/<devicename>` script.

## Formatting

Every device must have:

- 4 Magic bytes

Every device will work on a byte level (ie 8bit not 64bit), unless otherwise
stated. `accept _x` will accept the first device (wait if nessecary), and the
processor will generate a unique id in `_x` for that device.

Typical setup (for accepting an `ABCD`(magic bytes) device):

```
// accept the device 
accept 0
read 0 2 1
equal 2 'A 1
cjump 1 @A
halt
: A
read 0 2 1
equal 2 'B 1
cjump 1 @B
halt
: B
read 0 2 1
equal 2 'C 1
cjump 1 @C
halt
: C
read 0 2 1
equal 2 'D 1
cjump 1 @D
halt
: D
```

(Note: this doesn't show error handling, insead errors that were stored in `1`
were discarded, and assumption is made that everything worked.)

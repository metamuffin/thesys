# screen

## Formatting

- Pixel format: `rgba`
- Colorspace: Rec. BT.709.
- Resolution: 320x180
- Magic bytes: `SCRN`

writes should be byte values (ie [0x00,0xff]).

Example input for 2x2 resolution:
```
accept 0
write #00 0 1
write #ff 0 1
write #00 0 1
write #00 0 1

write #ff 0 1
write #ff 0 1
write #00 0 1
write #00 0 1

write #00 0 1
write #ff 0 1
write #00 0 1
write #00 0 1

write #ff 0 1
write #00 0 1
write #00 0 1
write #00 0 1
```

will result in:
```
00ff0000 ffff0000
00ff0000 ff000000
```
(Note: `a` in `rgba` should control the screen's transparency if possible)

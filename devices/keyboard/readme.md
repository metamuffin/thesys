# keyboard

## Formatting

- Character encoding: ASCII UTF-8
- Magic bytes: `KYBD`

Sends a byte encoding of the chracter as soon as a key is pressed.
(uses `stty raw`)

/// <reference lib="dom" />

document.addEventListener("DOMContentLoaded", () => {
    document.title = "Terminal: Connecting..."

    const ws = new WebSocket(`${document.location.protocol.endsWith("s:") ? "wss:" : "ws:"}//${document.location.host}/device`)

    ws.onerror = () => {
        document.title = "Terminal: Connection Error"
    }
    ws.onclose = () => {
        document.title = "Terminal: Disconnected"
    }
    ws.onopen = () => {
        document.title = "Terminal"
    }
    ws.onmessage = message => {
        write(message.data)
    }
    document.body.addEventListener("keydown", ev => {
        let k = ev.key
        if (ev.key.length == 1) k = ev.key
        if (ev.key == "Enter") k = "\n"
        if (k == "") return
        write(k);
        ws.send(k);
    })

})

function write(s) {
    const text = document.getElementById("text")
    text.textContent += s
}

CC = target/release/compiler

.PHONY: all
all: \
	all-unittests \
	source/os/fib.p \
	source/os/http.p \
	source/os/circle.p \
	source/os/screen.p

# Fib program
source/os/fib.p: source/os/misc/fib_recursive.o
	$(CC) -l $^ $@

# Circle program
source/os/circle.p: source/os/screen/circle.o source/os/misc/primitives.o
	$(CC) -l $^ $@

# HTTP server program
source/os/http.p: source/os/perf/http.o
	$(CC) -l $^ $@

# Terminal program
SCREEN_O = \
	source/os/screen/acceptDevices.o \
	source/os/screen/ascii.o \
	source/os/screen/screen.o \
	source/os/screen/terminal.o \
	source/os/screen/stringBuffers.o
source/os/screen.p: $(SCREEN_O)
	$(CC) -l $^ $@

# Unit tests program
.PHONY: all-unittests
all-unittests: \
	source/os/unittests/arith.p \
	source/os/unittests/functioncalls.p \
	source/os/unittests/stringbuffer.p
source/os/unittests/arith.p: source/os/unittests/arith.o
	$(CC) -l $^ $@
source/os/unittests/functioncalls.p: source/os/unittests/functioncalls.o
	$(CC) -l $^ $@
source/os/unittests/stringbuffer.p: source/os/unittests/stringbuffer.o source/os/screen/stringBuffers.o
	$(CC) -l $^ $@

%.o: %.tsasm $(CC)
	$(CC) -s $< $@
%.o: %.rs $(CC)
	$(CC) -c $< $@

$(CC): $(shell find source/compiler -name '*.rs')
	cargo build --release

clean:
	find source/os -name '*.o' -delete
	find source/os -name '*.p' -delete
	find source/os -name '*.l' -delete
